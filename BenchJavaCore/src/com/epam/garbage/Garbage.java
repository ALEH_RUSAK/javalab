package com.epam.garbage;

public class Garbage {
	
	private final int garbageIndex;
	
	private Garbage hugeGarbage;
	
	public Garbage(int garbageIndex) {
		this.garbageIndex = garbageIndex;
	}
	
	void over() {
		
	}
	
	@Override
	protected void finalize() {
		System.out.println("Garbage " + garbageIndex + " is ready to be deleted.");
	}

	public Garbage getHugeGarbage() {
		return hugeGarbage;
	}

	public void setHugeGarbage(Garbage hugeGarbage) {
		this.hugeGarbage = hugeGarbage;
	}

}
