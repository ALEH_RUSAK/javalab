package com.epam.core.test.string;

import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

import com.epam.core.test.Test;
import com.epam.garbage.Garbage;

/**
 * Provides testing case of changing string's encoding.
 * @author Aleh_Rusak
 */
public final class StringEncodingTest implements Test {
	
	private static final Logger LOGGER = Logger.getLogger(StringEncodingTest.class);
	
	private String engText;
	private String rusText;
	
	public StringEncodingTest() {
		engText = "English Text 123456789";
		rusText = "������� ���� 123";
	}
	
	@Override
	public void runTest() {
		try {
			byte[] engBytes = engText.getBytes("UTF-16");
			char[] utf16Eng = new String(engBytes).toCharArray();
			System.out.println(engText + ", length is: " + engText.length());
			//System.out.println(utf16Eng + ", UTF-16 length is: " + utf16Eng.length());
			for(int i = 0; i < engBytes.length; i++) {
				System.out.println(engBytes[i]);
			}
			byte[] rusBytes = rusText.getBytes("UTF-16");
			String utf16Rus = new String(rusBytes);
			System.out.println(rusText + ", length is: " + rusText.length());
			System.out.println(utf16Rus + ", UTF-16 length is: " + utf16Rus.length());
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}
	}
}
