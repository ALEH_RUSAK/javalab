package com.epam.core.test;

/**
 * Enumeration of test's names.
 * Used by <code>TestFactory</code> to produce tests.
 * @author Aleh_Rusak
 */
public enum TestName {

	GC_SCOPE_TEST, 
	REFERENCE_TEST, 
	STRING_ENCODING_TEST, 
	CLASSLOADER_TEST,
	FACADE_TEST,
	SERIALIZATION_TEST,
	MULTITHREADING_TEST,
	ATOMIC_TEST,
	DEAD_LOCK_TEST,
	PROXY_PATTERN_TEST,
	COLLECTIONS_TEST
}
