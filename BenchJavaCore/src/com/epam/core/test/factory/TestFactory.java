package com.epam.core.test.factory;

import com.epam.core.test.Test;
import com.epam.core.test.TestName;
import com.epam.core.test.impl.classloader.ClassLoaderTest;
import com.epam.core.test.impl.collections.CollectionsTest;
import com.epam.core.test.impl.design.FacadePatternTest;
import com.epam.core.test.impl.design.ProxyPatternTest;
import com.epam.core.test.impl.gc.GCScopeTest;
import com.epam.core.test.impl.gc.ReferenceTest;
import com.epam.core.test.impl.multithread.AtomicTest;
import com.epam.core.test.impl.multithread.DeadLockTest;
import com.epam.core.test.impl.multithread.MultithreadingTest;
import com.epam.core.test.impl.serialization.SerializationTest;
import com.epam.core.test.string.StringEncodingTest;

/**
 * Provides creating tests based on it's name.
 * @author Aleh_Rusak
 */
public final class TestFactory {
	
	private TestFactory() {}
	
	/**
	 * Provides creating test based on it's name.
	 * @return created test instance
	 */
	public static Test getTestByName(TestName testName) {
		Test test = null;
		switch(testName) {
		case GC_SCOPE_TEST:
			test = new GCScopeTest();
			break;
		case REFERENCE_TEST:
			test = new ReferenceTest();
			break;
		case STRING_ENCODING_TEST:
			test = new StringEncodingTest();
			break;
		case CLASSLOADER_TEST:
			test = new ClassLoaderTest();
			break;
		case FACADE_TEST:
			test = new FacadePatternTest();
			break;
		case SERIALIZATION_TEST:
			test = new SerializationTest();
			break;
		case MULTITHREADING_TEST:
			test = new MultithreadingTest();
			break;
		case ATOMIC_TEST:
			test = new AtomicTest();
			break;
		case DEAD_LOCK_TEST:
			test = new DeadLockTest();
			break;
		case PROXY_PATTERN_TEST:
			test = new ProxyPatternTest();
			break;
		case COLLECTIONS_TEST:
			test = new CollectionsTest();
			break;
		}
		return test;
	}
}
