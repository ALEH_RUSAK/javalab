package com.epam.core.test;

/**
 * Provides behavior for test classes.
 * All tests of the application have to implement this interface.
 * @author Aleh_Rusak
 */
public interface Test {
	
	/**
	 * Provides testing some case.
	 * Has to be implemented in all classes, which provide testing logic.
	 */
	public void runTest();
}
