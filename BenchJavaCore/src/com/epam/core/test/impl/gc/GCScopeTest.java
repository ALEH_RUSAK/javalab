package com.epam.core.test.impl.gc;

import org.apache.log4j.Logger;

import com.epam.core.test.Test;
import com.epam.garbage.Garbage;

/**
 * Class provides testing garbage collection.
 * @author Aleh_Rusak
 */
public final class GCScopeTest implements Test {
	
	private static final Logger LOGGER = Logger.getLogger(GCScopeTest.class);
	
	private Garbage garbageOne;
	private Garbage garbageTwo;
	private Garbage garbageThree;
	
	public GCScopeTest() {
		garbageOne = new Garbage(1);
		garbageTwo = new Garbage(2);
		garbageThree = new Garbage(3);
	}

	public Garbage getGarbageOne() {
		return garbageOne;
	}

	public Garbage getGarbageTwo() {
		return garbageTwo;
	}

	public Garbage getGarbageThree() {
		return garbageThree;
	}

	@Override
	public void runTest() {
		
		/* Creating garbage reference cycling.
		 * We see, that every object of garbage references on another garbage. */
		garbageOne.setHugeGarbage(garbageTwo);
		garbageTwo.setHugeGarbage(garbageThree);
		garbageThree.setHugeGarbage(garbageOne);
		
		/* GC can't delete object, because g3.hugeGarbage
		 * references to g1 */
		garbageOne = null;
		
		/* GC can't delete object, because g3.hugeGarbage
		 * references to g1 */
		garbageTwo = null; 
		
		/* GC can't delete object, because g3.hugeGarbage.hugeGarbage
		 * references to g2 */
		//garbageThree = null; 
		
		Runtime.getRuntime().gc();
		try {
			Thread.sleep(400);
		} catch (InterruptedException e) {
			LOGGER.error(e);
		}
	}
}
