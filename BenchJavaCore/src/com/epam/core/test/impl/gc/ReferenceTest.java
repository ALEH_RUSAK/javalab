package com.epam.core.test.impl.gc;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.epam.core.test.Test;

class SomeObject {
	
	private int objectIndex;
	
	public SomeObject(int objectIndex) {
		this.setObjectIndex(objectIndex);
	}

	@Override
	protected void finalize() {
		System.out.println("SomeObject " + objectIndex + " is finalizing now.");
	}
	
	public int getObjectIndex() {
		return objectIndex;
	}

	public void setObjectIndex(int objectIndex) {
		this.objectIndex = objectIndex;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getName());
		builder.append(" ");
		builder.append(objectIndex);
		return builder.toString();
	}
}

/**
 * Class provides testing different types of references in Java.
 * @author Aleh_Rusak
 */
public final class ReferenceTest implements Test {
	
	/**
	 * Strong references
	 */
	private List<SomeObject> strongRefs;
	/**
	 * Soft references
	 */
	private List<Reference<SomeObject>> softRefs; 
	/**
	 * Weak references
	 */
	private List<Reference<SomeObject>> weakRefs;  
	/**
	 * Phantom references
	 */
	private List<Reference<SomeObject>> phantomRefs; 
	/**
	 * Reference queue
	 */
	private ReferenceQueue<SomeObject> refQueue; 
	
	public ReferenceTest() {
		strongRefs = new ArrayList<SomeObject>();
		softRefs = new ArrayList<Reference<SomeObject>>();
		weakRefs = new ArrayList<Reference<SomeObject>>();
		phantomRefs = new ArrayList<Reference<SomeObject>>();
		refQueue = new ReferenceQueue<SomeObject>();
	}
	
	/**
	 * Provides initialization of references list
	 */
	private void initData() {
		strongRefs.clear();
		softRefs.clear();
		weakRefs.clear();
		phantomRefs.clear();
		final int refCount = 3;
		for(int index = 0; index < refCount; index++) {
			strongRefs.add(new SomeObject(index));
			softRefs.add(new SoftReference<SomeObject>(new SomeObject(index)));
			weakRefs.add(new WeakReference<SomeObject>(new SomeObject(index)));
			phantomRefs.add(new PhantomReference<SomeObject>(new SomeObject(index), refQueue));
		}
		System.out.println("\nBefore \n");
		this.checkRefLists();
	}
	
	/**
	 * Method is used by classes which extend
	 * <code>Reference</code> class because of it's method <code>get()</code>
	 */
	private void printRefList(List<Reference<SomeObject>> refList) {
		for(Reference<SomeObject> obj: refList) {
			SomeObject tempObj = obj.get();
			if(tempObj == null) {
				System.out.println(tempObj);
			} else {
				System.out.println(tempObj.toString());
			}
		}
	}
	
	/**
	 * Provides displaying reference lists
	 */
	private void checkRefLists() {
		System.out.println("Strong references: ");
		for(SomeObject obj: strongRefs) {
			System.out.println(obj.toString());
		}
		System.out.println("Soft references: ");
		printRefList(softRefs);
		System.out.println("Weak references: ");
		printRefList(weakRefs);
		System.out.println("Phantom references: ");
		printRefList(phantomRefs);
	}
	
	/**
	 * Provides testing phantom and weak references
	 */
	private void testPhantomWeakReferences() {
		System.out.println("\nTesting weak and phantom references.\n");
		this.initData();
		/* Give an advice to gc to perform cleaning */
		Runtime.getRuntime().gc();
		System.out.println("\nAfter \n");
		this.checkRefLists();
	}
	
	/**
	 * Provides testing soft references
	 */
	private void testSoftReferences() {
		System.out.println("\nTesting soft references.\n");
		this.initData();
		/* Give an advice to gc to perform cleaning */
		Runtime.getRuntime().gc();
		System.out.println("\nAfter \n");
		this.checkRefLists();
	}
	
	@Override
	public void runTest() {
		System.out.println("\nTest ReferenceTest has been started.\n");
		this.testPhantomWeakReferences();
		this.testSoftReferences();
	}
}
