package com.epam.core.test.impl.design;

import com.epam.core.design.proxy.DataManagerProxy;
import com.epam.core.design.proxy.DataPackage;
import com.epam.core.design.proxy.IDataManager;
import com.epam.core.test.Test;

/**
 * Provides showing the usage of the Proxy design pattern.
 * Logging proxy demonstration.
 * @author Aleh_Rusak
 */
public class ProxyPatternTest implements Test {

	private static final String PACKAGE_KEYWORD = "Package ";
	private static final String PACKAGE_DATA_CONTENT = "Data package content ";
	
	private IDataManager dataManagerProxy;
	
	public ProxyPatternTest() {
		dataManagerProxy = new DataManagerProxy();
	}
	
	@Override
	public void runTest() {
		final int contentSize = 4;
		for(int dataIndex = 0; dataIndex < contentSize; dataIndex++) {
			DataPackage data = new DataPackage();
			data.setDataContent(PACKAGE_DATA_CONTENT + dataIndex);
			String packageName = PACKAGE_KEYWORD + dataIndex;
			dataManagerProxy.writeData(packageName, data);
		}
		String removeOne = PACKAGE_KEYWORD + 2;
		String removeTwo = PACKAGE_KEYWORD + 3;
		dataManagerProxy.removeData(removeOne);
		dataManagerProxy.removeData(removeTwo);
		for(int dataIndex = 0; dataIndex < contentSize; dataIndex++) {
			String packageName = PACKAGE_KEYWORD + dataIndex;
			System.out.println(dataManagerProxy.readData(packageName));
		}
	}
}
