package com.epam.core.test.impl.design;

import com.epam.core.design.facade.CarStarter;
import com.epam.core.test.Test;

/**
 * Provides testing the usage of Facade pattern.
 * @author Aleh_Rusak
 */
public final class FacadePatternTest implements Test {

	@Override
	public void runTest() {
		CarStarter starter = new CarStarter(1, false, true);
		System.out.println(starter.startCar());
	}
}
