package com.epam.core.test.impl.multithread;

import com.epam.core.test.Test;
import com.epam.core.test.impl.multithread.thread.ReadThread;
import com.epam.core.test.impl.multithread.thread.WriteThread;

/**
 * Provides testing Java wait()/notify() multithreading concepts.
 * Uses <code>FileReadWriteManager</code> to provide testing cases
 * of waiting and notifying threads.
 * @author Aleh_Rusak
 */
public final class MultithreadingTest implements Test {
	
	@Override
	public void runTest() {
		FileReadWriteManager fileRWManager = new FileReadWriteManager();
		WriteThread writeThread = new WriteThread();
		ReadThread readThread = new ReadThread();
		writeThread.setFileRWManager(fileRWManager);
		readThread.setFileRWManager(fileRWManager);
		readThread.start();
		writeThread.start();
	}
}
