package com.epam.core.test.impl.multithread;

import com.epam.core.test.Test;

class SynchronizingClass {
	
	private final Object lockOne = new Object();
	private final Object lockTwo = new Object();
	
	public void methodOne() {
		synchronized(lockOne) {
			System.out.println("Thread 1 is waiting for lock 2...");
			synchronized(lockTwo) {
				System.out.println("Can't get this lock.");
			}
		}
	}
	
	public void methodTwo() {
		synchronized(lockTwo) {
			System.out.println("Thread 2 is waiting for lock 1...");
			synchronized(lockOne) {
				System.out.println("Can't get this lock.");
			}
		}
	}
}

public final class DeadLockTest implements Test {

	private SynchronizingClass synchroObject = new SynchronizingClass();
	
	private class ThreadOne extends Thread {
		
		@Override
		public void run() {
			synchroObject.methodOne();
		}
	}
	
	private class ThreadTwo extends Thread {
		
		@Override
		public void run() {
			synchroObject.methodTwo();
		}
	}
	
	@Override
	public void runTest() {
		ThreadOne threadOne = new ThreadOne();
		ThreadTwo threadTwo = new ThreadTwo();
		threadOne.start();
		threadTwo.start();
	}
}
