package com.epam.core.test.impl.multithread;
import static java.lang.Math.*;

/**
 * Provides calculating the double volatile value.
 * @author Aleh_Rusak
 */
public class Calculator {
	
	private volatile double value = 0.0;
	
	/**
	 * @return the calculated value
	 */
	public double readValue() {
		return value;
	}
	
	/**
	 * This method actually performs calculating.
	 */
	public void calculateValue() {
		value = ceil(sqrt(PI + sqrt(E) + acos(sqrt(0.654562))) / asin(Math.atan(0.245161)) * floor(E + sqrt(PI)));
	}
}
