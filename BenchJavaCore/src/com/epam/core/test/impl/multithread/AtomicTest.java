package com.epam.core.test.impl.multithread;

import com.epam.core.test.Test;
import com.epam.core.test.impl.multithread.thread.ReadThread;
import com.epam.core.test.impl.multithread.thread.WriteThread;

/**
 * Provides testing volatile keyword in multithreading environment.
 * @author Aleh_Rusak
 */
public final class AtomicTest implements Test {

	@Override
	public void runTest() {
		Calculator calculator = new Calculator();
		WriteThread writeThread = new WriteThread();
		writeThread.setCalculator(calculator);
		ReadThread readThread = new ReadThread();
		readThread.setCalculator(calculator);
		writeThread.start();
		readThread.start();
	}
}
