package com.epam.core.test.impl.multithread.thread;

import com.epam.core.test.impl.multithread.Calculator;
import com.epam.core.test.impl.multithread.FileReadWriteManager;

public class ReadThread extends Thread {

	private FileReadWriteManager fileRWManager;
	private Calculator calculator;
	
	private void readFromFile() {
		//for(int dataIndex = 0; dataIndex < 10; dataIndex++) {
			fileRWManager.readData();
			/*try {
				sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		//}
	}
	
	private double readCalculatorValue() {
		return calculator.readValue();
	}
	
	public ReadThread() {}
	
	@Override
	public void run() {
		System.out.println(this.readCalculatorValue());
		//this.readFromFile();
	}
	
	public void setFileRWManager(FileReadWriteManager fileRWManager) {
		this.fileRWManager = fileRWManager;
	}

	public void setCalculator(Calculator calculator) {
		this.calculator = calculator;
	}
}
