package com.epam.core.test.impl.multithread;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

/**
 * Provides read/write operations with .txt file in
 * multithreading environment.
 * @author Aleh_Rusak
 */
public class FileReadWriteManager {
	
	private static final Logger LOGGER = Logger.getLogger(FileReadWriteManager.class);
	
	/**
	 * The indicator of readiness of file.
	 */
	private boolean ready = false;
	
	/**
	 * The locking object. 
	 */
	private final Object lock = new Object();
	
	/**
	 * Method writes data into the file and notifies threads
	 * which are waiting for data readiness.
	 * Threads will synchronize on lock object.
	 */
	public void writeData() {
		synchronized(lock) {
			ready = false;
			try {
				System.out.println("Writer writes...");
				PrintWriter writer = new PrintWriter(new File("D:/workspace/threads.txt"));
				writer.write("Some data!!!!dawlfjwakjfkwajfkwajkfwajkf");
				Thread.sleep(2000);
				writer.close();
			} catch (FileNotFoundException | InterruptedException e) {
				LOGGER.error(e);
			}
			ready = true;
			lock.notify();
		}
	}
	
	/**
	 * Method reads data from file only if it is ready to
	 * be read. Waits until file becomes ready.
	 * Threads will synchronize on lock object.
	 */
	public void readData() {
		synchronized(lock) {
			while (!ready) {
				System.out.println("Reader waits...");
	            try {
	            	lock.wait();
	            } catch (InterruptedException e) {
	            	LOGGER.error(e);
	            }
	        }
			try {
				BufferedReader reader = new BufferedReader(new FileReader(new File("D:/workspace/threads.txt")));
				String dataLength = reader.readLine();
				System.out.println("Read data is: " + dataLength.length());
				reader.close();
			} catch (IOException e) {
				LOGGER.error(e);
			}	
		}
	}
}
