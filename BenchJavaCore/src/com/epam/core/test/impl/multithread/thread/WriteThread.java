package com.epam.core.test.impl.multithread.thread;

import com.epam.core.test.impl.multithread.Calculator;
import com.epam.core.test.impl.multithread.FileReadWriteManager;

public class WriteThread extends Thread {
	
	private FileReadWriteManager fileRWManager;
	private Calculator calculator;
	
	private void writeIntoFile() {
		//for(int dataIndex = 0; dataIndex < 10; dataIndex++) {
			fileRWManager.writeData();
			/*try {
				sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		//}
	}

	private void calculateValue() {
		calculator.calculateValue();
	}
	
	@Override
	public void run() {
		this.calculateValue();
		//this.writeIntoFile();
	}

	public void setFileRWManager(FileReadWriteManager fileRWManager) {
		this.fileRWManager = fileRWManager;
	}
	
	public void setCalculator(Calculator calculator) {
		this.calculator = calculator;
	}
}
