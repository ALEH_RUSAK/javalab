package com.epam.core.test.impl.serialization;

import java.io.Serializable;

/**
 * The model of employee. 
 * Instances of this class will store in a binary file.
 * @author Aleh_Rusak
 */
public class Employee implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1363570929252058033L;
	
	/**
	 * Employee's name
	 */
	private String name;
	
	//private String property;
	
	/**
	 * Employee's expierience in years
	 */
	private int expierience;
	
	public Employee() {
		name = "employee name";
		expierience = 2;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employee [name=");
		builder.append(name);
		builder.append(", expierience=");
		builder.append(expierience);
		//builder.append(", property=");
		//builder.append(property);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the expierience
	 */
	public int getExpierience() {
		return expierience;
	}
	
	/**
	 * @param expierience the expierience to set
	 */
	public void setExpierience(int expierience) {
		this.expierience = expierience;
	}
	
}
