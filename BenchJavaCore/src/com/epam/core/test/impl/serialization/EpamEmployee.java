package com.epam.core.test.impl.serialization;

/**
 * Epamer's model. 
 * Instances of this class will store in a binary file.
 * @author Aleh_Rusak
 */
public class EpamEmployee extends Employee {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8277914026992607520L;

	/**
	 * Employee's position
	 */
	private String workPosition;

	public EpamEmployee() {
		workPosition = "Epam employee.";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EpamEmployee: ");
		builder.append(super.toString());
		builder.append(", workPosition=");
		builder.append(workPosition);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the workPosition
	 */
	public String getWorkPosition() {
		return workPosition;
	}

	/**
	 * @param workPosition the workPosition to set
	 */
	public void setWorkPosition(String workPosition) {
		this.workPosition = workPosition;
	}
}
