package com.epam.core.test.impl.serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.log4j.Logger;

import com.epam.core.test.Test;

/**
 * Provides testing the serialization process.
 * Testing cases are: serialization and 
 * deserialization using <code>Serializable</code>,
 * serialization of objects which contain another object. 
 * @author Aleh_Rusak
 */
public class SerializationTest implements Test {
	
	private static final Logger LOGGER = Logger.getLogger(SerializationTest.class);
	
	private Employee employee;
	private EpamEmployee epamEmployee;
	
	public SerializationTest() {
		employee = new Employee();
		epamEmployee = new EpamEmployee();
	}

	@Override
	public void runTest() {
		boolean serialize = true;
		File file = new File("D:/workspace/sFile.bin");
        try {
        	if(serialize == true) {
        		FileOutputStream fileOutput = new FileOutputStream(file);
    			ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
    			objectOutput.writeObject(employee);
    			objectOutput.close();
    			fileOutput.close();
        	} else {
        		FileInputStream fileInput = new FileInputStream(file);
            	ObjectInputStream objectInput = new ObjectInputStream(fileInput);
            	Employee newObject = (Employee) objectInput.readObject();
            	System.out.println(newObject.toString());
            	objectInput.close();
            	fileInput.close();
        	}
		} catch (IOException | ClassNotFoundException e) {
			LOGGER.error(e);
		}
	}
}
