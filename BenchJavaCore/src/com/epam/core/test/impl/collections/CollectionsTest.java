package com.epam.core.test.impl.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.epam.core.test.Test;

/**
 * Provides testing collections benefits.
 * @author Aleh_Rusak
 */
public final class CollectionsTest implements Test {

	private long timeOfSearching(List<Integer> list) {
		long start = System.nanoTime();
		list.contains(732847);
		long end = System.nanoTime();
		return end - start;
	}
	
	private long timeOfSearchingIndex(List<Integer> list) {
		long start = System.nanoTime();
		list.get(732847);
		long end = System.nanoTime();
		return end - start;
	}
	
	private long timeOfSearching(Set<Integer> set) {
		long start = System.nanoTime();
		set.contains(732847);
		long end = System.nanoTime();
		return end - start;
	}
	
	@Override
	public void runTest() {
		Set<Integer> treeSet = new TreeSet<Integer>();
		Set<Integer> hashSet = new HashSet<Integer>();
		List<Integer> arrayList = new ArrayList<Integer>();
		List<Integer> linkedList = new LinkedList<Integer>();
		for(int i = 0; i < 10000000; i++) {
			treeSet.add(i);
			hashSet.add(i);
			arrayList.add(i);
			linkedList.add(i);
		}
		System.out.println("Array list time: " + timeOfSearching(arrayList));
		System.out.println("Linked list time: " + timeOfSearching(linkedList));
		System.out.println("Array list time: " + timeOfSearchingIndex(arrayList));
		System.out.println("Linked list time: " + timeOfSearchingIndex(linkedList));
		System.out.println("HashSet time: " + timeOfSearching(hashSet));
		System.out.println("TreeSet time: " + timeOfSearching(treeSet));
	}
}
