package com.epam.core.test.impl.classloader;

import org.apache.log4j.Logger;

import com.epam.core.test.Test;
import com.epam.core.test.impl.gc.GCScopeTest;

/**
 * Class provides testing custom class loader.
 * @author Aleh_Rusak
 */
public final class ClassLoaderTest implements Test {

	private static final Logger LOGGER = Logger.getLogger(GCScopeTest.class);
	
	private String betSystemPath;
	private String marathonBetPath;
	
	public ClassLoaderTest() {
		betSystemPath = "D:/workspace/BetSystem.class";
		marathonBetPath = "D:/workspace/MarathonBet.class";
	}
	
	@Override
	public void runTest() {
		try {
			BetSystemLoader loader = new BetSystemLoader(ClassLoader.getSystemClassLoader());
			loader.setClassFileSystemPath(betSystemPath);
			loader.loadClass("com.epam.core.bet.BetSystem");
			loader.setClassFileSystemPath(marathonBetPath);
			loader.loadClass("com.epam.core.bet.MarathonBet");
		} catch (ClassNotFoundException e) {
			LOGGER.error(e);
		}
	}

}
