package com.epam.core.test.impl.classloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * User-defined class loader must extend java.lang.ClassLoader. It has to
 * support the dynamic delegating model. Method loadClass() is already defined
 * correctly in java.lang.ClassLoader as method findLoadedClass() which tries to
 * find out if class was already loaded. We are just about overriding method
 * findClass() just to help the loader to find byte-code in the file system.
 * @author Aleh_Rusak
 */
public class BetSystemLoader extends ClassLoader {

	/**
	 * As mentioned, we have to tell class loader, where the byte-code is. This
	 * is path variable.
	 */
	private String classFileSystemPath;

	/**
	 * In default constructor we define the path, where loader will try to find
	 * class by default.
	 * @param parentLoader the parent class loader
	 */
	public BetSystemLoader(ClassLoader parentLoader) {
		/* Supporting dynamic delegating model */
		super(parentLoader);
	}

	/**
	 * Constructor gets the path to .class file in the file system as parameter.
	 * @param classFileSystemPath the path to .class file in the file system
	 * @param parentLoader the parent class loader
	 */
	public BetSystemLoader(String classFileSystemPath, ClassLoader parentLoader) {
		/* Supporting dynamic delegating model */
		super(parentLoader);
		this.classFileSystemPath = classFileSystemPath;
	}

	/**
	 * Provides reading the file content into byte array.
	 * @param qualyfiedClassPath the full class path
	 * @return the byte-code of a .class file
	 * @throws IOException 
	 */
	private byte[] getClassCode(String qualyfiedClassPath) throws IOException {
		
		InputStream byteStream = new FileInputStream(new File(qualyfiedClassPath));
		long fileSize = new File(qualyfiedClassPath).length();
		// Create the byte array to hold the data
		byte[] byteCode = new byte[(int) fileSize];
		int offset = 0;
		int numRead = 0;
		while (offset < byteCode.length
				&& (numRead = byteStream.read(byteCode, offset, byteCode.length - offset)) >= 0) {
			offset += numRead;
		}
		if (offset < byteCode.length) {
			byteStream.close();
			throw new IOException("Unfinished file reading " + qualyfiedClassPath);
		}
		byteStream.close();
		return byteCode;
	}

	@Override
	public Class<?> findClass(String className) throws ClassNotFoundException {
		try {
			byte byteCode[] = this.getClassCode(classFileSystemPath);
			return defineClass(className, byteCode, 0, byteCode.length);
		} catch (IOException e) {
			return super.findClass(className);
		} 
	}
	
	public void setClassFileSystemPath(String classFileSystemPath) {
		this.classFileSystemPath = classFileSystemPath;
	}
}
