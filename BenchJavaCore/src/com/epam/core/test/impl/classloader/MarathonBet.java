package com.epam.core.test.impl.classloader;

/**
 * Provides implementation of bet system working 
 * based on marathon bet.
 * @author Aleh_Rusak
 */
public class MarathonBet implements BetSystem {

	/**
	 * The maximum value of money that player can bet.
	 */
	private static final int maxBet = 1000;
	
	/**
	 * The minimum value of money that player can bet.
	 */
	private static final int minBet = 100;

	@Override
	public void acceptBet(int money) {
		if(money < minBet && money > maxBet) {
			System.out.println("������ ������ ������.");
		} else {
			System.out.println("������ �������!");
		}
	}
}
