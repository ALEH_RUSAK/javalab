package com.epam.core.test.impl.classloader;

/**
 * Provides behavior of betting systems
 * @author Aleh_Rusak
 */
public interface BetSystem {

	/**
	 * Provides accepting bet from player.
	 */
	public void acceptBet(int money);
}
