package com.epam.core.bet;

/**
 * Provides behavior of betting systems
 * @author Aleh_Rusak
 */
public interface BetSystem {
	
	/**
	 * Provides accepting bet from player
	 * @return true, if bet was accepted
	 */
	public boolean acceptBet(int money);
}
