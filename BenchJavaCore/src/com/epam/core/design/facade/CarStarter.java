package com.epam.core.design.facade;

import com.epam.core.design.facade.equipment.CarSystemChecker;
import com.epam.core.design.facade.equipment.EngineStarter;
import com.epam.core.design.facade.equipment.FuelController;

/**
 * This class facades the process of starting car.
 * @author Aleh_Rusak
 */
public class CarStarter {
	
	private FuelController fuelController;
	private EngineStarter engineStarter;
	private CarSystemChecker systemParamsChecker;
	
	public CarStarter(int fuelLevel, boolean airbags, boolean conditioning) {
		//this
		systemParamsChecker = new CarSystemChecker(airbags, conditioning);
		fuelController = new FuelController(fuelLevel);
		engineStarter = new EngineStarter();
	}
	
	public String startCar() {
		StringBuilder builder = new StringBuilder();
		builder.append("Starting the car...\n");
		boolean hasFuel = fuelController.checkFuelLevel();
		builder.append(systemParamsChecker.checkSystems());
		builder.append(engineStarter.startEngine(hasFuel));
		return builder.toString();
	}
}
