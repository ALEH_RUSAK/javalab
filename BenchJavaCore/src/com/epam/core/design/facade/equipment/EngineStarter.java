package com.epam.core.design.facade.equipment;

/**
 * Provides starting car's engine.
 * @author Aleh_Rusak
 */
public class EngineStarter {
	
	private static final String ENGINE_NOT_STARTED = "Engine not started. Not enough fuel.";
	private static final String ENGINE_STARTED = "Engine started successfully. Have a nice trip.";
	
	public EngineStarter() {}
	
	/**
	 * Provides attempting to start car's engine.
	 * @param hasFuel has the car fuel or not
	 * @return the result of starting as a String
	 */
	public String startEngine(boolean hasFuel) {
		if(hasFuel) {
			return ENGINE_STARTED;
		} else {
			return ENGINE_NOT_STARTED;
		}
	}
}