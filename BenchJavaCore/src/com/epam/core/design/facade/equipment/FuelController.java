package com.epam.core.design.facade.equipment;

/**
 * Provides the controlling of fuel level.
 * @author Aleh_Rusak
 */
public class FuelController {
	
	private int fuelPercentage;
	
	public FuelController(int fuelPercentage) {
		this.fuelPercentage = fuelPercentage;
	}
	
	/**
	 * Provides checking the fuel level.
	 * @return false, if there is not enough fuel to start engine 
	 */
	public boolean checkFuelLevel() {
		if(fuelPercentage <= 5) {
			return false;
		} else {
			return true;
		}
	}
}
