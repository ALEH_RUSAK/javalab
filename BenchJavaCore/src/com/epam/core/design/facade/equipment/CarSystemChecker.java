package com.epam.core.design.facade.equipment;

/**
 * Provides checking car's security systems.
 * @author Aleh_Rusak
 */
public class CarSystemChecker {
	
	private static final String CONDITIONINIG_NOT_WORKING = "Air conditioning is not working.";
	private static final String CONDITIONINIG_WORKING = "Air conditioning is fine.";
	private static final String AIRBAGS_NOT_WORKING = "Airbags are not working.";
	private static final String AIRBAGS_WORKING = "Airbags are fine.";
	
	private boolean airConditioning;
	private boolean airbags;
	
	public CarSystemChecker(boolean airConditioning, boolean airbags) {
		this.airConditioning = airConditioning;
		this.airbags = airbags;
	}
	
	private String checkAirbags() {
		if(airbags) {
			return AIRBAGS_WORKING;
		} else {
			return AIRBAGS_NOT_WORKING;
		}
	}
	
	private String checkConditioning() {
		if(airConditioning) {
			return CONDITIONINIG_WORKING;
		} else {
			return CONDITIONINIG_NOT_WORKING;
		}
	}
	
	/**
	 * Checks if airbags or conditioning are not working.
	 * @return information about systems as a string
	 */
	public String checkSystems() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.checkAirbags());
		builder.append("\n");
		builder.append(this.checkConditioning());
		builder.append("\n");
		return builder.toString();
	}
}
