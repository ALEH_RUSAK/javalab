package com.epam.core.design.proxy;

/**
 * Data is encapsulated in packages.
 * Provides consistency of writing and reading data.
 * @author Aleh_Rusak
 */
public class DataPackage {

	/**
	 * The content of a package
	 */
	private String dataContent;
	
	public DataPackage() {}
	
	public DataPackage(String dataContent) {
		this.dataContent = dataContent;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataPackage [dataContent=");
		builder.append(dataContent);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Provides adding some data into package.
	 * @param data the data to be added.
	 */
	public void setDataContent(String dataContent) {
		this.dataContent = dataContent;
	}
	
	/**
	 * Provides getting all amount of data stored in package.
	 * @return dataContent
	 */
	public String getDataContent() {
		return dataContent;
	}
}
