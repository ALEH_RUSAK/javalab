package com.epam.core.design.proxy;

/**
 * Provides methods to be implemented by data managers.
 * Write/read actions can be performed only through data managers.
 * @author Aleh_Rusak
 */
public interface IDataManager {
	
	/**
	 * Provides writing data stored in <code>DataPackage</code> instance.
	 * @param dataPackage the package of data to be written.
	 */
	public void writeData(String packageName, DataPackage dataPackage);
	
	/**
	 * Provides reading data by it's unique packageId.
	 * @param packageId the id of the data package.
	 * @return <code>DataPackage</code> instance.
	 */
	public DataPackage readData(String packageName);
	
	/**
	 * Provides removing data from storage.
	 * @param packageId the id of the data package.
	 */
	public void removeData(String packageName);
}
