package com.epam.core.design.proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * The data manager implementation.
 * Provides implementation of operations that can
 * be performed with <code>DataPackage<code>'s
 * @author Aleh_Rusak
 */
public class DataManagerImpl implements IDataManager {
	
	private Map<String, DataPackage> storage;
	
	public DataManagerImpl() {
		storage = new HashMap<String, DataPackage>(); 
	}
	
	@Override
	public void writeData(String packageName, DataPackage dataPackage) {
		storage.put(packageName, dataPackage);
	}

	@Override
	public DataPackage readData(String packageName) {
		return storage.get(packageName);
	}

	@Override
	public void removeData(String packageName) {
		storage.remove(packageName);
	}
}
