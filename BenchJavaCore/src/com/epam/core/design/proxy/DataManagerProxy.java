package com.epam.core.design.proxy;

/**
 * The Logging Access Proxy implementation for <code>IDataManager</code>s.
 * @author Aleh_Rusak
 */
public class DataManagerProxy implements IDataManager {

	//private static final Logger LOGGER = Logger.getLogger(DataManagerProxy.class);
	
	private static final String PACKAGE_START = "Data package ";
	private static final String PACKAGE_WRITTEN_END = " has been written.";
	private static final String PACKAGE_READ_END = " has been read.";
	private static final String PACKAGE_REMOVED_END = " has been removed.";
	
	private DataManagerImpl dataManager;

	public DataManagerProxy() {
		dataManager = new DataManagerImpl();
	}

	@Override
	public void writeData(String packageName, DataPackage dataPackage) {
		dataManager.writeData(packageName, dataPackage);
		//LOGGER.debug(PACKAGE_WRITTEN);
		System.out.println(PACKAGE_START + packageName + PACKAGE_WRITTEN_END);
	}

	@Override
	public DataPackage readData(String packageName) {
		DataPackage dataPackage = dataManager.readData(packageName);
		//LOGGER.debug(PACKAGE_READ);
		System.out.println(PACKAGE_START + packageName + PACKAGE_READ_END);
		return dataPackage;
	}

	@Override
	public void removeData(String packageName) {
		dataManager.removeData(packageName);
		//LOGGER.debug(PACKAGE_REMOVED);
		System.out.println(PACKAGE_START + packageName + PACKAGE_REMOVED_END);
	}

}
