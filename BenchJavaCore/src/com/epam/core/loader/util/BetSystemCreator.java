package com.epam.core.loader.util;

import org.apache.log4j.Logger;

import com.epam.core.bet.BetSystem;
import com.epam.core.test.impl.classloader.BetSystemLoader;

/**
 * 
 * @author Aleh_Rusak
 */
public final class BetSystemCreator {
	
	private static final Logger LOGGER = Logger.getLogger(BetSystemCreator.class);
	
	private static final String CLASS_NOT_FOUND_MESSAGE = "Class was not found.";
	private static final String INSTANTIATION_MESSAGE = "Instance was not created.";
	private static final String ILLEGAL_ACCESS_MESSAGE = "Illegal Access.";
	
	/**
	 * Provides creating an instance of betSystem using class loader.
	 * @param loader the class loader
	 * @param className the name of a class
	 * @return instantiated bet system or null
	 */
	public static BetSystem createBetSystem(BetSystemLoader loader, String className) {
		BetSystem betSystem = null;
		try {
			Class<?> newClassInstance = loader.loadClass(className);
			betSystem = (BetSystem) newClassInstance.newInstance();
		} catch (ClassNotFoundException e) {
			LOGGER.debug(CLASS_NOT_FOUND_MESSAGE);
		} catch (InstantiationException e) {
			LOGGER.debug(INSTANTIATION_MESSAGE);
		} catch (IllegalAccessException e) {
			LOGGER.debug(ILLEGAL_ACCESS_MESSAGE);
		} 
		return betSystem;
	}
}
