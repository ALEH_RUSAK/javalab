package com.epam.testapp.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Connection;

import com.epam.testapp.database.connection.DBConnectionPool;
import com.epam.testapp.exceptions.ConnectionException;

/**
 * DBConnectionPool test for the application
 * @author Aleh_Rusak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:TestContext.xml" })
public class DBConnectionPoolTest {
	
	@Autowired
	private DBConnectionPool connectionPool;
	
	@Test
	public void getConnectionTest() throws ConnectionException {
		Connection connection = connectionPool.getConnection();
		boolean expected = true;
		boolean actual;
		if(connection != null) {
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(expected, actual);
	}
}
