package com.epam.testapp.tests;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.assertion.DbUnitAssert;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.testapp.database.dao.INewsDAO;
import com.epam.testapp.model.News;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:TestContext.xml" })
public class NewsDAOImplTest {
	
	/**
	 * DataSets locations
	 */
	private static final String STARTING_DATA_SET = "/test/dataset/StartingDataSet.xml";
	private static final String FETCH_DATA_SET = "/test/dataset/FetchByIdDataSet.xml";
	private static final String DELETE_DATA_SET = "/test/dataset/RemoveDataSet.xml";
	private static final String SAVE_DATA_SET = "/test/dataset/SaveDataSet.xml";
	/**
	 * SQL constants
	 */
	private static final String TABLE = "NEWS_TEST";
	private static final String SCHEMA = "OLEG";
	private static final String SELECT_FIRST_NEWS = "SELECT * FROM NEWS WHERE NEWS_ID = 1";
	private static final String SELECT_ALL_NEWS = "SELECT * FROM NEWS ORDER BY NEWS_ID DESC";
	private static final String SELECT_THIRD_NEWS = "SELECT * FROM NEWS WHERE NEWS_ID = 3";
	
	private static final DbUnitAssert ASSERTION = new DbUnitAssert();
	
	@Autowired
	INewsDAO newsDAO;
	
	@BeforeClass
	public static void setUp() throws Exception {
		IDatabaseConnection connection = getConnection();
		try {
			getSetUpOperation().execute(getConnection(), getDataSet());
		} finally {
			connection.close();
		}
	};

	public static IDatabaseConnection getConnection() throws Exception {		
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "oleg", "olegrusak");
		DatabaseConnection databaseConnection = new DatabaseConnection(connection, SCHEMA);
		databaseConnection.getConfig().setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);
		return databaseConnection;
	}

	public static IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(NewsDAOImplTest.class.getResourceAsStream(STARTING_DATA_SET));
	}

	public static DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.CLEAN_INSERT;
	}

	public DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
	
	@Test
	public void testRead() throws Exception {
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(NewsDAOImplTest.class.getResourceAsStream(FETCH_DATA_SET));
		QueryDataSet databaseData = new QueryDataSet(getConnection());
		databaseData.addTable(TABLE, SELECT_FIRST_NEWS);
		ASSERTION.assertEquals(expectedData, databaseData);
	}
	
	@Test 
	public void testNewsList() throws Exception {
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(NewsDAOImplTest.class.getResourceAsStream(STARTING_DATA_SET));
		QueryDataSet databaseData = new QueryDataSet(getConnection());
		databaseData.addTable(TABLE, SELECT_ALL_NEWS);
		ASSERTION.assertEquals(expectedData, databaseData); 
	}

	@Test
	public void testRemoveNews() throws Exception {
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(NewsDAOImplTest.class.getResourceAsStream(DELETE_DATA_SET));
		List<Integer> newsIdList = new ArrayList<Integer>();
		newsIdList.add(1);
		newsIdList.add(2);
		newsDAO.removeNews(newsIdList);
		QueryDataSet databaseData = new QueryDataSet(getConnection());
		databaseData.addTable(TABLE, SELECT_ALL_NEWS);
		ASSERTION.assertEquals(expectedData, databaseData);
	}
	
	@Test
	public void testSaveNews() throws Exception {
		IDataSet expectedData = new FlatXmlDataSetBuilder().build(NewsDAOImplTest.class.getResourceAsStream(SAVE_DATA_SET));
		News news = new News(3, "NewsTitle3", "NewsBrief3", "NewsContent3", Date.valueOf("2001-09-21"));
		newsDAO.saveNews(news);
		QueryDataSet databaseData = new QueryDataSet(getConnection());
		databaseData.addTable(TABLE, SELECT_THIRD_NEWS);
		String[] ignoringCols = {"NEWS_ID"};
		ASSERTION.assertEqualsIgnoreCols(expectedData, databaseData, TABLE, ignoringCols);
	}
}