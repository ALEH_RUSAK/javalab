package com.epam.testapp.resources.constants;

/**
 * Keeps and selects proper static final <code>String</code> value
 * @author Aleh_Rusak
 */
public final class Constants {
	/**
	 * Language constants
	 */
	public static final String ENGLISH_LOCALE = "en";
	public static final String RUSSIAN_LOCALE = "ru";
	/**
	 * Date patterns
	 */
	public static final String EN_DATE_FORMAT = "MM/dd/yyyy";
	public static final String RU_DATE_FORMAT = "dd/MM/yyyy";
	/**
	 * Logger informational constants
	 */
	public static final String ACTION_METHOD_EXECUTED = "method executed.";
	public static final String PAGE_NOT_FOUND = "Page not found.";
	/**
	 * Logger informational constants for <code>NewsDAOHibernate</code> 
	 */
	public static final String SESSION_OPENED = "Session opened.";
	public static final String TRANSACTION_STARTED = "Transation started.";
	public static final String TRANSACTION_COMMITED = "Transation commited.";
	public static final String SESSION_CLOSED = "Session closed.";
	
	private Constants() {}
}