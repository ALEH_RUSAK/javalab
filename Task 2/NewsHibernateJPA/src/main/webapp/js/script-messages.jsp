<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

var DELETE_CONFIRM = '<bean:message key="js.message.delete.confirm"/>';
var DELETE_ALERT = '<bean:message key="js.error.deletion"/>';

var DATE_ALERT = '<bean:message key="js.error.date.format"/>';

var TITLE_REQUIRED = '<bean:message key="js.error.required.title"/>';
var BRIEF_REQUIRED = '<bean:message key="js.error.required.brief"/>';
var CONTENT_REQUIRED = '<bean:message key="js.error.required.content"/>';
var DATE_REQUIRED = '<bean:message key="js.error.required.date"/>';

var TITLE_LENGTH = '<bean:message key="js.error.length.title"/>';
var BRIEF_LENGTH = '<bean:message key="js.error.length.brief"/>';
var CONTENT_LENGTH = '<bean:message key="js.error.length.content"/>';
var DATE_LENGTH = '<bean:message key="js.error.length.date"/>';

var DATE_PATTERN = '<bean:message key="js.date.pattern"/>';

