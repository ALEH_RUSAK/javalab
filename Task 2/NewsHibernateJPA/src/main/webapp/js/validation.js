/**
 * Function checks, if user clicked on at least one checkbox
 * to proceed deleting news
 */
function deleteNews() {
	var selectedNews = document.getElementsByName('selectedNews');
	for (var i = 0; i < selectedNews.length; i++) {
		if (selectedNews[i].checked) {
			return confirmDeletion();
		}
	}
	alert(DELETE_ALERT);
	return false;
}

function confirmDeletion() {
	return confirm(DELETE_CONFIRM);
}

function validateFormFields() {
	var errorMessage = '';
	
	if (document.getElementsByName('newsMessage.title')[0].value == '') {
		errorMessage += TITLE_REQUIRED += '\n';
	}
	if (document.getElementsByName('newsMessage.brief')[0].value == '') {
		errorMessage += BRIEF_REQUIRED += '\n';
	} 
	if (document.getElementsByName('newsMessage.content')[0].value == '') {
		errorMessage += CONTENT_REQUIRED += '\n';
	} 
	if (document.getElementsByName('stringDate')[0].value == '') {
		errorMessage += DATE_REQUIRED += '\n';
	} 
	
	var titleLength = document.getElementsByName('newsMessage.title')[0].value.length;
	var briefLength = document.getElementsByName('newsMessage.brief')[0].value.length;
	var contentLength = document.getElementsByName('newsMessage.content')[0].value.length;
	var dateLength = document.getElementsByName('stringDate')[0].value.length;
	
	if (titleLength >= 100) {
		errorMessage += TITLE_LENGTH += '\n';
	}
	
	if (briefLength >= 500) {
		errorMessage += BRIEF_LENGTH += '\n';
	} 
	
	if (contentLength >= 2048) {
		errorMessage += CONTENT_LENGTH += '\n';
	} 
	
	if (dateLength >= 15) {
		errorMessage += DATE_LENGTH += '\n';
	} 
	
	var stringDate = document.getElementsByName('stringDate')[0].value;
	var datePattern = new RegExp(DATE_PATTERN);	
	var flag = datePattern.test(stringDate);
	if (!flag) {
		errorMessage += DATE_ALERT += '\n';
	} 
	
	if(errorMessage.length > 0) {
		alert(errorMessage);
		return false;
	} else {
		return true;
	}
}

