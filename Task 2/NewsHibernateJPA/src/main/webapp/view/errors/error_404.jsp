<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Not found</title>
	</head>
	<body>
		<h1 align=center>
			<bean:message key="error404.title" />
		</h1>
		<h3 align=center>
			<bean:message key="error404.message" />
		</h3>
	</body>
</html>
