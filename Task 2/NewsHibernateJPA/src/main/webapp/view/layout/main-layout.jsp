<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <META content="text/html; charset=windows-1252" http-equiv="Content-Type">
        <title>News</title>
        <link href="view/style/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
    	<tiles:insert attribute="header" />
    	<div class="menu_body">
	    	<tiles:insert attribute="menu" /> 
	        <tiles:insert attribute="body" /> 
    	</div>
        <tiles:insert attribute="footer" />
    </body>
</html>