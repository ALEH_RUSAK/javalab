<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<div class="page_body">
	<div class="left_side">
		<div class="menu">
			<p class="menu_head">
				<bean:message key="menu.title" />
			</p>
			<div class="navigation">
				<span>
					<img src="imgs/menu-image.png">
					<html:link page="/newsList.do">
						<bean:message key="menu.list" />
					</html:link>
				</span>
				<span>
					<img src="imgs/menu-image.png"> 
					<html:link page="/addNews.do">
						<bean:message key="menu.add" />
					</html:link>
				</span>
			</div>
		</div>
	</div>
</div>