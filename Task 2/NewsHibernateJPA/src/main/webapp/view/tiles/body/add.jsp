<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script src="js/script-messages.jsp" type="text/javascript"></script>
<script src="js/validation.js" type="text/javascript"></script>

<div class="right_side">
	<div class="navigation_links">
		<html:link href="newsList.do">
			<bean:message key="link.main" />
		</html:link>
		>>
		<bean:message key="link.add" />
	</div>
	
	<html:form action="/saveNews" onsubmit="return validateFormFields()">
		<div class="view_page">
			<div class="valid_errors">
				<html:errors/>
			</div>
			<nested:nest property="newsMessage">
				<div class="block">
					<div class="head">
						<bean:message key="label.news.title" />:
					</div>
					<div class="elem">
						<nested:text property="title" size="70" styleClass="title_area" />
					</div>
				</div>
				
				<div class="block">
					<div class="head">
						<bean:message key="label.news.date" />:
					</div>
					<div class="elem"> 
						<html:text property="stringDate" maxlength="15" styleClass="date_area" />
					</div>
				</div>
				
				<div class="block">
					<div class="head">
						<bean:message key="label.news.brief" />:
					</div>
					<div class="brief">
						<nested:textarea rows="5" cols="45" property="brief" styleClass="breif_area" />
					</div>
				</div>
				
				<div class="block">
					<div class="head">
						<bean:message key="label.news.content" />:
					</div>
					<div class="content">
						<nested:textarea rows="5" cols="45" property="content" styleClass="content_area" />
					</div>
				</div>
			</nested:nest>
		</div>
		
		<div class="block save_message">
			<div class="button_area">
				<html:submit styleId="button_style">
					<bean:message key="button.save" />
				</html:submit>
			</div>
		</div>
	</html:form>
		
	<div class="block cancel_message">
		<div class="button_area">
			<html:form action="/cancel">
				<html:submit styleId="button_style">
					<bean:message key="button.cancel" />
				</html:submit>
			</html:form>
		</div>
	</div>
</div>
<div class="clearfix"></div>