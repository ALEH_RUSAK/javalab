package com.epam.testapp.database.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;

/**
 * DAO implementation using Hibernate Framework
 * @author Aleh_Rusak
 */
public class NewsDAOHibernate implements INewsDAO {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(NewsDAOHibernate.class);
	/**
	 * <code>SessionFactory</code> field
	 */
	private SessionFactory sessionFactory;
	/**
	 * HQL constant query
	 */
	private static final String HQL_DELETE_NEWS = "deleteNews";
	private static final String HQL_NEWS_ID_LIST = "newsIdList";
	private static final String HQL_NEWS_ORDER_BY = "date";

	/**
	 * Method provides getting all <code>News</code>
	 * @return the <code>List</code> of <code>News</code>
	 * @throws DAOException 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<News> getNewsList() throws DAOException {
		List<News> newsList = null;
		Session session = null;
		Transaction transaction = null;
		try {
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			newsList = (List<News>) session.createCriteria(News.class)
					.addOrder(Order.desc(HQL_NEWS_ORDER_BY)).list();
			transaction.commit();
		} catch(HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			logger.error(e);
			throw new DAOException(e.getMessage());
		} 
		return newsList;
	}

	/**
	 * Method provides saving new <code>News</code>
	 * into the database
	 * @return generated id
	 * @throws DAOException
	 */
	@Override
	public int saveNews(News newsMessage) throws DAOException {
		Session session = null;
		Transaction transaction = null;
		try {
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			if(newsMessage.getNewsId() == 0) {
				session.save(newsMessage);
			} else {
				session.update(newsMessage);
			}
			transaction.commit();
		} catch(HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			logger.error(e);
			throw new DAOException(e.getMessage());
		} 
		return newsMessage.getNewsId();
	}

	/**
	 * Method provides removing <code>News</code>
	 * @param newsIdArray the array of <code>News</code>
	 * id's of <code>News</code> that should be removed
	 * @throws DAOException 
	 */
	@Override
	public void removeNews(List<Integer> newsIdList) throws DAOException {
		Session session = null;
		Transaction transaction = null;
		try {
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			Query deletionQuery = session.getNamedQuery(HQL_DELETE_NEWS);
			deletionQuery.setParameterList(HQL_NEWS_ID_LIST, newsIdList);
			deletionQuery.executeUpdate();
			transaction.commit();
		} catch(HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
	}

	/**
	 * Method provides getting <code>News</code> by it's id
	 * @param newsId the id of the <code>News</code>
	 * @return <code>News</code> instance
	 * @throws DAOException 
	 */
	@Override
	public News fetchNewsById(int newsId) throws DAOException {
		News news = null;
		Session session = null;
		Transaction transaction = null;
		try {
			session = sessionFactory.getCurrentSession();
			transaction = session.beginTransaction();
			news = (News) session.get(News.class, newsId);
			transaction.commit();
		} catch(HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
		return news;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
