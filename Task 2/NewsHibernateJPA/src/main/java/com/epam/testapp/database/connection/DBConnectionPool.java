package com.epam.testapp.database.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.epam.testapp.exceptions.ConnectionException;

/**
 * Class provides implementation 
 * for getting and releasing connections from pool and into it
 * @author Aleh_Rusak
 */
public final class DBConnectionPool {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(DBConnectionPool.class);
	/**
	 * Logger informational constants
	 */
	private static final String EX_MSG_CONNECTION_NULL = "Attempting to put null into the pool";
	private static final String DRIVER_REGISTERED_SUCCESSFULLY = "Driver registered successfully: ";
	private static final String CONNECTION_CREATED = "Connection created.";
	private static final String POOL_HAS_BEEN_CREATED = "Pool has been created.";
	private static final String CONNECTION_RECIEVED_FROM_POOL = "Connection recieved from pool.";
	private static final String CONNECTION_RETURNED_TO_POOL = "Connection returned to pool.";
	private static final String CONNECTION_CLOSED = "Connection closed.";
	private static final String CONNECTION_POOL_IS_CLEAR = "Pool has been destroyed.";
	/**
	 * Required fields to initialize pool
	 */
	private String driverName;
	private String url;
	private String userName;
	private String password;
	private int maxConnections;
	/**
	 * Concurrent collection for keeping connections
	 */
	private BlockingQueue<Connection> allConnections;
	/**
	 * Concurrent collection for keeping given connections
	 */
	private BlockingQueue<Connection> givenConnections;
	/**
	 * Method initializes the ConnectionPool
	 * @return connectionPool pool's instance
	 * @throws ConnectionException 
	 */
	public void initPool() throws ConnectionException {
		Locale.setDefault(Locale.ENGLISH);
		allConnections = new ArrayBlockingQueue<Connection>(maxConnections);
		givenConnections = new ArrayBlockingQueue<Connection>(maxConnections);
	    try {
			Class.forName(driverName);
			if(logger.isInfoEnabled()) {
				logger.info(DRIVER_REGISTERED_SUCCESSFULLY + driverName);
			}
			for(int i = 0; i < maxConnections; i++) {
				allConnections.add(DriverManager.getConnection(url, userName, password));
				logger.info(CONNECTION_CREATED);
			}
			logger.info(POOL_HAS_BEEN_CREATED);
		} catch (ClassNotFoundException | SQLException e) {
			logger.error(e);
			throw new ConnectionException(e.getMessage());
		}
	}
	
	/**
	 * Method getConnection() is used to get a connection from the queue
	 * @return connection from the queue
	 * @throws ConnectionException 
	 */
	public Connection getConnection() throws ConnectionException {
		try {
			Connection connection = allConnections.poll(3, TimeUnit.SECONDS);
			givenConnections.add(connection);
			logger.info(CONNECTION_RECIEVED_FROM_POOL);
			return connection;
    	} catch (InterruptedException e) {
    		logger.error(e);
            throw new ConnectionException(e.getMessage());
        } 
    }

	/**
	 * Method is used to put connection into the connections collection
	 * @param connection - the <code>Connection</code> instance to be returned into pool
	 * @throws ConnectionException 
	 */
    public void releaseConnection(Connection connection) throws ConnectionException {
		try {
			if(connection != null) {
				givenConnections.remove(connection);
				allConnections.put(connection);
				logger.info(CONNECTION_RETURNED_TO_POOL);
			} else {
				throw new ConnectionException(EX_MSG_CONNECTION_NULL);
			}
		} catch (InterruptedException e) {
			logger.error(e);
			throw new ConnectionException(e.getMessage());
		}
    }
    
    /**
     * Method provides destroying connection pool
     * That means to close all connections 
     * and clear allConnections collection
     * @throws ConnectionException 
     */
    public void destroyPool() throws ConnectionException {
    	try {
    		for(Connection connection: allConnections) {
        		connection.close();
        		logger.info(CONNECTION_CLOSED);
        	}
			for(Connection connection: givenConnections) {
				connection.close();
			}
    		allConnections.clear();
			givenConnections.clear();
    		logger.info(CONNECTION_POOL_IS_CLEAR);
    	} catch (SQLException e) {
    		logger.error(e);
    		throw new ConnectionException(e.getMessage());
    	}
    }

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getDriverName() {
		return driverName;
	}

	public String getUrl() {
		return url;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}
	
	public int getMaxConnections() {
		return maxConnections;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

}
