package com.epam.testapp.exceptions;

/**
 * Thrown when <code>NewsDAOImpl</code> method execution is incorrect
 * @author Aleh_Rusak
 */
public class DAOException extends BaseException {
	
	private static final long serialVersionUID = 1L;

	public DAOException() {}
	
	public DAOException(String exceptionMessage) {
		this.code = 3;
		this.title = DAOException.class.toString();
		this.exceptionMessage = exceptionMessage;
	}
}
