package com.epam.testapp.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.epam.testapp.database.connection.DBConnectionPool;
import com.epam.testapp.exceptions.ConnectionException;
import com.epam.testapp.exceptions.DAOException;

/**
 * Class provides management for JDBC resources such as <code>Statement</code>,
 * <code>Connection</code> and so on.
 * @author Aleh_Rusak
 */
public final class DAOResourceManager {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(DAOResourceManager.class);

	/**
	 * Method closes all opened JDBC objects 
	 * and releases connection into the pool
	 * @throws DAOException
	 */
	public static void closeJdbcResources(DBConnectionPool connectionPool,
			PreparedStatement preparedStatement, ResultSet resultSet,
			Connection connection) throws DAOException {
		try {
			connectionPool.releaseConnection(connection);
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException | ConnectionException e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
	}
	
	/**
	 * Method closes all opened JDBC objects 
	 * and releases connection into the pool
	 * @throws DAOException
	 */
	public static void closeJdbcResources(DBConnectionPool connectionPool,
			Statement statement, ResultSet resultSet,
			Connection connection) throws DAOException {
		try {
			connectionPool.releaseConnection(connection);
			if (statement != null) {
				statement.close();
			}
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException | ConnectionException e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
	}
}
