package com.epam.testapp.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The declaration of News model
 * of the application
 * @author Aleh_Rusak
 */
@Entity 
@Table(name = "NEWS")
@NamedQueries({
	@NamedQuery(name="deleteNews", query="DELETE FROM News news WHERE news.newsId IN (:newsIdList)"),
	@NamedQuery(name="selectAllNews", query="SELECT news FROM News news") 
})
public class News implements Serializable {
	
	private static final long serialVersionUID = 1L;
	/**
	 * News Id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "NEWS_ID", nullable = false)
	private int newsId;
	/**
	 * News title
	 */
	@Column(name = "TITLE", nullable = false)
	private String title;	
	/**
	 * News date of publishing
	 */
	@Column(name = "NEWS_DATE", nullable = false)
	private Date date;
	/**
	 * News brief
	 */
	@Column(name = "BRIEF", nullable = false)
	private String brief;
	/**
	 * News content
	 */
	@Column(name = "CONTENT", nullable = false)
	private String content;
	
	public News() {}
	
	public News(int newsId, String title, String brief, String content, Date date) {
		this.newsId = newsId;
		this.title = title;
		this.date = date;
		this.brief = brief;
		this.content = content;
	}
	
	public int hashCode() {
		int code = 11 * newsId;
		code += ((title != null) ? title.hashCode() : 0);
		code += ((date != null) ? date.hashCode() : 0);
		code += ((brief != null) ? brief.hashCode() : 0);
		code += ((content != null) ? content.hashCode() : 0);
		return (int) code;
	}
	
	public boolean equals(Object anObject) {
		if(this == anObject) {
			return true;
		}
		if(anObject == null) {
			return false;
		}
		if(getClass() == anObject.getClass()) {
			News temp = (News) anObject;
			return newsId == temp.newsId
				&& title.equals(temp.title)
				&& date.equals(temp.date)
				&& brief.equals(temp.brief)
				&& content.equals(temp.content);
		} else {
			return false;
		}
	}
	
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("News { newsId = ");
		strBuilder.append(newsId);
		strBuilder.append(", title = ");
		strBuilder.append(title);
		strBuilder.append(", date = ");
		strBuilder.append(date.toString());
		strBuilder.append(", brief = ");
		strBuilder.append(brief);
		strBuilder.append(", content = ");
		strBuilder.append(content);
		strBuilder.append(" }");
		return strBuilder.toString();
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getBrief() {
		return brief;
	}
	
	public void setBrief(String brief) {
		this.brief = brief;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public int getNewsId() {
		return newsId;
	}

	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}
}
