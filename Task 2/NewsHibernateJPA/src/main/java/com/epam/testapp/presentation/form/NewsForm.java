package com.epam.testapp.presentation.form;

import java.util.List;

import org.apache.struts.validator.ValidatorActionForm;

import com.epam.testapp.model.News;
import com.epam.testapp.resources.constants.Constants;

/**
 * Form declaration
 * @author Aleh_Rusak
 */
public final class NewsForm extends ValidatorActionForm {
	
	private static final long serialVersionUID = 1L;
	/**
	 * <code>News</code> list
	 */
	private List<News> newsList;
	/**
	 * Concrete <code>News</code> object
	 */
	private News newsMessage;
	/**
	 * Form's locale
	 */
	private String locale;
	/**
	 * Form's date's string representation
	 */
	private String stringDate;
	/**
	 * Form's date pattern depending on locale
	 */
	private String dateFormat;
	/**
	 * An array that contains selected boxes
	 */
	private Integer[] selectedNews;
	
	public NewsForm()
	{
	    newsMessage = new News();
	    /* Constructing default form we use default locale */
	    this.setLocale(Constants.ENGLISH_LOCALE);
	    this.setDateFormat(Constants.EN_DATE_FORMAT);
	}
	
	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}

	public News getNewsMessage() {
		return newsMessage;
	}

	public void setNewsMessage(News newsMessage) {
		this.newsMessage = newsMessage;
	}

	public Integer[] getSelectedNews() {
		return selectedNews;
	}

	public void setSelectedNews(Integer[] selectedNews) {
		this.selectedNews = selectedNews;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
}
