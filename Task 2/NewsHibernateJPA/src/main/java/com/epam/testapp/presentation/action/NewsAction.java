package com.epam.testapp.presentation.action;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.MappingDispatchAction;

import com.epam.testapp.database.dao.INewsDAO;
import com.epam.testapp.exceptions.ActionException;
import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.exceptions.ForwardException;
import com.epam.testapp.model.News;
import com.epam.testapp.presentation.form.NewsForm;
import com.epam.testapp.resources.constants.Constants;
import com.epam.testapp.util.ForwardManager;

/**
 * The Action-class of the application
 * 
 * @author Aleh_Rusak
 */
public final class NewsAction extends MappingDispatchAction {
	/**
	 * The Logger instance
	 */
	private static Logger logger = Logger.getLogger(NewsAction.class);
	/**
	 * DAO reference
	 */
	private INewsDAO newsDAO;
	/**
	 * Request parameters identifiers
	 */
	private static final String CANCEL_FORWARD = "cancelForward";
	/**
	 * Forward identifiers
	 */
	private static final String FORWARD_PAGE_LIST = "forwardList";
	private static final String FORWARD_PAGE_VIEW = "forwardView";
	private static final String FORWARD_PAGE_ADD = "forwardAdd";
	/**
	 * Redirection identifiers
	 */
	private static final String REDIRECT_LIST = "redirectList";
	private static final String REDIRECT_VIEW = "redirectView";
	/**
	 * Method sets newsDAO
	 */
	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public INewsDAO getNewsDAO() {
		return newsDAO;
	}

	/**
	 * Method provides getting <code>News</code> list
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException
	 * @throws ForwardException 
	 */
	public ActionForward newsList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) 
					throws ActionException, ForwardException {
		try {
			request.getSession().setAttribute(CANCEL_FORWARD, FORWARD_PAGE_LIST);
			NewsForm newsForm = (NewsForm) form;
			newsForm.setNewsList(newsDAO.getNewsList());
			logger.info(Constants.ACTION_METHOD_EXECUTED);
		} catch (DAOException e) {
			logger.error(e);
			throw new ActionException(e.getMessage());
		}
		return ForwardManager.getForward(mapping, FORWARD_PAGE_LIST);
	}

	/**
	 * Method enables to view <code>News</code>
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException
	 * @throws ForwardException 
	 */
	public ActionForward viewNews(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ActionException, ForwardException {
		try {
			request.getSession().setAttribute(CANCEL_FORWARD, FORWARD_PAGE_VIEW);
			NewsForm newsForm = (NewsForm) form;
			int newsId = newsForm.getNewsMessage().getNewsId();
			News newsMessage = newsDAO.fetchNewsById(newsId);
			newsForm.setNewsMessage(newsMessage);
			logger.info(Constants.ACTION_METHOD_EXECUTED);
		} catch (DAOException e) {
			logger.error(e);
			throw new ActionException(e.getMessage());
		}
		return ForwardManager.getForward(mapping, FORWARD_PAGE_VIEW);
	}

	/**
	 * Method deletes selected <code>News</code>.
	 * Launches on news_list.jsp
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException
	 * @throws ForwardException 
	 */
	public ActionForward multipleDeletion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws ActionException, ForwardException {
		ArrayList<Integer> newsToDelete = new ArrayList<Integer>();
		try {
			NewsForm newsForm = (NewsForm) form;
			Integer[] selectedNews = newsForm.getSelectedNews();
			for (Integer selected : selectedNews) {
				newsToDelete.add(selected);
			}
			newsDAO.removeNews(newsToDelete);
			newsForm.setNewsList(newsDAO.getNewsList());
			logger.info(Constants.ACTION_METHOD_EXECUTED);
		} catch (DAOException e) {
			logger.error(e);
			throw new ActionException(e.getMessage());
		}
		return ForwardManager.getForward(mapping, REDIRECT_LIST);
	}
	
	/**
	 * Method deletes <code>News</code>.
	 * Launches on news_view.jsp
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException
	 * @throws ForwardException 
	 */
	public ActionForward singleDeletion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws ActionException, ForwardException {
		ArrayList<Integer> newsToDelete = new ArrayList<Integer>();
		NewsForm newsForm = (NewsForm) form;
		try {
			newsToDelete.add(newsForm.getNewsMessage().getNewsId());
			newsDAO.removeNews(newsToDelete);
			newsForm.setNewsList(newsDAO.getNewsList());
			logger.info(Constants.ACTION_METHOD_EXECUTED);
		} catch (DAOException e) {
			logger.error(e);
			throw new ActionException(e.getMessage());
		}
		return ForwardManager.getForward(mapping, REDIRECT_LIST);
	}

	/**
	 * Method saves edited <code>News</code>
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException
	 * @throws ForwardException 
	 * @throws ParseException 
	 */
	public ActionForward saveNews(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) 
					throws ActionException, ForwardException {
		try {
			NewsForm newsForm = (NewsForm) form;
			News newsMessage = newsForm.getNewsMessage();
			
			newsMessage.setTitle(newsMessage.getTitle().trim());
			newsMessage.setBrief(newsMessage.getBrief().trim());
			newsMessage.setContent(newsMessage.getContent().trim());

			/* Converting string date format into sql date format */
			SimpleDateFormat sqlFormat = new SimpleDateFormat(newsForm.getDateFormat());
			java.util.Date parsedDate = sqlFormat.parse(newsForm.getStringDate());
			newsMessage.setDate(new Date(parsedDate.getTime()));

			int newsId = newsDAO.saveNews(newsMessage);
			newsForm.setNewsMessage(newsDAO.fetchNewsById(newsId));
			
			logger.info(Constants.ACTION_METHOD_EXECUTED);
		} catch (DAOException | ParseException e) {
			logger.error(e);
			throw new ActionException(e.getMessage());
		}
		return ForwardManager.getForward(mapping, REDIRECT_VIEW);
	}

	/**
	 * Method edits <code>News</code>
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException
	 * @throws ForwardException 
	 */
	public ActionForward editNews(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws ActionException, ForwardException {
		try {
			
			NewsForm newsForm = (NewsForm) form;
			int newsId = newsForm.getNewsMessage().getNewsId();
			newsForm.setNewsMessage(newsDAO.fetchNewsById(newsId));
			
			/* Formatting current date using SimpleDateFormat class */
			Date currentDate = newsForm.getNewsMessage().getDate();
			SimpleDateFormat localedDateFormat = new SimpleDateFormat(newsForm.getDateFormat());
			newsForm.setStringDate(localedDateFormat.format(currentDate));
			
			logger.info(Constants.ACTION_METHOD_EXECUTED);
		} catch (DAOException e) {
			logger.error(e);
			throw new ActionException(e.getMessage());
		}
		return ForwardManager.getForward(mapping, FORWARD_PAGE_ADD);
	}

	/**
	 * Method adds new <code>News</code> instance
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException 
	 * @throws ForwardException 
	 */
	public ActionForward addNews(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws ActionException, ForwardException {
		NewsForm newsForm = (NewsForm) form;
		request.getSession().setAttribute(CANCEL_FORWARD, FORWARD_PAGE_LIST);
		/* Formatting current date using SimpleDateFormat class */
		Date currentDate = new Date(System.currentTimeMillis());
		SimpleDateFormat localedDateFormat = new SimpleDateFormat(newsForm.getDateFormat());
		newsForm.setStringDate(localedDateFormat.format(currentDate));
		
		News newsMessage = newsForm.getNewsMessage();
		newsMessage.setNewsId(0);
		newsMessage.setBrief(null);
		newsMessage.setContent(null);
		newsMessage.setTitle(null);
	
		logger.info(Constants.ACTION_METHOD_EXECUTED);
		return ForwardManager.getForward(mapping, FORWARD_PAGE_ADD);
	}

	/**
	 * Method cancels user's action
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException
	 * @throws ForwardException 
	 */
	public ActionForward cancel(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ActionException, ForwardException {
		
		NewsForm newsForm = (NewsForm) form;
		News newsMessage = newsForm.getNewsMessage();
		if (newsMessage.getNewsId() != 0) {
			try {
				newsForm.setNewsMessage(newsDAO.fetchNewsById(newsMessage.getNewsId()));
				logger.info(Constants.ACTION_METHOD_EXECUTED);
			} catch (DAOException e) {
				logger.error(e);
				throw new ActionException(e.getMessage());
			}
		}
		return ForwardManager.getForward(mapping, request.getSession().getAttribute(CANCEL_FORWARD).toString());
	}

	/**
	 * Method changes the language of interface
	 * @param mapping ActionMapping instance
	 * @param form ActionForm instance
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponce instance
	 * @return next page provided by ActionMapping class
	 * @throws ActionException 
	 * @throws ForwardException 
	 */
	public ActionForward changeInterfaceLanguage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request, HttpServletResponse response)
					throws ActionException, ForwardException {
		
		NewsForm newsForm = (NewsForm) form;
		Locale locale = new Locale(newsForm.getLocale(), newsForm.getLocale());
		
		request.getSession().setAttribute(Globals.LOCALE_KEY, locale);
		if ((Constants.RUSSIAN_LOCALE).equals(newsForm.getLocale())) {
			newsForm.setDateFormat(Constants.RU_DATE_FORMAT);
		} else if ((Constants.ENGLISH_LOCALE).equals(newsForm.getLocale())) {
			newsForm.setDateFormat(Constants.EN_DATE_FORMAT);
		}	
		logger.info(Constants.ACTION_METHOD_EXECUTED);
		return ForwardManager.getForward(mapping, REDIRECT_LIST);
	}
}
