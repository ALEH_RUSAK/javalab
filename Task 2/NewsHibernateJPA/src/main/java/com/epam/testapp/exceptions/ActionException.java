package com.epam.testapp.exceptions;

/**
 * Thrown when <code>NewsAction</code> method execution is incorrect
 * @author Aleh_Rusak
 */
public class ActionException extends BaseException {
	
	private static final long serialVersionUID = 1L;
	
	public ActionException() {}

	public ActionException(String exceptionMessage) {
		this.code = 1;
		this.title = ActionException.class.toString();
		this.exceptionMessage = exceptionMessage;
	}
}
