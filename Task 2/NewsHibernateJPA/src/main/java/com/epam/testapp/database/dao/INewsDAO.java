package com.epam.testapp.database.dao;

import java.util.List;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;

/**
 * Provides description of DAO methods
 * @author Aleh_Rusak
 */
public interface INewsDAO {
	/**
	 * Method provides getting all <code>News</code>
	 * @return the <code>List</code> of <code>News</code>
	 * @throws DAOException 
	 */
	public List<News> getNewsList() throws DAOException;
	
	/**
	 * Method provides saving new <code>News</code>
	 * into the database
	 * @return generated id
	 * @throws DAOException
	 */
	public int saveNews(News newsMessage) throws DAOException;
	
	/**
	 * Method provides removing <code>News</code>
	 * @param newsIdArray the array of <code>News</code>
	 * id's that should be removed
	 * @throws DAOException 
	 */
	public void removeNews(List<Integer> newsIdList) throws DAOException;
	
	/**
	 * Method provides getting <code>News</code>
	 * by it's id
	 * @param newsId the id of the <code>News</code>
	 * @return 
	 * @throws DAOException 
	 */
	public News fetchNewsById(int newsId) throws DAOException;
}
