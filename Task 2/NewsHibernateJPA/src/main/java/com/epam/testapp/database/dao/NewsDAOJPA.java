package com.epam.testapp.database.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;

/**
 * DAO implementation using Java Persistence API
 * @author Aleh_Rusak
 */
@Transactional
public class NewsDAOJPA implements INewsDAO {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(NewsDAOJPA.class);
	/**
	 * EntityManager field
	 */
	@PersistenceContext
	private EntityManager entityManager;
	/**
	 * JPA queries
	 */
	private static final String JPQL_SELECT_ALL = "selectAllNews";
	private static final String JPQL_DELETE_NEWS = "deleteNews";
	private static final String JPQL_NEWS_ID_LIST = "newsIdList";
	/**
	 * Method provides getting all <code>News</code>
	 * @return the <code>List</code> of <code>News</code>
	 * @throws DAOException 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<News> getNewsList() throws DAOException {
		List<News> newsList = null;
		try {
			Query selectionQuery = entityManager.createNamedQuery(JPQL_SELECT_ALL);
			newsList = (List<News>) selectionQuery.getResultList();
		} catch(Exception e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
		return newsList;
	}

	/**
	 * Method provides saving new <code>News</code>
	 * into the database
	 * @return generated id
	 * @throws DAOException
	 */
	@Override
	public int saveNews(News newsMessage) throws DAOException {
		try {
			News updatedNews = entityManager.merge(newsMessage);
			newsMessage.setNewsId(updatedNews.getNewsId());
		} catch(Exception e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
		return newsMessage.getNewsId();
	}

	/**
	 * Method provides removing <code>News</code>
	 * @param newsIdArray the list of <code>News</code>
	 * id's of <code>News</code> that are going to be removed
	 * @throws DAOException 
	 */
	@Override
	public void removeNews(List<Integer> newsIdList) throws DAOException {
		try {
			Query deletionQuery = entityManager.createNamedQuery(JPQL_DELETE_NEWS); 
			deletionQuery.setParameter(JPQL_NEWS_ID_LIST, newsIdList);
			deletionQuery.executeUpdate();
		} catch (Exception e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
	}

	/**
	 * Method provides getting <code>News</code> by it's id
	 * @param newsId the id of the <code>News</code>
	 * @return <code>News</code> instance
	 * @throws DAOException 
	 */
	@Override
	public News fetchNewsById(int newsId) throws DAOException {
		News newsMessage = null;
		try {
			newsMessage = entityManager.find(News.class, newsId);
		} catch (Exception e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		}
		return newsMessage;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
