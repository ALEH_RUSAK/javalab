package com.epam.testapp.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.testapp.database.connection.DBConnectionPool;
import com.epam.testapp.exceptions.ConnectionException;
import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;
import com.epam.testapp.util.DAOResourceManager;

/**
 * The DAO implementation for <code>News</code>
 * @author Aleh_Rusak
 */
public class NewsDAOImpl implements INewsDAO {
	/**
	 * The Logger
	 */
	private static Logger logger = Logger.getLogger(NewsDAOImpl.class);
	/**
	 * Implemented manually connection pool
	 */
	private DBConnectionPool connectionPool;
	/**
	 * Required informational constants
	 */
	private static final String NEWS_ID = "NEWS_ID";
	/**
	 * SQL commands
	 */
	private static final String SQL_INSERT_NEWS = "INSERT INTO NEWS (TITLE, BRIEF, CONTENT, NEWS_DATE) VALUES (?,?,?,?)";
	private static final String SQL_SELECT_NEWS_BY_ID = "SELECT NEWS_ID, TITLE, BRIEF, CONTENT, NEWS_DATE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_SELECT_ALL_NEWS = "SELECT NEWS_ID, TITLE, BRIEF, CONTENT, NEWS_DATE FROM NEWS ORDER BY NEWS_DATE DESC";
	private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ? , BRIEF = ? , CONTENT = ? ,  NEWS_DATE = ?  WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID IN ";
	
	/**
	 * Method initializes statement with values
	 * from <code>News</code> object
	 * @throws SQLException 
	 */
	private void initStatement(News newsMessage, PreparedStatement preparedStatement)
			throws SQLException {
		preparedStatement.setString(1, newsMessage.getTitle());
		preparedStatement.setString(2, newsMessage.getBrief());
		preparedStatement.setString(3, newsMessage.getContent());
		preparedStatement.setDate(4, newsMessage.getDate());
	}
	
	/**
	 * Method gets <code>ResultSet</code> values
	 * and initializes <code>News</code> object with it
	 * @throws SQLException 
	 * @throws ParseException 
	 */
	private News createNews(ResultSet resultSet) throws SQLException {
		News news = new News();
		news.setNewsId(resultSet.getInt(1));
		news.setTitle(resultSet.getString(2));
		news.setBrief(resultSet.getString(3));
		news.setContent(resultSet.getString(4));
		news.setDate(resultSet.getDate(5));
		return news;
	}
	
	/**
	 * Method builds a <code>String</code> to add it into 
	 * the sql query of deletion
	 * @param newsIdList the list of id's
	 * @return <code>String</code> to be added into the deletion query
	 */
	private String prepareDeletionQuery(List<Integer> newsIdList) {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("(");
		for(int index = 0; index < newsIdList.size(); index++) {
			strBuilder.append(newsIdList.get(index).toString());
			if(index != newsIdList.size() - 1) {
				strBuilder.append(", ");
			} else {
				strBuilder.append(")");
			}
		}
		return strBuilder.toString();
	}
	
	/* For all implemented methods below @see INewsDAO description */
	
	@Override
	public int saveNews(News newsMessage) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionPool.getConnection();
			/* Using prepareStatement() method to tell 
			 * database engine, that it has to generate new key
			 * at NEWS_ID column. So if newsId of newsMessage
			 * is 0, then it this newsMessage is new and a key 
			 * has to be generated properly 
			 */
			if(newsMessage.getNewsId() == 0) {
				preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS, new String[] {NEWS_ID});
				initStatement(newsMessage, preparedStatement);
				preparedStatement.execute();
				resultSet = preparedStatement.getGeneratedKeys();
				resultSet.next();
				return resultSet.getInt(1);
			} else {
				preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
				initStatement(newsMessage, preparedStatement);
				preparedStatement.setInt(5, newsMessage.getNewsId());
				preparedStatement.executeUpdate();
				return newsMessage.getNewsId();
			}
		} catch (SQLException | ConnectionException e) {
			throw new DAOException(e.getMessage());
		} finally {
			DAOResourceManager.closeJdbcResources(connectionPool, preparedStatement, resultSet, connection);
		}
	}

	@Override
	public void removeNews(List<Integer> newsIdList) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			String deletionQueryAddon = prepareDeletionQuery(newsIdList);
			connection = connectionPool.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS + deletionQueryAddon);
			preparedStatement.executeUpdate();
		} catch (ConnectionException | SQLException e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		} finally {
			DAOResourceManager.closeJdbcResources(connectionPool, preparedStatement, resultSet, connection);
		}
	}

	@Override
	public List<News> getNewsList() throws DAOException {
		Connection connection = null;
		Statement statement = null;
	    ResultSet resultSet = null;
	    
	    ArrayList<News> newsList = new ArrayList<News>();
	    try {
			connection = connectionPool.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQL_SELECT_ALL_NEWS);
			while(resultSet.next()) {
				News news = createNews(resultSet);
				newsList.add(news);
			}
		} catch (ConnectionException | SQLException e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		} finally {
			DAOResourceManager.closeJdbcResources(connectionPool, statement, resultSet, connection);
		}
		return newsList;
	}

	@Override
	public News fetchNewsById(int newsId) throws DAOException {
		Connection connection = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    try {
			connection = connectionPool.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_ID);
			preparedStatement.setInt(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				return createNews(resultSet);
			} else {
				return new News();
			}
		} catch (ConnectionException | SQLException e) {
			logger.error(e);
			throw new DAOException(e.getMessage());
		} finally {
			DAOResourceManager.closeJdbcResources(connectionPool, preparedStatement, resultSet, connection);
		}
	}
	
	public DBConnectionPool getConnectionPool() {
		return connectionPool;
	}

	public void setConnectionPool(DBConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}
}
