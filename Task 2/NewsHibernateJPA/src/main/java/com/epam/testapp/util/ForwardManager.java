package com.epam.testapp.util;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.epam.testapp.exceptions.ForwardException;
import com.epam.testapp.resources.constants.Constants;

/**
 * Class provides validation of forwarding
 * @author Aleh_Rusak
 */
public final class ForwardManager {
	/**
	 * The Logger
	 */
	private static Logger logger = Logger.getLogger(ForwardManager.class);
	
	private ForwardManager() {}
	
	/**
	 * Method provides validation of forwarding
	 * @param mapping ActionMapping instance
	 * @param paramForward forward parameter
	 * @return found forward page
	 * @throws ForwardException 
	 */
	public static ActionForward getForward(ActionMapping mapping, final String paramForward) throws ForwardException {
		if(paramForward == null || mapping.findForward(paramForward) == null) {
			logger.error(Constants.PAGE_NOT_FOUND);
			throw new ForwardException(Constants.PAGE_NOT_FOUND);
		} 
		return mapping.findForward(paramForward);
	}
}
