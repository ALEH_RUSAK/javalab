package com.epam.webtest.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.lang.model.element.Modifier;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface TypeContentAccess {
	public Modifier access();
}
