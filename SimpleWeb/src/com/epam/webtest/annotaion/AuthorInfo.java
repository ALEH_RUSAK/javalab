/**
 * 
 */
package com.epam.webtest.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Aleh_Rusak
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthorInfo {
	public String id() default "0";
	public String name() default "unknown";
	public String title() default "unknown";
}
