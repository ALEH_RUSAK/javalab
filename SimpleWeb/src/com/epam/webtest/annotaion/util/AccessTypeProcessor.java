package com.epam.webtest.annotaion.util;

import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

import com.epam.webtest.annotaion.TypeContentAccess;

@SupportedAnnotationTypes("TypeContentAccess")
public class AccessTypeProcessor extends AbstractProcessor {

	@Override
	public boolean process(Set<? extends TypeElement> declaredTypeSet, RoundEnvironment roundEnv) {
		for(Element type: roundEnv.getElementsAnnotatedWith(TypeContentAccess.class)) {
			TypeContentAccess accessAnnotation = type.getAnnotation(TypeContentAccess.class);
			List<? extends Element> typeEnclosedElements = type.getEnclosedElements();
			for(Element typeElem: typeEnclosedElements) {
				Set<Modifier> elementModifiers = typeElem.getModifiers();
				if(!elementModifiers.contains(accessAnnotation.access())) {
					String message = typeElem.getKind().toString() + " must be "
							+ accessAnnotation.access() + " only. ";
					processingEnv.getMessager().printMessage(Kind.ERROR, message, typeElem);
				}
			}
		}
		return true;
	}
}
