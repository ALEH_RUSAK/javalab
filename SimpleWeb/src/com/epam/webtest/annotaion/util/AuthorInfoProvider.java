package com.epam.webtest.annotaion.util;

import com.epam.webtest.annotaion.AuthorInfo;

public final class AuthorInfoProvider {
	
	private AuthorInfoProvider() {}

	public static String getAuthorInfo(Class<?> type) {
		if(type.isAnnotationPresent(AuthorInfo.class)) {
			StringBuilder authorBuilder = new StringBuilder();
			AuthorInfo info = type.getAnnotation(AuthorInfo.class);
			authorBuilder.append("Class author: id = ");
			authorBuilder.append(info.id());
			authorBuilder.append(", name = ");
			authorBuilder.append(info.name());
			authorBuilder.append(", title = ");
			authorBuilder.append(info.title());
			return authorBuilder.toString();
		} else {
			return "No information about author found.";
		}
	}
}
