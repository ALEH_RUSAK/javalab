package com.epam.webtest;

import com.epam.webtest.annotaion.modules.JuniorClass;
import com.epam.webtest.annotaion.modules.MiddleClass;
import com.epam.webtest.annotaion.modules.SeniorClass;
import com.epam.webtest.annotaion.util.AuthorInfoProvider;

public class TestClass {
	
	public static void main(String[] args) {
		String classInfo = AuthorInfoProvider.getAuthorInfo(JuniorClass.class);
		System.out.println(classInfo);
		classInfo = AuthorInfoProvider.getAuthorInfo(MiddleClass.class);
		System.out.println(classInfo);
		classInfo = AuthorInfoProvider.getAuthorInfo(SeniorClass.class);
		System.out.println(classInfo);
		
		Integer i = 127;
		Integer j = 127;
		System.out.println(i == j);
		System.out.println(i.equals(j));
	}
}
