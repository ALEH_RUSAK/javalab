package com.epam.webtest;

public class BeanClass {
	
	private String stringProp;
	private int intProp;
	
	public BeanClass() {}

	public String getStringProp() {
		return stringProp;
	}

	public void setStringProp(String stringProp) {
		this.stringProp = stringProp;
	}

	public int getIntProp() {
		return intProp;
	}

	public void setIntProp(int intProp) {
		this.intProp = intProp;
	}
	
}
