package com.epam.webtest.customtag;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class TimeInfoTag extends BodyTagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3537804933408884637L;
	
	private String userMessage;
	
	@Override
	public int doStartTag() throws JspException {
		Calendar calendar = new GregorianCalendar();
		String exactTime = "<hr/>Time: <b>" + calendar.getTime() + "</b><hr/>";
		String locale = "Locale: <b>" + Locale.getDefault() + "</b><hr/>";
		JspWriter writer = pageContext.getOut();
		try {
			writer.write(userMessage);
			writer.write(exactTime + locale);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_BODY_INCLUDE;
	}
	
	@Override
	public int doAfterBody() throws JspException {
		JspWriter writer = pageContext.getOut();
		try {
			writer.write("Processing end.\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
	
	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}
}
