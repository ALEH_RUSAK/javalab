package com.epam.webtest;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	
	static {
		System.out.println("LoginServlet class has been loaded.");
	}
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        System.out.println("LoginServlet instance has been created.");
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie[] loginCookies = request.getCookies();
		String message = "Your credentials are: " 
				+ loginCookies[0].getValue() + loginCookies[1].getValue();
		request.setAttribute("message", message);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*Cookie loginCookie = new Cookie("login", request.getParameter("login"));
		Cookie passwordCookie = new Cookie("password", request.getParameter("password"));
		response.addCookie(loginCookie);
		response.addCookie(passwordCookie);
		Cookie[] loginCookies = request.getCookies();
		String message = "Your credentials are: " 
				+ loginCookies[0].getValue() + loginCookies[1].getValue();
		request.setAttribute("message", message);
		HttpSession session = request.getSession();*/
		/*request.setAttribute("userMessage", request.getParameter("userMessage"));
		*/
		request.setAttribute("login", request.getParameter("login"));
		request.setAttribute("userMessage", "Welcome to web-site with custom tags!");
		RequestDispatcher dispatcher = request.getRequestDispatcher("pages/success.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	@Override
	public void destroy() {
		System.out.println("LoginServlet destroy");
	}
}
