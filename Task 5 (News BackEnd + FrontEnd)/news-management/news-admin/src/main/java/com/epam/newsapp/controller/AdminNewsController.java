package com.epam.newsapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.service.INewsManagerService;
import com.epam.newsapp.util.NavigationDirection;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;
import com.epam.newsapp.valueobject.NewsMessage;

/**
 * Provides connected with news messages administration requests 
 * execution according to REST architecture.
 * @author Aleh_Rusak
 */
@Controller
@RequestMapping(value = "/news")
@SessionAttributes({Constants.AUTHORS_LIST, Constants.TAGS_LIST, Constants.CURRENT_PAGE, 
	Constants.FILTER, Constants.PAGER})
public class AdminNewsController {
	
	private static final Logger logger = Logger.getLogger(AdminNewsController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;
	
	private Pager getPager(ModelMap model) {
		Pager pager = (Pager) model.get(Constants.PAGER);
		if(pager == null) {
			return new Pager(); 
		} else {
			return pager;
		}
	}
	
	private SearchFilter getSearchFilter(ModelMap model) {
		SearchFilter filter = (SearchFilter) model.get(Constants.FILTER);
		if(filter == null) {
			return new SearchFilter(); 
		} else {
			return filter;
		}
	}
	
	/**
	 * Provides forwadding to the news-list.jsp page with the list of news.
	 * Selects news messages to be displayed using pagination.
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with the list of news
	 */
	@RequestMapping(value = "/list/{currentPage}", method = RequestMethod.GET)
    public String viewNewsMessageList(@PathVariable Integer currentPage, ModelMap model) {
        try {
        	Pager pager = this.getPager(model);
        	SearchFilter filter = this.getSearchFilter(model);
        	model.addAttribute(Constants.CURRENT_PAGE, currentPage);
        	model.addAttribute(Constants.FILTER, filter);
        	pager.setCurrentPage(currentPage);
        	Long numberOfNews = newsManagerService.getNumberOfFilteredNews(filter);
        	List<NewsMessage> newsMessageList = newsManagerService.getNewsOnPageUseFilter(pager, filter);
        	if(numberOfNews > pager.getItemsPerPage()) {
        		model.addAttribute(Constants.NUMBER_OF_PAGES, pager.getNumberOfPages(numberOfNews));
        	}
        	model.addAttribute(Constants.NEWS_LIST, newsMessageList);
        	model.addAttribute(Constants.AUTHORS_LIST, newsManagerService.getAllAuthors());
        	model.addAttribute(Constants.TAGS_LIST, newsManagerService.getAllTags());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.FORWARD_NEWS_LIST;
    }
	
	/**
	 * Provides forwadding to the news-view.jsp page with concrete news to view
	 * @param newsId the id of the news
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with where news can be viewed
	 */
	@RequestMapping(value = "/view/{newsId}", method = RequestMethod.GET)
    public String viewSingleNewsMessage(@PathVariable String newsId, ModelMap model) {
        try {
        	Long concreteNewsId = Long.valueOf(newsId);
        	model.addAttribute(Constants.CONCRETE_NEWS, 
        			newsManagerService.getSingleNews(concreteNewsId));
        	model.addAttribute(Constants.VIEWING_NEWS_ID, newsId);
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.FORWARD_VIEW_NEWS;
    }
	
	/**
	 * Redirects to the news-view.jsp that will show NEXT or PREVIOUS news message
	 * depending on the context of request URL.
	 * @param currentNewsCommentCount the count of comments of current displayed news message
	 * @param currentNewsId the id of cuurent displayed news message
	 * @param request the <code>HttpServletRequest</code> instance
	 * @return URL identifier of page with where news message can be viewed
	 */
	@RequestMapping(value = {"/previous/{currentNewsId}", "/next/{currentNewsId}"}, 
			method = RequestMethod.GET)
	public String newsNavigationResolve(@PathVariable Long currentNewsId, 
			HttpServletRequest request, ModelMap model) {
		try {
			String requestURL = request.getRequestURL().toString();
			NavigationDirection navigationParam = null;
			if(requestURL.contains("previous")) {
				navigationParam = NavigationDirection.PREVIOUS;
			} else if(requestURL.toString().contains("next")) {
				navigationParam = NavigationDirection.NEXT;
			}
			SearchFilter filter = this.getSearchFilter(model);
			Long toNewsId = newsManagerService.getNewsIdToNavigate(currentNewsId, filter,
					navigationParam);
			if(toNewsId == 0L) {
				toNewsId = currentNewsId;
			}
			return Constants.REDIRECT_VIEW_NEWS + String.valueOf(toNewsId);
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
	}
	
	/**
	 * Provides forwarding to the add-news.jsp page to edit some news message.
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with the ability of editing news message
	 */
	@RequestMapping(value = "/edit/{newsId}", method = RequestMethod.GET)
	public String editNewsPage(@PathVariable Long newsId, ModelMap model) {
		try {
			NewsMessage newsMessage = newsManagerService.getSingleNews(newsId);
			model.addAttribute(Constants.NEWS_MESSAGE, newsMessage);
		} catch (NumberFormatException | ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.FORWARD_ADD_NEWS; 
	}
	
	/**
	 * Provides forwarding to the add-news.jsp page to add some news message.
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with the ability of adding news message
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET) 
	public String addNewsPage(ModelMap model) {
		NewsTO news = new NewsTO();
		Date currentDate = new Date();
		news.setCreationDate(currentDate);
		news.setModificationDate(currentDate);
		NewsMessage newsMessage = new NewsMessage();
		newsMessage.setNews(news);
		try {
			model.addAttribute(Constants.NEWS_MESSAGE, newsMessage);
			model.addAttribute(Constants.AUTHORS_LIST, newsManagerService.getAllAuthors());
			model.addAttribute(Constants.TAGS_LIST, newsManagerService.getAllTags());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.FORWARD_ADD_NEWS;
	}
	
	/**
	 * Provides saving news messages after adding or updating
	 * @param newsMessage the <code>NewsMessage</code> instance coming as model attribute
	 * @param authorId the id of the author
	 * @param selectedTags selected tags
	 * @param newsId the id of the news
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where list of news can be viewed
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveNews(@ModelAttribute(Constants.NEWS_MESSAGE) NewsMessage newsMessage,
			@RequestParam Long authorId, @RequestParam List<Long> selectedTags,
			@RequestParam Long newsId, ModelMap model) {
		try {
			List<TagTO> newsTags = new ArrayList<TagTO>(selectedTags.size());
			for(Long tagId: selectedTags) {
				newsTags.add(newsManagerService.getTag(tagId));
			}
			newsMessage.getNews().setEntityId(newsId);
			newsMessage.setNewsTags(newsTags);
			newsMessage.setNewsAuthor(newsManagerService.getAuthor(authorId));
			if(newsId == 0L) {
				newsManagerService.addNews(newsMessage);
			} else {
				newsManagerService.updateNews(newsMessage);
			}
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_NEWS_LIST + 1;
	}
	
	/**
	 * Provides deleting selected news messages.
	 * @param selectedNews selected news id's
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where list of news can be viewed
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.GET) 
	public String deleteNews(@RequestParam Long[] selectedNews, ModelMap model) {
		try {
			newsManagerService.deleteNews(selectedNews);
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_NEWS_LIST + model.get(Constants.CURRENT_PAGE); 
	}

}
