package com.epam.newsapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.service.INewsManagerService;

/**
 * Provides connected with authors administration requests 
 * execution according to REST architecture.
 * @author Aleh_Rusak
 */
@Controller
@RequestMapping(value = "/author")
@SessionAttributes({Constants.AUTHORS_LIST})
public class AdminAuthorController {
	
	private static final Logger logger = Logger.getLogger(AdminAuthorController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;
	
	/**
	 * Provides forwarding to add-author.jsp.
	 * @return URL identifier of page where authors can be added or updated
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addAuthorPage() {
		return Constants.FORWARD_ADD_AUTHOR;
	}
	
	/**
	 * Provides selecting some author to edit it.
	 * @return URL identifier of page where authors can be added or updated
	 */
	@RequestMapping(value = "/edit/{entityId}", method = RequestMethod.GET)
	public String editAuthorPage(@PathVariable Long entityId, ModelMap model) {
		model.addAttribute(Constants.ID_TO_UPDATE, entityId);
		return Constants.FORWARD_ADD_AUTHOR;
	}
	
	/**
	 * Provides updating selected author. 
	 * Then redirects to the page where author can be added or updated.
	 * with the ability to update selected tag.
	 * @param authorId the id of the author
	 * @param authorsName author's name
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where author dan be added or updated
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateAuthor(@RequestParam Long authorId, @RequestParam String authorsName,
			ModelMap model) {
		try {
			AuthorTO authorToUpdate = new AuthorTO();
			authorToUpdate.setEntityId(authorId);
			authorToUpdate.setName(authorsName);
			newsManagerService.updateAuthor(authorToUpdate);
			model.addAttribute(Constants.AUTHORS_LIST, newsManagerService.getAllAuthors());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_ADD_AUTHOR;
	}

	/**
	 * Provides saving added or updated author.
	 * @param authorsName the name of the author
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where author can be added or updated
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveAuthor(@RequestParam String authorsName, ModelMap model) {
		try {
			AuthorTO newAuthor = new AuthorTO();
			newAuthor.setName(authorsName);
			newsManagerService.addAuthor(newAuthor);
			model.addAttribute(Constants.AUTHORS_LIST, newsManagerService.getAllAuthors());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		} catch (EntityAlreadyExistsException e) {
			logger.error(Constants.TAG_ALREADY_EXISTS + authorsName);
			model.addAttribute(Constants.ALREADY_EXISTS_ERROR, "error");
			return Constants.FORWARD_ADD_AUTHOR;
		}
		return Constants.REDIRECT_ADD_AUTHOR;
	}
	
	/**
	 * Provides deleting author.
	 * @param entityId the id of the tag
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where authors can be added or updated
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteAuthor(@RequestParam Long entityId, HttpServletRequest request, 
			ModelMap model) {
		try {
			newsManagerService.deleteAuthor(entityId);
			model.addAttribute(Constants.AUTHORS_LIST, newsManagerService.getAllAuthors());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_ADD_AUTHOR;
	}
}
