package com.epam.newsapp.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.service.INewsManagerService;
import com.epam.newsapp.util.SearchFilter;

/**
 * Provides connected with search filter administration requests 
 * execution according to REST architecture.
 * @author Aleh_Rusak
 */
@Controller
@RequestMapping(value = "/filter")
@SessionAttributes({Constants.CURRENT_PAGE, Constants.FILTER, Constants.AUTHORS_NAME})
public class AdminFilterController {
	
	private static final Logger logger = Logger.getLogger(AdminCommentController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;
	
	private SearchFilter getSearchFilter(ModelMap model) {
		SearchFilter filter = (SearchFilter) model.get(Constants.FILTER);
		if(filter == null) {
			return new SearchFilter(); 
		} else {
			return filter;
		}
	}
	
	/**
	 * Provides filtering news by author and tags.
     * @param authorId the id of the author
	 * @param selectedTags an array of tag's ids
	 * @return URL identifier of page with the list of news
	 */
	@RequestMapping(value = "/set", method = RequestMethod.GET)
	public String setFilterParameters(@RequestParam(required = false) Long authorId, 
			@RequestParam(required = false) Long[] selectedTags, ModelMap model) {
		SearchFilter filter = this.getSearchFilter(model);
		filter.setSelectedTags(selectedTags);
		filter.setAuthorId(authorId);
		model.addAttribute(Constants.FILTER, filter);
		try {
			if(authorId != 0L) {
				model.addAttribute(Constants.AUTHORS_NAME, newsManagerService.getAuthor(authorId).getName());
			}
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_NEWS_LIST + model.get(Constants.CURRENT_PAGE);
	}
	
	/**
	 * Provides resetting search filter.
	 * @param model the instance of <code>ModelMap</code>
	 * @return URL identifier of page with the list of news
	 */
	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String resetFilter(ModelMap model) {
		SearchFilter filter = this.getSearchFilter(model);
		filter.setSelectedTags(new Long[0]);
		filter.setAuthorId(0L);
		model.addAttribute(Constants.FILTER, filter);
		model.addAttribute(Constants.AUTHORS_NAME, "");
		return Constants.REDIRECT_NEWS_LIST + model.get(Constants.CURRENT_PAGE);
	}
}
