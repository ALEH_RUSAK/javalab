package com.epam.newsapp.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.service.INewsManagerService;

/**
 * Provides connected with tags administration requests 
 * execution according to REST architecture.
 * @author Aleh_Rusak
 */
@Controller
@RequestMapping(value = "/tag")
@SessionAttributes({Constants.TAGS_LIST})
public class AdminTagController {

	private static final Logger logger = Logger.getLogger(AdminNewsController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;
	
	/**
	 * Provides forwarding to add-tag.jsp.
	 * @return URL identifier of page where tags can be added or updated
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addTagPage() {
		return Constants.FORWARD_ADD_TAG;
	}
	
	/**
	 * Provides selecting some tag to edit it.
	 * @return URL identifier of page where tags can be added or updated
	 */
	@RequestMapping(value = "/edit/{entityId}", method = RequestMethod.GET)
	public String editTagPage(@PathVariable Long entityId, ModelMap model) {
		model.addAttribute(Constants.ID_TO_UPDATE, entityId);
		return Constants.FORWARD_ADD_TAG;
	}
	
	/**
	 * Provides updating selected tag. 
	 * Then redirects to the page where tags can be added or updated
	 * with the ability to update selected tag.
	 * @param tagId the id of the tag
	 * @param tagsName tag's name
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where tags can be added or updated
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateTag(@RequestParam Long tagId, @RequestParam String tagsName, ModelMap model) {
		try {
			TagTO tagToUpdate = new TagTO();
			tagToUpdate.setEntityId(tagId);
			tagToUpdate.setTagName(tagsName);
			newsManagerService.updateTag(tagToUpdate);
			model.addAttribute(Constants.TAGS_LIST, newsManagerService.getAllTags());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_ADD_TAG;
	}
	
	/**
	 * Provides saving added or updated tag.
	 * @param tagsName the name of the tag
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where tags can be added or updated
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveTag(@RequestParam String tagsName, ModelMap model) {
		TagTO newTag = new TagTO();
		newTag.setTagName(tagsName);
		try {
			newsManagerService.addTag(newTag);
			model.addAttribute(Constants.TAGS_LIST, newsManagerService.getAllTags());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		} catch (EntityAlreadyExistsException e) {
			logger.error(Constants.TAG_ALREADY_EXISTS + tagsName);
			model.addAttribute(Constants.ALREADY_EXISTS_ERROR, "error");
			return Constants.FORWARD_ADD_TAG;
		}
		return Constants.REDIRECT_ADD_TAG;
	}
	
	/**
	 * Provides deleting tag.
	 * @param entityId the id of the tag
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where authors can be added or updated
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteTag(@RequestParam Long entityId, ModelMap model) {
		try {
			newsManagerService.deleteTag(entityId);
			model.addAttribute(Constants.TAGS_LIST, newsManagerService.getAllTags());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_ADD_TAG;
	}
}
