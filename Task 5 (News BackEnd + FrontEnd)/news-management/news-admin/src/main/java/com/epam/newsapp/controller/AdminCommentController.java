package com.epam.newsapp.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.service.INewsManagerService;

/**
 * Provides connected with comments administration requests 
 * execution according to REST architecture.
 * @author Aleh_Rusak
 */
@Controller
@RequestMapping(value = "/comment")
@SessionAttributes( {Constants.VIEWING_NEWS_ID})
public class AdminCommentController {
	
	private static final Logger logger = Logger.getLogger(AdminCommentController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;
	
	/**
	 * Creates and posts new users comment.
	 * Redirects to the news-view.jsp with the comment-parameter. 
	 * @param commentText the request parameter that contains the text of users comment
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with where news can be viewed
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String postComment(@RequestParam String commentText, @RequestParam Long newsId,
			ModelMap model) {
        try {
        	CommentTO newComment = new CommentTO();
        	newComment.setEntityId(0L);
        	newComment.setCommentText(commentText);
        	newComment.setCreationDate(new Date());
        	newComment.setNewsId(newsId);
        	newsManagerService.addComment(newComment);
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.REDIRECT_VIEW_NEWS + newsId;
    }
	
	/**
	 * Provides deleting comment. Then redirects to the current viewing news page.
	 * @param commentId the id of the comment
	 * @param newsId the id of the news
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page where news message can be viewed
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.GET) 
	public String deleteComment(@RequestParam Long commentId, @RequestParam Long newsId,
			ModelMap model) {
		try {
			newsManagerService.deleteComment(commentId);
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
		return Constants.REDIRECT_VIEW_NEWS + newsId; 
	}
}
