<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="${pageContext.request.contextPath}/resources/style/page-style.css" rel="stylesheet">
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/login-validation.js"></script>
		<title>News Portal</title>
	</head>
	
	<body>
		<tiles:insertAttribute name="header" />
		<div class="middle">
			<div class="page_body">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
	</body>
</html>