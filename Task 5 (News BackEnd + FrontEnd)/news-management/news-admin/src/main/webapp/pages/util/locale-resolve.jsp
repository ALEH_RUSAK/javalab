<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="currentLocale" value="${pageContext.response.locale}"/>

<c:choose>
	<c:when test="${currentLocale == 'en'}">
		<c:set var="datePattern" value="MM/dd/yyyy" scope="application" />
	</c:when>
	<c:otherwise>
		<c:set var="datePattern" value="dd/MM/yyyy" scope="application" />
	</c:otherwise>
</c:choose>