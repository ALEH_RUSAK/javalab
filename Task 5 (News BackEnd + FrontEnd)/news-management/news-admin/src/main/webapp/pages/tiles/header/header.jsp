<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="page_header">
    <div class="top">
		<div class="title">
			<spring:message code="label.mainTitle" />
		</div>
		<div class="language_links">
			<a href="?lang=en"><spring:message code="link.english"/></a>
			<a href="?lang=ru"><spring:message code="link.russian"/></a>
		</div>
	</div>
</div>

