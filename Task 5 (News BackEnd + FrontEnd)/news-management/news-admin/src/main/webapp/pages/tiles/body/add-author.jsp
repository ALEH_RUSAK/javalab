<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="${pageContext.request.contextPath}/resources/style/add-style.css" rel="stylesheet">

<jsp:include page="/pages/util/locale-resolve.jsp" />

<div class="errors_exists">
	<c:if test="${alreadyExistsError != null}">
		<spring:message code="error.authorAlreadyExists"/>
	</c:if>
</div>

<c:forEach var="author" items="${authorsList}">
	<div class="update_author_tag">
		<div class="block_name">
			<spring:message code="label.author" /> :
		</div>
		<c:if test="${author.entityId != idToUpdate}">
			<div class="author_tag_input">
				<input type="text" value="${author.name}" readonly="readonly" size="46" style="background-color: #e0e1e4;"/>
			</div>
			<div class="edit_link">
				<c:url var="editAuthor" value="/author/edit/${author.entityId}" />
			    <a href="${editAuthor}">
			    	<c:out value="Edit" />
			    </a>
			</div>
		</c:if>
		<c:if test="${author.entityId == idToUpdate}">
			<form name="updateAuthor" action="${pageContext.request.contextPath}/author/update" method="POST"
				onsubmit="return validateAuthor('<spring:message code="js.error.emptyAuthorName"/>')">
				<div class="author_tag_input">
					<input type="text" id="authorsName" name="authorsName"
						value="${author.name}" size="46" style="background-color: white"/>
				</div>
				<input name="authorId" type="hidden" value="${author.entityId}" />
				<div class="edit_link">
					<input type="submit" class="link_button" value="Update" />
				</div>
			</form>
		</c:if>
	</div>
	<form method="POST" action="${pageContext.request.contextPath}/author/delete">
		<input class="delete_button_style" type="submit" name="deleteAuthor" value="X" />
		<input type="hidden" name="entityId" value="${author.entityId}" />
	</form>
</c:forEach>

<form name="addAuthor" method="POST" action="${pageContext.request.contextPath}/author/save"
	onsubmit="return validateAuthor('<spring:message code="js.error.emptyAuthorName"/>')">
	<div class="add_author_tag">
		<div class="block_name">
			<spring:message code="label.add.author" /> :
		</div>
		<div class="author_tag_input">
			<input type="text" id="authorsName" name="authorsName" value="" size="46"/>
		</div>
		<div class="button_area">
			<input type="submit" class="author_button_style" 
				value="<spring:message code="button.save" />" />
		</div>
	</div>
</form>