<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
		
<link href="${pageContext.request.contextPath}/resources/style/list-style.css" rel="stylesheet">

<script>
    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
</script>
	
<jsp:include page="/pages/util/locale-resolve.jsp" />

<form name="filterForm" action="${pageContext.request.contextPath}/filter/set" method="GET" 
		onsubmit="return validateFilter('<spring:message code="js.error.unsetFilterParams"/>')">
	<div class="filter">
		<select name="authorId" size="1">
			<option selected="selected" value="${filter.authorId}">
				<c:if test="${not empty authorsName}">
					<c:out value="${authorsName}"/>
				</c:if>
				<c:if test="${empty authorsName}">
					<spring:message code="label.author.select"/>
				</c:if>
			</option>
			<c:forEach var="author" items="${authorsList}">
				<option style="color: black;" value="${author.entityId}">
					<c:out value="${author.name}" />
				</option> 
			</c:forEach> 
		</select>
		<div class="multiselect">
			<div class="select_box" onclick="showCheckboxes()">
		        <select>
		            <option selected="selected" value="false">
		            	<spring:message code="label.tags.select"/>
		            </option>
		        </select>
		        <div class="over_select">
		        </div>
		    </div>
			<div id="checkboxes">
			    <c:forEach var="tag" items="${tagsList}">
					<label for="${tag.tagName}">
						<c:set var="checked" value="no" />
						<c:forEach var="filtersTagId" items="${filter.selectedTags}">
							<c:if test="${tag.entityId == filtersTagId}">
								<c:set var="checked" value="yes" />
							</c:if>
						</c:forEach>
						<c:if test="${checked == 'yes'}">
							<input type="checkbox" name="selectedTags" value="${tag.entityId}" checked="checked"/>
						</c:if>
						<c:if test="${checked == 'no'}">
							<input type="checkbox" name="selectedTags" value="${tag.entityId}" />
						</c:if>
						<c:out value="${tag.tagName}" />
					</label>
				</c:forEach> 
			</div>
		</div>
	</div> 
	<div class="filter_button_area">
		<input type="submit" class="filter_button_style" name="filter" 
			value="<spring:message code="button.filter"/>" />
	</div>
</form>

<form action="${pageContext.request.contextPath}/filter/reset" method="GET">
	<div class="reset_button_area">
		<input type="submit" class="filter_button_style" name="filter" 
			value="<spring:message code="button.resetFilter"/>" />
	</div>
</form>

<c:choose>
	<c:when test="${empty newsMessageList}">
		<div class="news_not_found">
			<spring:message code="no.news"/>
		</div>
	</c:when>
	<c:otherwise>
		<form name="deleteForm" action="${pageContext.request.contextPath}/news/delete" method="GET"
			onsubmit="return deleteNews('<spring:message code="js.error.deletion"/>', '<spring:message code="js.message.delete.confirm"/>')">
			<c:forEach var="concreteNews" items="${newsMessageList}">
				<div class="info">
					<div class="news_title_date">
						<div class="title">
							<c:url var="viewNews" value="/news/view/${concreteNews.news.entityId}" />
						    <a href="${viewNews}">
						    	<c:out value="${concreteNews.news.title}" />
						    </a>
						</div>
						<div class="author">
							<c:out value="( by ${concreteNews.newsAuthor.name} )" />
						</div>
						<span>
							<fmt:formatDate pattern="${datePattern}" value="${concreteNews.news.modificationDate}" />
						</span>
					</div>
					<div class="brief"> 
						<c:out value="${concreteNews.news.brief}" />
					</div>
					
					<div class="clearfix">
					</div>
					
					<div class="news_tags_comments">
						<div class="delete_checkbox">
							<input type="checkbox" name="selectedNews" value="${concreteNews.news.entityId}" />
						</div>
						<div class="links">
							<c:url var="editNews" value="/news/edit/${concreteNews.news.entityId}" />
					     	<a href="${editNews}"><spring:message code="link.edit" /></a>
						</div>
						<div class="comments">
							<spring:message code="label.comments" />
							<c:out value="(${fn:length(concreteNews.newsComments)})" />
						</div>
						<div class="tag">
							<c:forEach var="tag" items="${concreteNews.newsTags}">
								<c:out value="${tag.tagName}  " />
							</c:forEach>
						</div>
					</div>
				</div>
			</c:forEach>	
			
			<div class="delete_button_area">
				<input type="submit" class="delete_button_style" 
					value="<spring:message code="button.delete" />" />
			</div>
		</form>
			
		<div class="paging">
			<div class="page_numbers">
				<c:forEach var="pageNo" begin="1" end="${numberOfPages}">
					<c:url var="page" value="/news/list/${pageNo}" />
			     	<a href="${page}">
			     		<button>${pageNo}</button>
			     	</a>
				</c:forEach>
			</div>	
		</div>
	
		<div class="clearfix">
		</div>
			
	</c:otherwise>
</c:choose>


