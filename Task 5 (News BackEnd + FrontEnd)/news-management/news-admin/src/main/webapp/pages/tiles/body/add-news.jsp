<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="${pageContext.request.contextPath}/resources/style/add-style.css" rel="stylesheet">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>

<input type="hidden" id="TITLE_REQUIRED" value="<spring:message code="js.error.required.title"/>" />
<input type="hidden" id="BRIEF_REQUIRED" value="<spring:message code="js.error.required.brief"/>" />
<input type="hidden" id="CONTENT_REQUIRED" value="<spring:message code="js.error.required.content"/>" />
<input type="hidden" id="DATE_REQUIRED" value="<spring:message code="js.error.required.date"/>" />

<input type="hidden" id="TITLE_LENGTH" value="<spring:message code="js.error.length.title"/>" />
<input type="hidden" id="BRIEF_LENGTH" value="<spring:message code="js.error.length.brief"/>" />
<input type="hidden" id="CONTENT_LENGTH" value="<spring:message code="js.error.length.content"/>" />
<input type="hidden" id="DATE_LENGTH" value="<spring:message code="js.error.length.date"/>" />

<input type="hidden" id="DATE_ALERT" value="<spring:message code="js.error.date.format"/>" />
<input type="hidden" id="DATE_PATTERN" value="<spring:message code="js.date.pattern"/>" />

<input type="hidden" id="UNSET_AUTHOR" value="<spring:message code="js.error.unsetAuthor"/>" />
<input type="hidden" id="UNSET_TAGS" value="<spring:message code="js.error.unsetTags"/>" />

<script>
    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
</script>

<jsp:include page="/pages/util/locale-resolve.jsp" />

<form:form name="addForm" method="POST" action="${pageContext.request.contextPath}/news/save" 
	commandName="newsMessage" onsubmit="return validateFormFields()">
	
	<div class="add_block">
		
		<input type="hidden" name="newsId" value="${newsMessage.news.entityId}"/>
		
		<div class="block">
			<div class="block_name">
				<spring:message code="label.news.title" /> :
			</div>
			<div class="title_input">
				<form:input path="news.title" id="title" type="text" class="add_input"/>
			</div>
		</div>

		<div class="block">
			<div class="block_name">
				<spring:message code="label.news.date" /> :
			</div>
			<div class="date_input">
				<fmt:formatDate pattern="${datePattern}" value="${newsMessage.news.modificationDate}" 
					var="formattedDate" />
				<form:input path="news.modificationDate" id="modificationDate" type="text" class="add_date_input" 
					value="${formattedDate}" />
			</div>
		</div>

		<div class="block">
			<div class="block_name">
				<spring:message code="label.news.brief" /> :
			</div>
			<div class="brief_input">
				<form:textarea path="news.brief" id="brief" rows="4" cols="66" />
			</div>
		</div>

		<div class="block">
			<div class="block_name">
				<spring:message code="label.news.content" /> :
			</div>
			<div class="content_input">
				<form:textarea path="news.content" id="content" rows="8" cols="66"  />
			</div>
		</div>
	</div>
	
	<div class="filter">
		<select name="authorId" size="1">
			<c:set var="newsAuthorName" value="${newsMessage.newsAuthor.name}" />
			<option selected="selected" value="${newsMessage.newsAuthor.entityId}">
				<c:if test="${not empty newsAuthorName}">
					<c:out value="${newsAuthorName}"/>
				</c:if>
				<c:if test="${empty newsAuthorName}">
					<spring:message code="label.author.select"/>
				</c:if>
			</option>
			<c:forEach var="author" items="${authorsList}">
				<option style="color: black;" value="${author.entityId}">
					<c:out value="${author.name}" />
				</option> 
			</c:forEach> 
		</select>
		<div class="multiselect">
			<div class="select_box" onclick="showCheckboxes()">
		        <select>
		            <option selected="selected" value="false">
		            	<spring:message code="label.tags.select"/>
		            </option>
		        </select>
		        <div class="over_select">
		        </div>
		    </div>
			<div id="checkboxes">
			    <c:forEach var="tag" items="${tagsList}">
					<label for="${tag.tagName}">
						<c:set var="checked" value="no" />
						<c:forEach var="newsTag" items="${newsMessage.newsTags}">
							<c:if test="${tag.entityId == newsTag.entityId}">
								<c:set var="checked" value="yes" />
							</c:if>
						</c:forEach>
						<c:if test="${checked == 'yes'}">
							<input type="checkbox" name="selectedTags" value="${tag.entityId}" checked="checked"/>
						</c:if>
						<c:if test="${checked == 'no'}">
							<input type="checkbox" name="selectedTags" value="${tag.entityId}" />
						</c:if>
						<c:out value="${tag.tagName}" />
					</label>
				</c:forEach> 
			</div>
		</div>
	</div> 

	<div class="button_area">
		<input type="submit" class="button_style" 
			value="<spring:message code="button.save" />" />
	</div>
</form:form>