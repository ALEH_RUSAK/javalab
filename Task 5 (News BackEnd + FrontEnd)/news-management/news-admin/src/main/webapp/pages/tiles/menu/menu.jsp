<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="${pageContext.request.contextPath}/resources/style/menu-style.css" rel="stylesheet">

<div class="menu">
	<div class="navigation">
		<span>
			<img src="${pageContext.request.contextPath}/resources/img/menu-image.png"> 
			<c:url var="list" value="/news/list/${currentPage}" />
		    <a href="${list}">
		    	<spring:message code="menu.list"/>
		    </a>
		</span>
		<span>
			<img src="${pageContext.request.contextPath}/resources/img/menu-image.png"> 
			<c:url var="add" value="/news/add" />
		    <a href="${add}">
		    	<spring:message code="menu.add.news"/>
		    </a>
		</span>
		<span>
			<img src="${pageContext.request.contextPath}/resources/img/menu-image.png"> 
			<c:url var="author" value="/author/add" />
		    <a href="${author}">
		    	<spring:message code="menu.add.author"/>
		    </a>
		</span>
		<span>
			<img src="${pageContext.request.contextPath}/resources/img/menu-image.png"> 
			<c:url var="tags" value="/tag/add" />
		    <a href="${tags}">
		    	<spring:message code="menu.add.tags"/>
		    </a>
		</span>
	</div>
</div>