<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="${pageContext.request.contextPath}/resources/style/add-style.css" rel="stylesheet">

<jsp:include page="/pages/util/locale-resolve.jsp" />

<c:if test="${alreadyExistsError != null}">
	<div class="errors_exists">
		<spring:message code="error.tagAlreadyExists"/>
	</div>
</c:if>

<c:forEach var="tag" items="${tagsList}">
	<div class="update_author_tag">
		<div class="block_name">
			<spring:message code="label.tag" /> :
		</div>
		<c:if test="${tag.entityId != idToUpdate}">
			<div class="author_tag_input">
				<input type="text" value="${tag.tagName}" readonly="readonly" size="46" style="background-color: #e0e1e4;"/>
			</div>
			<div class="edit_link">
				<c:url var="editTag" value="/tag/edit/${tag.entityId}" />
			    <a href="${editTag}">
			    	<c:out value="Edit" />
			    </a>
			</div>
		</c:if>
		<c:if test="${tag.entityId == idToUpdate}">
			<form name="updateTag" action="${pageContext.request.contextPath}/tag/update" method="POST"
				onsubmit="return validateTag('<spring:message code="js.error.emptyTagName"/>')">
				<div class="author_tag_input">
					<input type="text" id="tagsName" name="tagsName"
						value="${tag.tagName}" size="46" style="background-color: white"/>
				</div>
				<input name="tagId" type="hidden" value="${tag.entityId}" />
				<div class="edit_link">
					<input type="submit" class="link_button" value="Update" />
				</div>
			</form>
		</c:if>
	</div>
	<form action="${pageContext.request.contextPath}/tag/delete">
		<input class="delete_button_style" type="submit" name="deleteTag" value="X" />
		<input type="hidden" name="entityId" value="${tag.entityId}" />
	</form>
</c:forEach>

<form name="addTag" method="POST" action="${pageContext.request.contextPath}/tag/save"
	onsubmit="return validateTag('<spring:message code="js.error.emptyTagName"/>')">
	<div class="add_author_tag">
		<div class="block_name">
			<spring:message code="label.add.tag" /> :
		</div>
		<div class="author_tag_input">
			<input type="text" id="tagsName" name="tagsName" value="" size="46"/>
		</div>
		<div class="button_area">
			<input type="submit" class="author_button_style" 
				value="<spring:message code="button.save" />" />
		</div>
	</div>
</form>