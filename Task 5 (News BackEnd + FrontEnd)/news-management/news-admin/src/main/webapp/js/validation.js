
function deleteNews(DELETE_ALERT, DELETE_CONFIRM) {
	var selectedNews = document.forms["deleteForm"]["selectedNews"];
	if(selectedNews.checked) {
		return confirmDeletion(DELETE_CONFIRM);
	}
	for (var i = 0; i < selectedNews.length; i++) {
		if (selectedNews[i].checked) {
			return confirmDeletion(DELETE_CONFIRM);
		}
	}
	alert(DELETE_ALERT);
	return false;
}

function confirmDeletion(DELETE_CONFIRM) {
	return confirm(DELETE_CONFIRM);
}

function validateComment(EMPTY_COMMENT) {
	var commentText = document.forms["commentForm"]["commentText"].value;
	if (commentText.trim() == null || commentText.trim() == "") {
		alert(EMPTY_COMMENT);
		return false;
	} else {
		return true;
	}
}

function validateCheckedTags(FORM_NAME) {
	var selectedTags = document.forms[FORM_NAME]["selectedTags"];
	for (var i = 0; i < selectedTags.length; i++) {
		if (selectedTags[i].checked) {
			return true;
		}
	}
	return false;
}

function validateFilter(UNSET_PARAMS) {
	var authorId = document.forms["filterForm"]["authorId"].value;
	if(!validateCheckedTags("filterForm")) {
		if(authorId == "0") {
			alert(UNSET_PARAMS);
			return false;
		} else {
			return true;
		}
	} else {
		return true;
	}
}

function validateFormFields() {
	var errorMessage = '';
	
	if (document.forms['addForm']['title'].value.trim() == '') {
		errorMessage += document.getElementById('TITLE_REQUIRED').value += '\n';
	}
	if (document.forms['addForm']['brief'].value.trim() == '') {
		errorMessage += document.getElementById('BRIEF_REQUIRED').value += '\n';
	}
	if (document.forms['addForm']['content'].value.trim() == '') {
		errorMessage += document.getElementById('CONTENT_REQUIRED').value += '\n';
	}
	if (document.forms['addForm']['modificationDate'].value.trim() == '') {
		errorMessage += document.getElementById('DATE_REQUIRED').value += '\n';
	}
	
	var titleLength = document.getElementById('title').value.length;
	var briefLength = document.getElementById('brief').value.length;
	var contentLength = document.getElementById('content').value.length;
	var dateLength = document.getElementById('modificationDate').value.length;
	
	if (titleLength >= 30) {
		errorMessage += document.getElementById('TITLE_LENGTH').value += '\n';
	}
	
	if (briefLength >= 100) {
		errorMessage += document.getElementById('BRIEF_LENGTH').value += '\n';
	} 
	
	if (contentLength >= 2000) {
		errorMessage += document.getElementById('CONTENT_LENGTH').value += '\n';
	} 
	
	if (dateLength >= 12) {
		errorMessage += document.getElementById('DATE_LENGTH').value += '\n';
	} 
	
	var date = document.forms['addForm']['modificationDate'].value;
	var datePattern = new RegExp(document.getElementById('DATE_PATTERN').value);	
	var flag = datePattern.test(date);
	if (!flag) {
		errorMessage += document.getElementById('DATE_ALERT').value += '\n';
	} 
	
	var authorId = document.forms["addForm"]["authorId"].value;
	if(authorId == "0" || authorId == "") {
		errorMessage += document.getElementById('UNSET_AUTHOR').value += '\n';
	}

	if(!validateCheckedTags("addForm")) {
		errorMessage += document.getElementById('UNSET_TAGS').value += '\n';
	}
	
	if(errorMessage.length > 0) {
		alert(errorMessage);
		return false;
	} else {
		return true;
	}
}

function validateAuthor(EMPTY_AUTHOR_NAME) {
	var name = document.getElementById('authorsName').value.trim();
	if(name == '') {
		alert(EMPTY_AUTHOR_NAME);
		return false;
	} else {
		return true;
	}
}

function validateTag(EMPTY_TAG_NAME) {
	var name = document.getElementById('tagsName').value.trim();
	if(name == '') {
		alert(EMPTY_TAG_NAME);
		return false;
	} else {
		return true;
	}
}