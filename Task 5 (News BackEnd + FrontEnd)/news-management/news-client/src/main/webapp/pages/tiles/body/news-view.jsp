<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<link href="${pageContext.request.contextPath}/resources/style/view-style.css" rel="stylesheet">

<jsp:include page="/pages/util/locale-resolve.jsp" />

<div class="view_page">
	<div class="navigation_links">
		<div class="links">
			<c:url var="list" value="/news/list/${currentPage}" />
		    <a href="${list}"><spring:message code="link.back"/></a>
		</div>
	</div>
	<div class="news_title_date">
		<div class="brief_elem">
			<c:out value="${concreteNews.news.brief}" />
		</div>
		<div class="date_elem">
			<fmt:formatDate pattern="${datePattern}" value="${concreteNews.news.modificationDate}" />
		</div>
		<div class="author_elem">
			<c:out value="( by ${concreteNews.newsAuthor.name} )" />
		</div>
	</div>
	<div class="content_elem">
		<c:out value="${concreteNews.news.content}" />
	</div>
	<div class="comments_area">
		<c:forEach var="newsComm" items="${concreteNews.newsComments}">
			<div class="comment_date">
				<fmt:formatDate pattern="${datePattern}" value="${newsComm.creationDate}" />
			</div>
			<div class="comment_text">
				<c:out value="${newsComm.commentText}" />
			</div>
		</c:forEach>
	</div>
	<form name="commentForm" action="${pageContext.request.contextPath}/news/view/postComment" method="POST" 
		onsubmit="return validateComment('<spring:message code="js.error.emptyComment"/>')">
		<div class="input_comment">
			<textarea rows="4" cols="60" name="commentText">
			</textarea>
			<input type="hidden" name="newsId" value="${concreteNews.news.entityId}" />
		</div>
		<div class="post_button_area">
			<input type="submit" class="button_style" name="postComment" 
				value="<spring:message code="button.postComment" />"/>
		</div>
	</form>
	<div class="navigation_links">
		<div class="previous_link">
			<c:url var="previous" value="/news/previous/${concreteNews.news.entityId}" />
		    <a href="${previous}"><spring:message code="link.previous" /></a>
		</div>
		<div class="next_link">
			<c:url var="next" value="/news/next/${concreteNews.news.entityId}" />
		    <a href="${next}"><spring:message code="link.next" /></a>
		</div>
	</div>
</div>

<div class="clearfix">
</div>