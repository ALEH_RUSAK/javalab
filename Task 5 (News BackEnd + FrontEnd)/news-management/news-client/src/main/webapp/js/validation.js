function validateComment(EMPTY_COMMENT) {
	var commentText = document.forms["commentForm"]["commentText"].value;
	if (commentText.trim() == null || commentText.trim() == "") {
		alert(EMPTY_COMMENT);
		return false;
	} else {
		return true;
	}
}

function validateCheckedTags() {
	var selectedTags = document.forms["filterForm"]["selectedTags"];
	for (var i = 0; i < selectedTags.length; i++) {
		if (selectedTags[i].checked) {
			return true;
		}
	}
	return false;
}

function validateFilter(UNSET_PARAMS) {
	var authorId = document.forms["filterForm"]["authorId"].value;
	if(!validateCheckedTags()) {
		if(authorId == "0") {
			alert(UNSET_PARAMS);
			return false;
		} else {
			return true;
		}
	} else {
		return true;
	}
}