package com.epam.newsapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsapp.constant.Constants;

/**
 * Home controller class provides handling request mapping "/" (home mapping)
 * and redirects to another action
 * @author Aleh_Rusak
 */
@Controller
public class HomeController {

	@RequestMapping(value = {"/", "/news/list"}, method = RequestMethod.GET)
	public String start() {
	    return Constants.REDIRECT_NEWS_LIST + 1;
	}
}
