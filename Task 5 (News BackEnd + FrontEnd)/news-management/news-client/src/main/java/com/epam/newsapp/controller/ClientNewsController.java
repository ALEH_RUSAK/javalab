<<<<<<< HEAD
package com.epam.newsapp.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.service.INewsManagerService;
import com.epam.newsapp.util.NavigationDirection;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;
import com.epam.newsapp.valueobject.NewsMessage;

/**
 * Controller class provides client's news 
 * requests execution according to REST architecture.
 * @author Aleh_Rusak
 */
@Controller
@RequestMapping(value = "/news")
@SessionAttributes({Constants.AUTHORS_LIST, Constants.TAGS_LIST, Constants.CURRENT_PAGE,
	Constants.FILTER, Constants.PAGER})
public class ClientNewsController {
	
	private static final Logger logger = Logger.getLogger(ClientNewsController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;
	
	private SearchFilter getSearchFilter(ModelMap model) {
		SearchFilter filter = (SearchFilter) model.get(Constants.FILTER);
		return filter == null ? new SearchFilter() : filter;
	}
	
	private Pager getPager(ModelMap model) {
		Pager pager = (Pager) model.get(Constants.PAGER);
		return pager == null ? new Pager() : pager;
	}
	
	/**
	 * Provides forwadding to the news-list.jsp page with the list of news.
	 * Select news messages to be displayed using pagination
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with the list of news
	 */
	@RequestMapping(value = "/list/{currentPage}", method = RequestMethod.GET)
    public String viewNewsMessageList(@PathVariable String currentPage, ModelMap model) {
        try {
        	Pager pager = this.getPager(model);
        	SearchFilter filter = this.getSearchFilter(model);
        	model.addAttribute(Constants.CURRENT_PAGE, currentPage);
        	model.addAttribute(Constants.FILTER, filter);
        	pager.setCurrentPage(Integer.valueOf(currentPage));
        	Long numberOfNews = newsManagerService.getNumberOfFilteredNews(filter);
        	List<NewsMessage> newsMessageList = newsManagerService.getNewsOnPageUseFilter(pager, filter);
        	if(numberOfNews > pager.getItemsPerPage()) {
        		model.addAttribute(Constants.NUMBER_OF_PAGES, pager.getNumberOfPages(numberOfNews));
        	}
        	model.addAttribute(Constants.NEWS_LIST, newsMessageList);
        	model.addAttribute(Constants.AUTHORS_LIST, newsManagerService.getAllAuthors());
        	model.addAttribute(Constants.TAGS_LIST, newsManagerService.getAllTags());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.FORWARD_NEWS_LIST;
    }
	
	/**
	 * Provides forwadding to the news-view.jsp page with concrete news to view
	 * @param newsId the id of the news
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with where news can be viewed
	 */
	@RequestMapping(value = "/view/{newsId}", method = RequestMethod.GET)
    public String viewSingleNewsMessage(@PathVariable String newsId, ModelMap model,
    		HttpServletRequest request) {
        try {
        	Long concreteNewsId = Long.valueOf(newsId);
        	model.addAttribute(Constants.CONCRETE_NEWS, 
        			newsManagerService.getSingleNews(concreteNewsId));
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.FORWARD_VIEW_NEWS;
    }
	
	/**
	 * Creates and posts new users comment.
	 * Redirects to the news-view.jsp with the comment-parameter 
	 * @param commentText the request parameter that contains the text of users comment
	 * @param model the <code>ModelMap</code> instance
	 * @return URL identifier of page with where news can be viewed
	 */
	@RequestMapping(value = "/view/postComment", method = RequestMethod.POST)
	public String postComment(@RequestParam String commentText, @RequestParam String newsId,
			ModelMap model) {
		Long concreteNewsId = Long.valueOf(newsId);
        try {
        	CommentTO newComment = new CommentTO();
        	newComment.setEntityId(0L);
        	newComment.setCommentText(commentText);
        	newComment.setCreationDate(new Date());
        	newComment.setNewsId(Long.valueOf(newsId));
        	newsManagerService.addComment(newComment);
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.REDIRECT_VIEW_NEWS + concreteNewsId;
    }
	
	/**
	 * Redirects to the news-view.jsp that will show NEXT or PREVIOUS news message
	 * depending on the context of request URL.
	 * @param currentNewsCommentCount the count of comments of current displayed news message
	 * @param currentNewsId the id of cuurent displayed news message
	 * @param request the <code>HttpServletRequest</code> instance
	 * @return URL identifier of page with where news message can be viewed
	 */
	@RequestMapping(value = {"/previous/{currentNewsId}", "/next/{currentNewsId}"}, 
			method = RequestMethod.GET)
	public String newsNavigationResolve(@PathVariable Long currentNewsId, 
			HttpServletRequest request, ModelMap model) {
		try {
			String requestURL = request.getRequestURL().toString();
			NavigationDirection navigationParam = null;
			if(requestURL.contains("previous")) {
				navigationParam = NavigationDirection.PREVIOUS;
			} else if(requestURL.toString().contains("next")) {
				navigationParam = NavigationDirection.NEXT;
			}
			SearchFilter filter = this.getSearchFilter(model);
			Long toNewsId = newsManagerService.getNewsIdToNavigate(
							currentNewsId, filter, navigationParam);
			if(toNewsId.equals(0L)) {
				toNewsId = currentNewsId;
			}
			return Constants.REDIRECT_VIEW_NEWS + toNewsId;
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
	}
}
=======
package com.epam.newsapp.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.service.INewsManagerService;
import com.epam.newsapp.util.NavigationDirection;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;
import com.epam.newsapp.valueobject.NewsMessage;

/**
 * Controller class provides client's news 
 * requests execution according to REST architecture.
 * @author Aleh_Rusak
 */
@Controller
@RequestMapping(value = "/news")
@SessionAttributes({Constants.AUTHORS_LIST, Constants.TAGS_LIST, Constants.CURRENT_PAGE,
	Constants.FILTER, Constants.PAGER})
public class ClientNewsController {
	
	private static final Logger logger = Logger.getLogger(ClientNewsController.class);
	
	@Autowired
	private INewsManagerService newsManagerService;
	
	private SearchFilter getSearchFilter(ModelMap model) {
		SearchFilter filter = (SearchFilter) model.get(Constants.FILTER);
		if(filter == null) {
			return new SearchFilter(); 
		} else {
			return filter;
		}
	}
	
	private Pager getPager(ModelMap model) {
		Pager pager = (Pager) model.get(Constants.PAGER);
		if(pager == null) {
			return new Pager(); 
		} else {
			return pager;
		}
	}
	
	/**
	 * Provides forwarding to the news-list.jsp page with the list of news.
	 * Selects news messages to be displayed using pagination.
	 * @param model the <code>ModelMap</code> instance
	 * @return identifier of tiles-built page with the list of news
	 */
	@RequestMapping(value = "/list/{currentPage}", method = RequestMethod.GET)
    public String viewNewsMessageList(@PathVariable String currentPage, ModelMap model) {
        try {
        	Pager pager = this.getPager(model);
        	SearchFilter filter = this.getSearchFilter(model);
        	model.addAttribute(Constants.CURRENT_PAGE, currentPage);
        	model.addAttribute(Constants.FILTER, filter);
        	pager.setCurrentPage(Integer.valueOf(currentPage));
        	Long numberOfNews = newsManagerService.getNumberOfFilteredNews(filter);
        	List<NewsMessage> newsMessageList = newsManagerService.getNewsOnPageUseFilter(pager, filter);
        	if(numberOfNews > pager.getItemsPerPage()) {
        		model.addAttribute(Constants.NUMBER_OF_PAGES, pager.getNumberOfPages(numberOfNews));
        	}
        	model.addAttribute(Constants.NEWS_LIST, newsMessageList);
        	model.addAttribute(Constants.AUTHORS_LIST, newsManagerService.getAllAuthors());
        	model.addAttribute(Constants.TAGS_LIST, newsManagerService.getAllTags());
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.FORWARD_NEWS_LIST;
    }
	
	/**
	 * Provides forwarding to the news-view.jsp page with concrete news to view
	 * @param newsId the id of the news
	 * @param model the <code>ModelMap</code> instance
	 * @return identifier of tiles-built page where single news message can be viewed
	 */
	@RequestMapping(value = "/view/{newsId}", method = RequestMethod.GET)
    public String viewSingleNewsMessage(@PathVariable String newsId, ModelMap model,
    		HttpServletRequest request) {
        try {
        	Long concreteNewsId = Long.valueOf(newsId);
        	model.addAttribute(Constants.CONCRETE_NEWS, 
        			newsManagerService.getSingleNews(concreteNewsId));
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
        return Constants.FORWARD_VIEW_NEWS;
    }
	
	/**
	 * Creates and posts new users comment.
	 * Redirects to the news-view.jsp with the comment-parameter.
	 * @param commentText the request parameter that contains the text of users comment
	 * @param model the <code>ModelMap</code> instance
	 * @return identifier of tiles-built page where single news message can be viewed
	 */
	@RequestMapping(value = "/view/postComment", method = RequestMethod.POST)
	public String postComment(@RequestParam String commentText, @RequestParam String newsId,
			ModelMap model) {
		Long concreteNewsId = Long.valueOf(newsId);
        try {
        	CommentTO newComment = new CommentTO();
        	newComment.setEntityId(0L);
        	newComment.setCommentText(commentText);
        	newComment.setCreationDate(new Date());
        	newComment.setNewsId(Long.valueOf(newsId));
        	newsManagerService.addComment(newComment);
        	return Constants.REDIRECT_VIEW_NEWS + concreteNewsId;
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
    }
	
	/**
	 * Redirects to the news-view.jsp that will show NEXT or PREVIOUS news message
	 * depending on the context of request URL.
	 * @param currentNewsCommentCount the count of comments of current displayed news message
	 * @param currentNewsId the id of current displayed news message
	 * @param request the <code>HttpServletRequest</code> instance
	 * @return URL identifier of page with where news message can be viewed
	 */
	@RequestMapping(value = {"/previous/{currentNewsId}", "/next/{currentNewsId}"}, 
			method = RequestMethod.GET)
	public String newsNavigationResolve(@PathVariable Long currentNewsId, 
			HttpServletRequest request, ModelMap model) {
		try {
			String requestURL = request.getRequestURL().toString();
			NavigationDirection navigationParam = null;
			if(requestURL.contains("previous")) {
				navigationParam = NavigationDirection.PREVIOUS;
			} else if(requestURL.toString().contains("next")) {
				navigationParam = NavigationDirection.NEXT;
			}
			SearchFilter filter = this.getSearchFilter(model);
			Long toNewsId = 
					newsManagerService.getNewsIdToNavigate(currentNewsId, filter, navigationParam);
			if(toNewsId.equals(0L)) {
				toNewsId = currentNewsId;
			}
			return Constants.REDIRECT_VIEW_NEWS + toNewsId;
		} catch (ServiceException e) {
			logger.error(e);
			return Constants.FORWARD_ERROR_PAGE;
		}
	}
}
>>>>>>> 6b913b5cc13c9dbe493c067f973f3a7891c40ec7
