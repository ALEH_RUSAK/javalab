<<<<<<< HEAD:Task 5 (News BackEnd + FrontEnd)/news-management/news-common/src/main/java/com/epam/newsapp/persistence/dao/util/NewsQueryBuilder.java
package com.epam.newsapp.persistence.dao.util;

import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.util.SearchFilter;

public class NewsQueryBuilder {

	private static final String SQL_SELECT_COMMENTS_COUNT = "LEFT JOIN (SELECT news_id, COUNT(*) CNT FROM Comments GROUP BY news_id) c "
			+ "ON n.news_id = c.news_id ";
	private static final String SQL_SELECTING_ORDER = "ORDER BY CNT DESC NULLS LAST, n.modification_date ";
	private static final String ROWNUM_DIAPASONE = "WHERE ROWNUM < ?) WHERE rnum >= ?";
	private static final String SQL_SELECTING_ORDER_ROWNUM_DIAPASONE = SQL_SELECTING_ORDER
			+ ") ns " + ROWNUM_DIAPASONE;
	private static final String SQL_SELECT_NEWS_ON_PAGE_USE_FILTER = "SELECT news_id, title, brief, content, creation_date, modification_date FROM "
			+ "(SELECT ns.news_id, ns.title, ns.brief, ns.content, ns.creation_date, ns.modification_date, ROWNUM rnum FROM (";
	private static final String SQL_SELECT_NEWS_COUNT_BY_FILTER = "SELECT COUNT(*) FROM ("
			+ "SELECT DISTINCT news_id FROM (SELECT ns.news_id FROM (SELECT n.news_id, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT;
	private static final String SQL_FILTER_NEWS_SELECTION = "SELECT DISTINCT n.news_id, n.title, n.brief, n.content, "
			+ "n.modification_date, n.creation_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT;
	private static final String SQL_FIND_BY_AUTHOR = "INNER JOIN News_Authors ON News_Authors.news_id = n.news_id ";
	private static final String SQL_FIND_BY_AUTHOR_CONDITION = "News_Authors.author_id = ?";
	private static final String SQL_FIND_BY_TAGS = "INNER JOIN News_Tag ON News_Tag.news_id = n.news_id ";
	private static final String SQL_FIND_BY_TAGS_CONDITION = "News_Tag.tag_id IN ";
	private static final String SQL_SELECT_WITH_NUM_QUERY_START = "WITH num_query AS "
			+ "(SELECT q.news_id, ROWNUM AS idx FROM "
			+ "(SELECT news_id, title, brief, content, creation_date, modification_date FROM "
			+ "(SELECT ns.news_id, ns.title, ns.brief, ns.content, ns.creation_date, ns.modification_date FROM (";
	private static final String SQL_SELECT_WITH_NUM_QUERY_END = " ) ns ) ) q) ";
	private static final String SQL_SELECT_ALL_NEWS = "SELECT n.news_id, n.title, n.brief, n.content, n.creation_date, "
			+ "n.modification_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT + SQL_SELECTING_ORDER;
	private static final String SQL_AND = " AND ";
	private static final String SQL_WHERE = " WHERE ";
	private static final String SELECT_COUNT_QUERY_END = ") ns ) )";
	private static final String SQL_SELECT_DIRECTION_DEFAULT_FILTER = SQL_SELECT_WITH_NUM_QUERY_START
			+ SQL_SELECT_ALL_NEWS + SQL_SELECT_WITH_NUM_QUERY_END;

	private static DatabaseUtil<NewsTO> databaseUtil = new DatabaseUtil<NewsTO>();

	private static String buildSearchCondition(Long[] selectedTags,
			boolean isAuthorSet, boolean isSelectedTagsSet) {
		StringBuilder searchCondition = new StringBuilder();
		if (isAuthorSet) {
			searchCondition.append(SQL_FIND_BY_AUTHOR);
		}
		if (isSelectedTagsSet) {
			searchCondition.append(SQL_FIND_BY_TAGS);
		}
		searchCondition.append(SQL_WHERE);
		if (isAuthorSet) {
			searchCondition.append(SQL_FIND_BY_AUTHOR_CONDITION);
			if (isSelectedTagsSet) {
				searchCondition.append(SQL_AND);
			}
		}
		if (isSelectedTagsSet) {
			searchCondition.append(SQL_FIND_BY_TAGS_CONDITION);
			searchCondition.append(databaseUtil
					.prepareWhereInQuery(selectedTags));
		}
		return searchCondition.toString();
	}

	/**
	 * Provides constructing SQL-command depending on the state of searching
	 * filter. Returned SQL-command will be used to select count of filtered
	 * news.
	 * 
	 * @param filter
	 *            the searching filter
	 * @return SQL-command as String or null, if the state of the filter is
	 *         UNSET
	 */
	public static String getQueryForNumberOfSelectedByFilter(SearchFilter filter) {
		StringBuilder countSelectionQuery = new StringBuilder();
		countSelectionQuery.append(SQL_SELECT_NEWS_COUNT_BY_FILTER);
		boolean isAuthorSet = filter.isSetAuthorId();
		boolean isSelectedTagsSet = filter.isSetSelectedTags();
		if (!isAuthorSet && !isSelectedTagsSet) {
			return null;
		} else {
			String builtCondition = buildSearchCondition(
					filter.getSelectedTags(), isAuthorSet, isSelectedTagsSet);
			countSelectionQuery.append(builtCondition);
		}
		countSelectionQuery.append(SELECT_COUNT_QUERY_END);
		return countSelectionQuery.toString();
	}

	/**
	 * Provides constructing SQL-command depending on the state of searching
	 * filter. Returned SQL-command will be used to select actually filtered
	 * news.
	 * 
	 * @param filter
	 *            the search filter instance
	 * @return SQL-command as String or null, if no filter parameters were set
	 */
	public static String getSelectionQueryByFilter(SearchFilter filter) {
		StringBuilder selectionQuery = new StringBuilder();
		selectionQuery.append(SQL_SELECT_NEWS_ON_PAGE_USE_FILTER);
		selectionQuery.append(SQL_FILTER_NEWS_SELECTION);
		boolean isAuthorSet = filter.isSetAuthorId();
		boolean isSelectedTagsSet = filter.isSetSelectedTags();
		if (!isAuthorSet && !isSelectedTagsSet) {
			return null;
		} else {
			String builtCondition = buildSearchCondition(
					filter.getSelectedTags(), isAuthorSet, isSelectedTagsSet);
			selectionQuery.append(builtCondition);
		}
		selectionQuery.append(SQL_SELECTING_ORDER_ROWNUM_DIAPASONE);
		return selectionQuery.toString();
	}

	/**
	 * Provides constructing SQL-command depending on the state of searching
	 * filter. Returned SQL-command will be used to select actually filtered
	 * news.
	 * 
	 * @param filter
	 *            the searching filter
	 * @param nextOrPreviousPart
	 *            the String part of query that contains query that selects NEXT
	 *            or PREVIOUS news
	 * @return SQL-command as String, if no filter parameters were set
	 */
	public static String getNextOrPreviousSelectionQuery(SearchFilter filter,
			final String nextOrPreviousPart) {
		StringBuilder selectionQuery = new StringBuilder();
		selectionQuery.append(SQL_SELECT_WITH_NUM_QUERY_START);
		String queryEnd = SQL_SELECTING_ORDER + SQL_SELECT_WITH_NUM_QUERY_END
				+ nextOrPreviousPart;
		boolean isAuthorSet = filter.isSetAuthorId();
		boolean isSelectedTagsSet = filter.isSetSelectedTags();
		if (!isAuthorSet && !isSelectedTagsSet) {
			return SQL_SELECT_DIRECTION_DEFAULT_FILTER + nextOrPreviousPart;
		} else {
			String builtCondition = buildSearchCondition(
					filter.getSelectedTags(), isAuthorSet, isSelectedTagsSet);
			selectionQuery.append(builtCondition);
		}
		selectionQuery.append(queryEnd);
		return selectionQuery.toString();
	}
}
=======
package com.epam.newsapp.persistence.dao.util.querybuild;

import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.util.DatabaseUtil;
import com.epam.newsapp.util.SearchFilter;

/**
 * 
 * @author Aleh_Rusak
 */
public class NewsQueryBuilder {
	
	private static final String SQL_SELECT_COMMENTS_COUNT = "LEFT JOIN (SELECT news_id, COUNT(*) CNT FROM Comments GROUP BY news_id) c "
            + "ON n.news_id = c.news_id ";
	private static final String SQL_SELECTING_ORDER = "ORDER BY CNT DESC NULLS LAST, n.modification_date ";
	private static final String ROWNUM_DIAPASONE = "WHERE ROWNUM < ?) WHERE rnum >= ?";
	private static final String SQL_SELECTING_ORDER_ROWNUM_DIAPASONE = SQL_SELECTING_ORDER + ") ns " + ROWNUM_DIAPASONE;
	private static final String SQL_SELECT_NEWS_ON_PAGE_USE_FILTER = "SELECT news_id, title, brief, content, creation_date, modification_date FROM "
			+ "(SELECT ns.news_id, ns.title, ns.brief, ns.content, ns.creation_date, ns.modification_date, ROWNUM rnum FROM (";
	private static final String SQL_SELECT_NEWS_COUNT_BY_FILTER = 
			"SELECT COUNT(*) FROM ("
			+ "SELECT DISTINCT n.news_id FROM News n ";
	private static final String SQL_FILTER_NEWS_SELECTION = 
			"SELECT DISTINCT n.news_id, n.title, n.brief, n.content, "
			+ "n.modification_date, n.creation_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT;
	private static final String SQL_FIND_BY_AUTHOR =
			"INNER JOIN News_Authors ON News_Authors.news_id = n.news_id ";
	private static final String SQL_FIND_BY_AUTHOR_CONDITION = 
			"News_Authors.author_id = ";
	private static final String SQL_FIND_BY_TAGS = 
			"INNER JOIN News_Tag ON News_Tag.news_id = n.news_id ";
	private static final String SQL_FIND_BY_TAGS_CONDITION = 
			"News_Tag.tag_id IN ";
	private static final String SQL_SELECT_WITH_NUM_QUERY_START = "WITH num_query AS "
			+ "(SELECT q.news_id, ROWNUM AS idx FROM "
			+ "(SELECT news_id, title, brief, content, creation_date, modification_date FROM "
			+ "(SELECT ns.news_id, ns.title, ns.brief, ns.content, ns.creation_date, ns.modification_date FROM (";
	private static final String SQL_SELECT_WITH_NUM_QUERY_END = " ) ns ) ) q) ";
	private static final String SQL_SELECT_ALL_NEWS = "SELECT n.news_id, n.title, n.brief, n.content, n.creation_date, "
			+ "n.modification_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT + SQL_SELECTING_ORDER;
	private static final String SQL_AND = " AND ";
	private static final String SQL_WHERE = " WHERE ";
	private static final String SELECT_COUNT_QUERY_END = ")";
	private static final String SQL_SELECT_DIRECTION_DEFAULT_FILTER = 
			SQL_SELECT_WITH_NUM_QUERY_START
			+ SQL_SELECT_ALL_NEWS
			+ SQL_SELECT_WITH_NUM_QUERY_END;
	
	private NewsQueryBuilder() {}
	
	private static DatabaseUtil<NewsTO> databaseUtil = new DatabaseUtil<NewsTO>();
	
	private static String buildSearchCondition(SearchFilter filter) {
		StringBuilder searchCondition = new StringBuilder();
		boolean isAuthorSet = filter.isSetAuthorId();
		boolean isSelectedTagsSet = filter.isSetSelectedTags();
		if(isAuthorSet) {
			searchCondition.append(SQL_FIND_BY_AUTHOR);
		}
		if(isSelectedTagsSet) {
			searchCondition.append(SQL_FIND_BY_TAGS);
		}
		searchCondition.append(SQL_WHERE);
		if(isAuthorSet) {
			searchCondition.append(SQL_FIND_BY_AUTHOR_CONDITION);
			searchCondition.append(filter.getAuthorId());
			if(isSelectedTagsSet) {
				searchCondition.append(SQL_AND);
			}
		}
		if(isSelectedTagsSet) {
			searchCondition.append(SQL_FIND_BY_TAGS_CONDITION);
			searchCondition.append(databaseUtil.prepareWhereInQuery(filter.getSelectedTags()));
		}
		return searchCondition.toString();
	}
	
	/**
	 * Provides constructing SQL-statement depending on the state of searching filter.
	 * Returned SQL-statement will be used to select count of filtered news.
	 * @param filter the searching filter 
	 * @return SQL-command as String or null, if the state of the filter is UNSET
	 */
	public static String getQueryForNumberOfSelectedByFilter(SearchFilter filter) {
		StringBuilder countSelectionQuery = new StringBuilder();
		countSelectionQuery.append(SQL_SELECT_NEWS_COUNT_BY_FILTER);
		if(!filter.isSetAuthorId() && !filter.isSetSelectedTags()) {
			return null;
		} else {
			countSelectionQuery.append(buildSearchCondition(filter));
		}
		countSelectionQuery.append(SELECT_COUNT_QUERY_END);
		return countSelectionQuery.toString();
	}
	
	/**
	 * Provides constructing SQL-statement depending on the state of searching filter.
	 * Returned SQL-statement will be used to select actually filtered news.
	 * @param filter the search filter instance
	 * @return SQL-command as String or null, if no filter parameters were set
	 */
	public static String getSelectionQueryByFilter(SearchFilter filter) {
		StringBuilder query = new StringBuilder();
		query.append(SQL_SELECT_NEWS_ON_PAGE_USE_FILTER);
		query.append(SQL_FILTER_NEWS_SELECTION);
		if(!filter.isSetAuthorId() && !filter.isSetSelectedTags()) {
			return null;
		} else {
			query.append(buildSearchCondition(filter));
		}
		query.append(SQL_SELECTING_ORDER_ROWNUM_DIAPASONE);
		return query.toString();
	}
	
	/**
	 * Provides constructing SQL-statement depending on the state of searching filter.
	 * Returned SQL-statement will be used to select actually filtered news.
	 * @param filter the searching filter 
	 * @param nextOrPreviousPart the String part of query that contains query that selects NEXT or PREVIOUS news
	 * @return SQL-command as String, if no filter parameters were set
	 */
	public static String getNextOrPreviousSelectionQuery(SearchFilter filter, 
			final String nextOrPreviousPart) {
		StringBuilder query = new StringBuilder();
		query.append(SQL_SELECT_WITH_NUM_QUERY_START);
		if(!filter.isSetAuthorId() && !filter.isSetSelectedTags()) {
			return SQL_SELECT_DIRECTION_DEFAULT_FILTER + nextOrPreviousPart;
		} else {
			query.append(buildSearchCondition(filter));
		}
		query.append(SQL_SELECTING_ORDER);
		query.append(SQL_SELECT_WITH_NUM_QUERY_END);
		query.append(nextOrPreviousPart);
		return query.toString();
	}
}
>>>>>>> 6b913b5cc13c9dbe493c067f973f3a7891c40ec7:Task 5 (News BackEnd + FrontEnd)/news-management/news-common/src/main/java/com/epam/newsapp/persistence/dao/util/querybuild/NewsQueryBuilder.java
