package com.epam.newsapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.persistence.dao.ITagDAO;
import com.epam.newsapp.service.ITagService;

/**
 * <code>ITagService</code> implementation
 * @see <code>ITagService</code> for full information
 * @author Aleh_Rusak
 */
@Service
public final class TagServiceImpl implements ITagService {
	/**
	 * Injected Tag DAO
	 */
	@Autowired
	private ITagDAO tagDAO;
	
	public Long createTag(TagTO newTag) throws ServiceException, EntityAlreadyExistsException {
		try {
			if(tagDAO.findTagByName(newTag.getTagName()) == null) {
				return tagDAO.create(newTag);
			} else {
				throw new EntityAlreadyExistsException(Constants.TAG_ALREADY_EXISTS
						+ newTag.getTagName());
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<TagTO> getAllTags() throws ServiceException {
		try {
			return tagDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void deleteTags(Long[] tagIdArray) throws ServiceException {
		try {
			tagDAO.multipleDeletion(tagIdArray);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<TagTO> getNewsTags(Long newsId) throws ServiceException {
		try {
			return tagDAO.getNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void bindTagsWithNews(List<Long> tagIdList, Long newsId)
			throws ServiceException {
		try {
			tagDAO.bindTagsWithNews(tagIdList, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public TagTO getTag(Long tagId) throws ServiceException {
		try {
			return tagDAO.read(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void unbindTagsOnNews(List<Long> tagIdList, Long newsId)
			throws ServiceException {
		try {
			tagDAO.unbindTagsOnNews(tagIdList, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateTag(TagTO tagToUpdate) throws ServiceException {
		try {
			tagDAO.update(tagToUpdate);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteTag(Long entityId) throws ServiceException {
		try {
			tagDAO.delete(entityId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
