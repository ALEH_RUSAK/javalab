package com.epam.newsapp.persistence.dao;

import java.util.List;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.AbstractTO;

/**
 * Provides highest abstraction for DAO layer of the application.
 * @author Aleh_Rusak
 * @param <T> instance type
 * @param <PK> primary key type
 */
public interface IGenericDAO <TO extends AbstractTO, PK>  {
	
	/** 
	 * Inserts <code>T</code> instance into database
	 * @return primary key
	 */
	public PK create(TO newInstance) throws DAOException;

	/** 
     * Gets <code>T</code> instance by <code>PK<code> instance
     * @param id the key
     * @return <code>Type</code> instance
     */
	public TO read(PK id) throws DAOException;

    /**
     * Updates some database object
     * @param transientObject object that will replace database object
     */
    public PK update(TO transientObject) throws DAOException;

    /** 
     * Deletes <code>persistenceObject</code> from database
     * @param id of the object to be deleted from database
     */
    public void delete(PK id) throws DAOException;
    
	/**
	 * Multiple deletion of some instances using their ids
	 * @param idList the list of ids of objects to be deleted
	 * @throws DAOException
	 */
	public void multipleDeletion(PK[] idList) throws DAOException;
	
	/**
	 * Getting list of objects from database
	 * @return list of <code>T</code> instances
	 * @throws DAOException
	 */
	public List<TO> getAll() throws DAOException;
}
