package com.epam.newsapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.persistence.dao.ICommentDAO;
import com.epam.newsapp.service.ICommentService;

/**
 * <code>IAuthorService</code> implementation
 * @see <code>ICommentService</code> for full methods description
 * @author Aleh_Rusak
 */
@Service
public final class CommentServiceImpl implements ICommentService {
	/**
	 * Injected Comment DAO
	 */
	@Autowired
	private ICommentDAO commentDAO;

	@Override
	public List<CommentTO> getNewsComments(Long newsId) throws ServiceException {
		try {
			return commentDAO.getNewsComments(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		try {
			commentDAO.delete(commentId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long createComment(CommentTO newComment) throws ServiceException {
		try {
			return commentDAO.create(newComment);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public CommentTO getComment(Long commentId) throws ServiceException {
		try {
			return commentDAO.read(commentId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
