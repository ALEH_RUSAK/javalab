package com.epam.newsapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.INewsDAO;
import com.epam.newsapp.service.INewsService;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;

/**
 * <code>INewsService</code> implementation
 * @see <code>INewsService</code> contract description
 * @author Aleh_Rusak
 */
@Service
public class NewsServiceImpl implements INewsService {
	/**
	 * Injected News DAO
	 */
	@Autowired
	private INewsDAO newsDAO;
	
	@Override
	public Long createNews(NewsTO news) throws ServiceException {
		try {
			return newsDAO.create(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getAllNews() throws ServiceException {
		try {
			return newsDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long updateNews(NewsTO news) throws ServiceException {
		try {
			return newsDAO.update(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNews(Long[] newsIdArray) throws ServiceException {
		try {
			newsDAO.multipleDeletion(newsIdArray);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException {
		try {
			return newsDAO.findNewsByAuthor(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsByTags(Long[] selectedTags) throws ServiceException {
		try {
			return newsDAO.findNewsByTags(selectedTags);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public NewsTO getNews(Long newsId) throws ServiceException {
		try {
			return newsDAO.read(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long getNextNewsId(Long currentNewsId, SearchFilter filter) throws ServiceException {
		try {
			return newsDAO.getNextNewsId(currentNewsId, filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public Long getPreviousNewsId(Long currentNewsId, SearchFilter filter) throws ServiceException {
		try {
			return newsDAO.getPreviousNewsId(currentNewsId, filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> findNewsByAuthorAndTags(Long authorId, Long[] selectedTags) 
			throws ServiceException {
		try {
			return newsDAO.findNewsByAuthorAndTags(authorId, selectedTags);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsOnPage(Pager pager) throws ServiceException {
		try {
			return newsDAO.getNewsOnPage(pager.getFrom(), pager.getItemsPerPage());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsOnPageUseFilter(Pager pager, SearchFilter filter) throws ServiceException {
		try {
			return newsDAO.getNewsOnPageUseFilter(pager, filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long getNumberOfFilteredNews(SearchFilter filter) throws ServiceException {
		try {
			return newsDAO.getNumberOfFilteredNews(filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
