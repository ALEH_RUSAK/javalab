package com.epam.newsapp.exceptions;

/**
 * Provides common context of exceptions in this application.
 * @author Aleh_Rusak
 */
public abstract class BaseException extends Exception {
	/**
	 * The ID version
	 */
	private static final long serialVersionUID = -2056348909667290866L;
	/**
	 * Exception's unique title
	 */
	protected String title;
	/**
	 * Nested exception
	 */
	protected Throwable nestedException;
	
	/**
	 * 
	 */
	public String toString() {
		StringBuilder exceptionContent = new StringBuilder();
		exceptionContent.append("Exception occured: ");
		exceptionContent.append(title);
		if(nestedException != null) {
			exceptionContent.append(", nested exception is: ");
			exceptionContent.append(nestedException.toString());
		}
		return exceptionContent.toString();
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the nestedException
	 */
	public Throwable getNestedException() {
		return nestedException;
	}
}
