package com.epam.newsapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.service.IAuthorService;
import com.epam.newsapp.service.ICommentService;
import com.epam.newsapp.service.INewsManagerService;
import com.epam.newsapp.service.INewsService;
import com.epam.newsapp.service.ITagService;
import com.epam.newsapp.util.NavigationDirection;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;
import com.epam.newsapp.valueobject.NewsMessage;

/**
 * This class is a business layer facade.
 * Declares all services as private injections
 * and provides interface to execute methods of injected services.
 * @see <code>INewsManagerService</code> declaration
 * @author Aleh_Rusak
 */
@Service
public class NewsManagerServiceImpl implements INewsManagerService {
	/**
	 * News service
	 */
	@Autowired
	private INewsService newsService;
	/**
	 * Author service
	 */
	@Autowired
	private IAuthorService authorService;
	/**
	 * Tag service
	 */
	@Autowired
	private ITagService tagService;
	/**
	 * Comment service
	 */
	@Autowired
	private ICommentService commentService;
	
	/**
	 * Provides creation of <code>NewsMessage</code> instance
	 * by collecting different news features in one Value Object
	 * @param newsId the id of the news
	 * @return <code>NewsMessage</code> instance
	 * @throws ServiceException 
	 */
	private NewsMessage createPartialNewsMessage(Long newsId) throws ServiceException {
		NewsMessage valuableNewsMessage = new NewsMessage();
		valuableNewsMessage.setNews(newsService.getNews(newsId));
		valuableNewsMessage.setNewsAuthor(authorService.getNewsAuthor(newsId));
		valuableNewsMessage.setNewsTags(tagService.getNewsTags(newsId));
		return valuableNewsMessage;
	}
		
	/**
	 * Provides creation of <code>NewsMessage</code> instance
	 * by collecting different news features in one Value Object
	 * @param newsId the id of the news
	 * @return <code>NewsMessage</code> instance
	 * @throws ServiceException 
	 */
	private NewsMessage createFullNewsMessage(Long newsId) throws ServiceException {
		NewsMessage valuableNewsMessage = this.createPartialNewsMessage(newsId);
		valuableNewsMessage.setNewsComments(commentService.getNewsComments(newsId));
		return valuableNewsMessage;
	}
	
	/**
	 * Provides creation the list of <code>NewsMessage</code>s
	 * @param newsList the list of news
	 * @return the list of <code>NewsMessage</code>s
	 * @throws ServiceException
	 */
	private List<NewsMessage> createNewsMessageList(List<NewsTO> newsList)
			throws ServiceException {
		List<NewsMessage> newsMessageList = new ArrayList<NewsMessage>();
		for(NewsTO news: newsList) {
			Long newsId = news.getEntityId();
			newsMessageList.add(this.createFullNewsMessage(newsId));
		}
		return newsMessageList;
	}
	
	/* ================ News Manager's API =================== */
	
	@Override
	@Transactional
	public void addNews(NewsMessage newsMessage) throws ServiceException {
		NewsTO news = newsMessage.getNews();
		AuthorTO author = newsMessage.getNewsAuthor();
		List<TagTO> tags = newsMessage.getNewsTags();
		List<Long> tagIdList = new ArrayList<Long>(tags.size());
		for(TagTO t: tags) {
			tagIdList.add(t.getEntityId());
		}
		news.setCreationDate(news.getModificationDate());
		Long newsId = newsService.createNews(news);
		tagService.bindTagsWithNews(tagIdList, newsId);
		authorService.bindAuthorWithNews(author.getEntityId(), newsId);
	}
	
	@Override
	public void updateNews(NewsMessage newsMessage) throws ServiceException {
		NewsTO news = newsMessage.getNews();
		List<TagTO> newNewsTags = newsMessage.getNewsTags();
		Long newsId = newsService.updateNews(news);
		List<TagTO> currentTags = tagService.getNewsTags(newsId);
		/* Updating news tags */
		if(!currentTags.equals(newNewsTags)) {
			List<Long> currentTagsIdList = new ArrayList<Long>(currentTags.size());
			for(TagTO t: currentTags) {
				currentTagsIdList.add(t.getEntityId());
			}
			tagService.unbindTagsOnNews(currentTagsIdList, newsId);
			List<Long> newTagsIdList = new ArrayList<Long>(newNewsTags.size());
			for(TagTO t: newNewsTags) {
				newTagsIdList.add(t.getEntityId());
			}
			tagService.bindTagsWithNews(newTagsIdList, newsId);
		}
		/* Updating news author */
		AuthorTO currentNewsAuthor = authorService.getNewsAuthor(newsId);
		AuthorTO newNewsAuthor = newsMessage.getNewsAuthor();
		if(!currentNewsAuthor.equals(newNewsAuthor)) {
			authorService.unbindAuthorOnNews(currentNewsAuthor.getEntityId(), newsId);
			authorService.bindAuthorWithNews(newNewsAuthor.getEntityId(), newsId);
		}
	}

	@Override
	@Transactional
	public List<NewsMessage> getNewsList() throws ServiceException {
		return this.createNewsMessageList(newsService.getAllNews());
	}
	
	@Override
	@Transactional
	public NewsMessage getSingleNews(Long newsId) throws ServiceException {
		return this.createFullNewsMessage(newsId);
	}
	
	@Override
	@Transactional
	public List<NewsMessage> findNewsByAuthor(String authorName) throws ServiceException {
		AuthorTO newsAuthor = authorService.findAuthorByName(authorName);
		Long authorId = newsAuthor.getEntityId();
		return this.createNewsMessageList(newsService.getNewsByAuthor(authorId));
	}
	
	@Override
	@Transactional
	public List<NewsMessage> findNewsByTags(Long[] selectedTags) throws ServiceException  {
		return this.createNewsMessageList(newsService.getNewsByTags(selectedTags));
	}

	@Override
	public void deleteNews(Long[] newsIdArray) throws ServiceException {
		newsService.deleteNews(newsIdArray);
	}
	
	@Override
	@Transactional
	public void addComment(CommentTO newComment) throws ServiceException {
		commentService.createComment(newComment);
	}
	
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.deleteComment(commentId);
	}
	
	@Override
	public Long addTag(TagTO newTag) throws ServiceException, EntityAlreadyExistsException  {
		return tagService.createTag(newTag);
	}
	
	@Override
	public List<TagTO> deleteTags(Long[] tagIdArray) throws ServiceException {
		tagService.deleteTags(tagIdArray);
		return tagService.getAllTags();
	}
	
	@Override
	public Long addAuthor(AuthorTO author) throws ServiceException, EntityAlreadyExistsException {
		return authorService.createAuthor(author);
	}

	@Override
	public Long getNewsIdToNavigate(Long currentNewsId, SearchFilter filter,
			NavigationDirection direction) throws ServiceException {
		Long newsId = null;
		switch(direction) {
			case NEXT:
				newsId = newsService.getNextNewsId(currentNewsId, filter);
				break;
			case PREVIOUS:
				newsId = newsService.getPreviousNewsId(currentNewsId, filter);
				break;
		}
		return newsId;
	}

	@Override
	public List<AuthorTO> getAllAuthors() throws ServiceException {
		return authorService.getAllAuthors();
	}

	@Override
	public List<TagTO> getAllTags() throws ServiceException {
		return tagService.getAllTags();
	}

	@Override
	public List<NewsMessage> getNewsOnPage(Pager pager) throws ServiceException {
		return this.createNewsMessageList(newsService.getNewsOnPage(pager));
	}

	@Override
	public List<NewsMessage> getNewsOnPageUseFilter(Pager pager, SearchFilter filter)
			throws ServiceException {
		return this.createNewsMessageList(newsService.getNewsOnPageUseFilter(pager, filter));
	}
	
	@Override
	public Long getNumberOfFilteredNews(SearchFilter filter) throws ServiceException {
		return newsService.getNumberOfFilteredNews(filter);
	}

	@Override
	public List<NewsMessage> findNewsByAuthorAndTags(Long authorId,
			Long[] selectedTags) throws ServiceException {
		return this.createNewsMessageList(newsService.findNewsByAuthorAndTags(authorId, selectedTags));
	}
	
	@Override
	public TagTO getTag(Long tagId) throws ServiceException {
		return tagService.getTag(tagId);
	}
	
	@Override
	public AuthorTO getAuthor(Long authorId) throws ServiceException {
		return authorService.getAuthor(authorId);
	}

	@Override
	public void updateTag(TagTO tagToUpdate) throws ServiceException {
		tagService.updateTag(tagToUpdate);
	}

	@Override
	public void updateAuthor(AuthorTO authorToUpdate) throws ServiceException {
		authorService.updateAuthor(authorToUpdate);
	}

	@Override
	public void deleteTag(Long entityId) throws ServiceException {
		tagService.deleteTag(entityId);
	}

	@Override
	public void deleteAuthor(Long entityId) throws ServiceException {
		authorService.deleteAuthor(entityId);
	}
}
