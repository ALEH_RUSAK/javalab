package com.epam.newsapp.persistence.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.persistence.dao.ICommentDAO;
import com.epam.newsapp.persistence.dao.util.DBResourceManager;
import com.epam.newsapp.persistence.dao.util.mapper.IRowMapper;
import com.epam.newsapp.persistence.dao.util.mapper.IStatementMapper;

/**
 * The DAO implementation to work with comments
 * @see <code>AbstractDAO</code> description and declaration
 * @author Aleh_Rusak
 */
@Repository
public class CommentDAOImpl extends AbstractDAO<CommentTO> implements ICommentDAO {
	/**
	 * Comment PK identifier
	 */
	private static final String COMMENT_ID = "comment_id";
	/**
	 * SQL commands
	 */
	private static final String SQL_INSERT_COMMENT = "INSERT INTO Comments (comment_id, comment_text, creation_date, news_id) VALUES (COMMENT_SEQ.nextval,?,?,?)";
	private static final String SQL_SELECT_COMMENT_BY_ID = "SELECT comment_id, comment_text, creation_date, news_id FROM Comments WHERE comment_id = ?";
	private static final String SQL_UPDATE_COMMENT = "UPDATE Comments SET comment_text = ? , creation_date = ? , news_id = ? WHERE comment_id = ?";
	private static final String SQL_DELETE_COMMENT = "DELETE FROM Comments WHERE comment_id = ?";
	private static final String SQL_SELECT_ALL_COMMENTS = "SELECT comment_id, comment_text, creation_date, news_id FROM Comments";
	private static final String SQL_SELECT_ALL_NEWS_COMMENTS = "SELECT comment_id, comment_text, creation_date, news_id FROM Comments WHERE news_id = ?";

	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>CommentTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class CommentRowMapper implements IRowMapper<CommentTO> {
		public CommentTO mapRow(ResultSet resultSet) throws SQLException {
			CommentTO comment = new CommentTO();
			comment.setEntityId(resultSet.getLong(1));
			comment.setCommentText(resultSet.getString(2));
			comment.setCreationDate(resultSet.getTimestamp(3));
			comment.setNewsId(resultSet.getLong(4));
			return comment;
		}
	}
	
	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>CommentTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class CommentStatementMapper implements IStatementMapper<CommentTO> {
		public int mapStatement(CommentTO transferObject,
				PreparedStatement preparedStatement) throws SQLException {
			preparedStatement.setString(1, transferObject.getCommentText());
			preparedStatement.setTimestamp(2, new Timestamp(transferObject.getCreationDate().getTime()));
			preparedStatement.setLong(3, transferObject.getNewsId());
			return 3;
		}	
	}
	
	/**
	 * Default constructor creates mapper objects
	 */
	public CommentDAOImpl() {
		rowMapper = new CommentRowMapper();
		statementMapper = new CommentStatementMapper();
	}

	public Long create(CommentTO newComment) throws DAOException {
		return querySender.performCreate(newComment, SQL_INSERT_COMMENT, COMMENT_ID, 
				statementMapper);
	}

	public CommentTO read(Long commentId) throws DAOException {
		return querySender.performRead(commentId, SQL_SELECT_COMMENT_BY_ID, rowMapper);
	}
	
	public Long update(CommentTO comment) throws DAOException {
		return querySender.performUpdate(comment, SQL_UPDATE_COMMENT, statementMapper);
	}

	public void delete(Long commentId) throws DAOException {
		querySender.performDelete(commentId, SQL_DELETE_COMMENT);
	}

	public void multipleDeletion(Long[] commentIdArray) throws DAOException {
		querySender.performMultipleDeletion(commentIdArray, SQL_DELETE_COMMENT);
	}

	public List<CommentTO> getAll() throws DAOException {
		return querySender.performGetAll(SQL_SELECT_ALL_COMMENTS, rowMapper);
	}

	public List<CommentTO> getNewsComments(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    try {
	    	connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_NEWS_COMMENTS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			return databaseUtil.createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement, resultSet);
		}
	}
}
