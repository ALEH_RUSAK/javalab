package com.epam.newsapp.persistence.dao;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.AuthorTO;

/**
 * <code>IGenericDAO<TO, PK></code> extension for operations with AUTHORS
 * @author Aleh_Rusak
 */
public interface IAuthorDAO extends IGenericDAO<AuthorTO, Long> {
	
	/**
	 * Binding author to news
	 * @param authorId the id of author
	 * @param newsId the id of news
	 * @throws DAOException
	 */
	public void bindAuthorWithNews(Long authorId, Long newsId) throws DAOException;
	
	/**
	 * Unbinding author on news
	 * @param authorId the id of author
	 * @param newsId the id of news
	 * @throws DAOException
	 */
	public void unbindAuthorOnNews(Long authorId, Long newsId) throws DAOException;
	
	/**
	 * Getting author of news
	 * @param newsId news id
	 * @return list of news authors
	 * @throws DAOException
	 */
	public AuthorTO getNewsAuthor(Long newsId) throws DAOException;
	
	/**
	 * Finds author by his name
	 * @param authorName name of the author
	 * @return <code>AuthorTO</code> instance or null if author with that name wasn't found
	 * @throws DAOException
	 */
	public AuthorTO findAuthorByName(String authorName) throws DAOException;
}
