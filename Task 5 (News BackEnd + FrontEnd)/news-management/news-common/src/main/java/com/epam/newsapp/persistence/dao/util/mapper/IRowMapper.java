package com.epam.newsapp.persistence.dao.util.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.newsapp.model.AbstractTO;

/**
 * Mapping <code>ResultSet</code> on some <code>AbstractTO</code> instance
 * @author Aleh_Rusak
 * @param <TO> an <code>AbstractTO</code> instance
 */
public interface IRowMapper<TO extends AbstractTO> {

	/**
	 * Gets <code>ResultSet</code> values
	 * and initializes object with it.
	 * @param resultSet the <code>ResultSet</code> instance
	 * @return initialized instance
	 * @throws SQLException 
	 */
	public TO mapRow(ResultSet resultSet) throws SQLException;
}
