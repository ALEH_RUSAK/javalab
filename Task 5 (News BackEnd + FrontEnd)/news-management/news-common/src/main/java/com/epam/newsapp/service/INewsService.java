package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;

/**
 * Service interface for news actions
 * @author Aleh_Rusak
 */
public interface INewsService {
	
	/**
	 * Provides service layer encapsulation for creating news
	 * @param newsMessage <code>NewsTO</code> instance to be saved
	 * @return form to be proceeded
	 * @throws ServiceException
	 */
	public Long createNews(NewsTO newsMessage) throws ServiceException;

	/**
	 * Provides service layer encapsulation for updating news
	 * @param newsMessage news to be updated
	 * @return the id of updated news
	 * @throws ServiceException
	 */
	public Long updateNews(NewsTO newsMessage) throws ServiceException;
	
	/**
	 * Provides service layer encapsulation for deleting news
	 * @param newsIdArray the array of news ids
	 * @throws ServiceException
	 */
	public void deleteNews(Long[] newsIdArray) throws ServiceException;
	 
	/**
	 * Getting news of concrete author
	 * @param authorId author's id
	 * @return the list of news wrote by author with authorId id
	 * @throws ServiceException
	 */
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Getting all news by selected tags
	 * @param tags ids list
	 * @return the list of news connected with tags
	 * @throws ServiceException
	 */
	public List<NewsTO> getNewsByTags(Long[] selectedTags) throws ServiceException;
	
	/**
	 * Getting news message by it's id
	 * @param newsId the id of the news
	 * @return <code>NewsTO</code> instance
	 * @throws ServiceException
	 */
	public NewsTO getNews(Long newsId) throws ServiceException;

	/**
	 * Provides service layer encapsulation for viewing the list of news messages 
	 * @return the list of news
	 * @throws ServiceException
	 */
	public List<NewsTO> getAllNews() throws ServiceException;
	
	/**
	 * Provides service layer encapsulation for getting NEXT <code>NewsTO</code> instance
	 * @param currentNewsId the id of current displayed news
	 * @return the id of next or previous news
	 * @throws ServiceException
	 */
	public Long getNextNewsId(Long currentNewsId, SearchFilter filter) throws ServiceException;
	
	/**
	 * Provides service layer encapsulation for getting PREVIOUS <code>NewsTO</code> instance
	 * @param currentNewsId the id of current displayed news
	 * @return the id of next or previous news
	 * @throws ServiceException
	 */
	public Long getPreviousNewsId(Long currentNewsId, SearchFilter filter) throws ServiceException;
	
	/**
	 * Provides service layer encapsulation for getting list of news found by it's author and the list of it's tags
	 * @param authorId the id of news author
	 * @param selectedTags the array of tag's ids
	 * @return the list of <code>NewsTO</code> instances
	 * @throws ServiceException
	 */
	public List<NewsTO> findNewsByAuthorAndTags(Long authorId, Long[] selectedTags) 
			throws ServiceException;
	
	/**
	 * Provides service layer encapsulation for getting fixed count of news. Useful for paging. 
	 * @param from the index from which we start selecting news
	 * @param count the count of news should be selected
	 * @return the list of news
	 * @throws ServiceException
	 */
	public List<NewsTO> getNewsOnPage(Pager pager) throws ServiceException;
	
	/**
	 * Provides service layer encapsulation for getting fixed count of news using searching filter.
	 * @param pager the instance of <code>Pager</code>
	 * @param filter the instance of <code>SearchFilter</code>
	 * @throws ServiceException
	 */
	public List<NewsTO> getNewsOnPageUseFilter(Pager pager, SearchFilter filter)
			throws ServiceException;
	
	/**
	 * Provides service layer encapsulation for getting number of news selected 
	 * via help of <code>SearchFilter</code>.
	 * @param filter the instance of <code>SearchFilter</code>
	 * @return number of selected news
	 * @throws ServiceException
	 */
	public Long getNumberOfFilteredNews(SearchFilter filter) throws ServiceException;
}
