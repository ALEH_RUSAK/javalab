package com.epam.newsapp.exceptions;

/**
 * Thrown when there is an attempt to create duplicate row in database,
 * where we can't do it.
 * @author Aleh_Rusak
 */
public class EntityAlreadyExistsException extends BaseException {
	/**
	 * The Id version
	 */
	private static final long serialVersionUID = 2486397187170428700L;
	
	public EntityAlreadyExistsException() {}
	
	public EntityAlreadyExistsException(Throwable nestedException) {
		this.title = EntityAlreadyExistsException.class.toString();
		this.nestedException = nestedException;
	}
	
	public EntityAlreadyExistsException(String exceptionMessage) {
		this.title = EntityAlreadyExistsException.class.toString();
	}
}
