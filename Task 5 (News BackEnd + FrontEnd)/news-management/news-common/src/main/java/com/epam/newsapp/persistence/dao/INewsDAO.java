package com.epam.newsapp.persistence.dao;

import java.util.List;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;

/**
 * <code>IGenericDAO<TO, PK></code> extension for operations with NEWS
 * @author Aleh_Rusak
 */
public interface INewsDAO extends IGenericDAO<NewsTO, Long> {
	
	/**
	 * Provides getting list of news found by it's author
	 * @param authorId the id of news author
	 * @return the list of <code>NewsTO</code> instances
	 * @throws DAOException
	 */
	public List<NewsTO> findNewsByAuthor(Long authorId) throws DAOException;
	
	/**
	 * Provides getting list of news found by the list of it's tags
	 * @param selectedTags array that containg tag's ids
	 * @return the list of <code>NewsTO</code> instances
	 * @throws DAOException
	 */
	public List<NewsTO> findNewsByTags(Long[] selectedTags) throws DAOException;
	
	/**
	 * Provides getting list of news found by it's author and the list of it's tags
	 * @param authorId the id of news author
	 * @param tagList the list of tag's ids
	 * @return the list of <code>NewsTO</code> instances
	 * @throws DAOException
	 */
	public List<NewsTO> findNewsByAuthorAndTags(Long authorId, Long[] selectedTags) throws DAOException;
	
	/**
	 * Provides getting id of the news that is NEXT in current selection.
	 * News are sorted by the number of it's comments.
	 * @param currentNewsId the id of current displayed news
	 * @return the id of next or previous news or 0L, if there is no NEXT news
	 * @throws DAOException
	 */
	public Long getNextNewsId(Long currentNewsId, SearchFilter filter) throws DAOException;
	
	/**
	 * Provides getting id of the news that is PREVIOUS in current selection.
	 * News are sorted by the number of it's comments.
	 * @param currentNewsId the id of current displayed news
	 * @return the id of next or previous news or 0L, if there is no PREVIOUS news
	 * @throws DAOException
	 */
	public Long getPreviousNewsId(Long currentNewsId, SearchFilter filter) throws DAOException;
	
	/**
	 * Provides getting fixed count of news. Useful for paging. 
	 * @param from the index from which we start selecting news
	 * @param count the count of news should be selected
	 * @return the list of news
	 * @throws DAOException
	 */
	public List<NewsTO> getNewsOnPage(int from, int count) throws DAOException;
	
	/**
	 * Provides getting just count of news which passed through filter conditions in a NEWS table.
	 * @return the count of news in a NEWS table
	 */
	public Long getNumberOfFilteredNews(SearchFilter filter) throws DAOException;
	
	/**
	 * Provides getting fixed count of news using searching filter on author and tags
	 * @param pager the instance of <code>Pager</code> class
	 * @param filter the instance of <code>SearchFilter</code> class
	 * @return the list of news
	 * @throws DAOException
	 */
	public List<NewsTO> getNewsOnPageUseFilter(Pager pager, SearchFilter filter) 
			throws DAOException;
}
