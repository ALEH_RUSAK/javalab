package com.epam.newsapp.persistence.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.INewsDAO;
import com.epam.newsapp.persistence.dao.query.NewsQuery;
import com.epam.newsapp.persistence.dao.util.DBResourceManager;
import com.epam.newsapp.persistence.dao.util.mapper.IRowMapper;
import com.epam.newsapp.persistence.dao.util.mapper.IStatementMapper;
import com.epam.newsapp.persistence.dao.util.querybuild.NewsQueryBuilder;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;

/**
 * The DAO implementation for working with <code>NewsTO</code> 
 * and it's representation in the database.
 * @see <code>AbstractDAO</code> description
 * @author Aleh_Rusak
 */
@Repository
public class NewsDAOImpl extends AbstractDAO<NewsTO> implements INewsDAO {
	/**
	 * PK identifier
	 */
	private static final String NEWS_ID = "news_id";
	
	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>NewsTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class NewsRowMapper implements IRowMapper<NewsTO> {
		public NewsTO mapRow(ResultSet resultSet) throws SQLException {
			NewsTO newsMessage = new NewsTO();
			newsMessage.setEntityId(resultSet.getLong(1));
			newsMessage.setTitle(resultSet.getString(2));
			newsMessage.setBrief(resultSet.getString(3));
			newsMessage.setContent(resultSet.getString(4));
			newsMessage.setCreationDate(resultSet.getTimestamp(5));
			newsMessage.setModificationDate(resultSet.getDate(6));
			return newsMessage;
		}
	}
	
	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>NewsTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class NewsStatementMapper implements IStatementMapper<NewsTO> {
		public int mapStatement(NewsTO news, PreparedStatement preparedStatement) throws SQLException {
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getBrief());
			preparedStatement.setString(3, news.getContent());
			preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()));
			return 5;
		}	
	}
	
	/**
	 * Method provides navigating through news using searching criteria
	 * @param preparedStatement the <code>PreparedStatement</code> instance
	 * @param filter the search filter instance
	 * @param currentNewsId the id of the currently viewing news
	 * @return newsIdToNavigate to go if next or previous button was pushed
	 * @throws SQLException
	 */
	private Long getNewsIdToNavigate(PreparedStatement preparedStatement, SearchFilter filter, 
			Long currentNewsId) throws SQLException {
		ResultSet resultSet = null;
		int columnIndex = 1;
		if(filter.getAuthorId() != 0L) {
    		preparedStatement.setLong(columnIndex, filter.getAuthorId());
    		columnIndex++;
    	}
    	preparedStatement.setLong(columnIndex, currentNewsId);
		resultSet = preparedStatement.executeQuery();
		Long newsIdToNavigate = 0L; 
		if(resultSet.next()) {
			newsIdToNavigate = resultSet.getLong(1);
		} 
		preparedStatement.close();
		resultSet.close();
		return newsIdToNavigate;
	}
	
	/**
	 * Default constructor creates objects of mappers
	 */
	public NewsDAOImpl() {
		rowMapper = new NewsRowMapper();
		statementMapper = new NewsStatementMapper();
	}
	
	@Override
	public Long create(NewsTO newsMessage) throws DAOException {
		return querySender.performCreate(newsMessage, NewsQuery.SQL_INSERT_NEWS, NEWS_ID, statementMapper);
	}

	@Override
	public NewsTO read(Long newsId) throws DAOException {
	    return querySender.performRead(newsId, NewsQuery.SQL_SELECT_NEWS_BY_ID, rowMapper);
	}
	
	@Override
	public Long update(NewsTO newsMessage) throws DAOException {
		return querySender.performUpdate(newsMessage, NewsQuery.SQL_UPDATE_NEWS, statementMapper);
	}

	@Override
	public void delete(Long newsId) throws DAOException {
		querySender.performDelete(newsId, NewsQuery.SQL_DELETE_NEWS);
	}
	
	@Override
	public void multipleDeletion(Long[] newsIdList) throws DAOException {
		querySender.performMultipleDeletion(newsIdList, NewsQuery.SQL_DELETE_NEWS);
	}
	
	@Override
	public List<NewsTO> getAll() throws DAOException {
		return querySender.performGetAll(NewsQuery.SQL_SELECT_ALL_NEWS, new NewsRowMapper());
	}

	@Override
	public List<NewsTO> findNewsByAuthor(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
	    try {
	    	connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(NewsQuery.SQL_FIND_NEWS_BY_AUTHOR);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			return databaseUtil.createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement, resultSet);
		}
	}

	@Override
	public List<NewsTO> findNewsByTags(Long[] selectedTags) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
	    try {
	    	connection = DataSourceUtils.getConnection(dataSource);
	    	preparedStatement = connection.prepareStatement(NewsQuery.SQL_FIND_NEWS_BY_TAGS +
	    			databaseUtil.prepareWhereInQuery(selectedTags));
	    	resultSet = preparedStatement.executeQuery();
			return databaseUtil.createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement, resultSet);
		}
	}
	
	@Override
	public Long getNextNewsId(Long currentNewsId, SearchFilter filter) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
	    	connection = DataSourceUtils.getConnection(dataSource);
	    	String query = NewsQueryBuilder.getNextOrPreviousSelectionQuery(filter, 
	    			NewsQuery.SQL_SELECT_NEXT);
	    	preparedStatement = connection.prepareStatement(query);
	    	return this.getNewsIdToNavigate(preparedStatement, filter, currentNewsId);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement);
		}
	}
	
	@Override
	public Long getPreviousNewsId(Long currentNewsId, SearchFilter filter) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
	    	connection = DataSourceUtils.getConnection(dataSource);
	    	String query = NewsQueryBuilder.getNextOrPreviousSelectionQuery(filter, 
	    			NewsQuery.SQL_SELECT_PREVIOUS);
	    	preparedStatement = connection.prepareStatement(query);
	    	return this.getNewsIdToNavigate(preparedStatement, filter, currentNewsId);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement);
		}
	}

	@Override
	public List<NewsTO> findNewsByAuthorAndTags(Long authorId, Long[] selectedTags) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
	    try {
	    	connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(NewsQuery.SQL_FIND_NEWS_BY_AUTHOR_AND_TAGS
					+ databaseUtil.prepareWhereInQuery(selectedTags));
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			return databaseUtil.createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement, resultSet);
		}
	}

	@Override
	public List<NewsTO> getNewsOnPage(int from, int count) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
	    try {
	    	connection = DataSourceUtils.getConnection(dataSource);
	    	preparedStatement = connection.prepareStatement(NewsQuery.SQL_SELECT_NEWS_ON_PAGE);
	    	preparedStatement.setInt(1, from + count);
	    	preparedStatement.setInt(2, from);
	    	resultSet = preparedStatement.executeQuery();
			return databaseUtil.createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement, resultSet);
		}
	}
	
	@Override
	public List<NewsTO> getNewsOnPageUseFilter(Pager pager, SearchFilter filter)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
	    try {
	    	connection = DataSourceUtils.getConnection(dataSource);
	    	String selectionQuery = NewsQueryBuilder.getSelectionQueryByFilter(filter);
	    	int columnIndex = 1;
	    	if(selectionQuery != null) {
	    		preparedStatement = connection.prepareStatement(selectionQuery);
	    		if(filter.getAuthorId() != 0L) {
	    			preparedStatement.setLong(columnIndex, filter.getAuthorId());
	    			columnIndex++;
	    		}
	    	} else { 
	    		return this.getNewsOnPage(pager.getFrom(), pager.getItemsPerPage());
	    	}
	    	preparedStatement.setInt(columnIndex, pager.getFrom() + pager.getItemsPerPage());
	    	preparedStatement.setInt(columnIndex + 1, pager.getFrom());
	    	resultSet = preparedStatement.executeQuery();
			return databaseUtil.createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement, resultSet);
		}
	}

	@Override
	public Long getNumberOfFilteredNews(SearchFilter filter) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
	    	connection = DataSourceUtils.getConnection(dataSource);
	    	String query = NewsQueryBuilder.getQueryForNumberOfSelectedByFilter(filter);
	    	if(query != null) {
	    		preparedStatement = connection.prepareStatement(query);
	    	} else { 
	    		preparedStatement = connection.prepareStatement(NewsQuery.SQL_SELECT_ALL_COUNT);
	    	}
	    	resultSet = preparedStatement.executeQuery();
	    	if(resultSet.next()) {
	    		return resultSet.getLong(1);
	    	} else {
	    		return 0L;
	    	} 
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBResourceManager.closeJdbcResources(dataSource, connection, preparedStatement, resultSet);
		}
	}
}
