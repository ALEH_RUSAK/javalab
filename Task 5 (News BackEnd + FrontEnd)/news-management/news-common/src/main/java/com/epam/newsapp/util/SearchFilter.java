package com.epam.newsapp.util;

import java.io.Serializable;

/**
 * Provides functionality to create and store information about searching news by criterias 
 * @author Aleh_Rusak
 */
public class SearchFilter implements Serializable {

	private static final long serialVersionUID = 8454609764560824419L;

	private Long authorId;

	private Long[] selectedTags;
	
	public SearchFilter() {
		authorId = 0L;
		selectedTags = new Long[0];
	}
	
	public boolean isSetAuthorId() {
		if(authorId != null && authorId != 0L) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isSetSelectedTags() {
		if(selectedTags != null && selectedTags.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the authorId
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * @param authorId the authorId to set
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	
	/**
	 * @return the selectedTags
	 */
	public Long[] getSelectedTags() {
		return selectedTags;
	}

	/**
	 * @param selectedTags the selectedTags to set
	 */
	public void setSelectedTags(Long[] selectedTags) {
		this.selectedTags = selectedTags;
	}
}
