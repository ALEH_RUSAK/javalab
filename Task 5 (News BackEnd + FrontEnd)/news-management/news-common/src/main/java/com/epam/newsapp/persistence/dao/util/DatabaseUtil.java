package com.epam.newsapp.persistence.dao.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.epam.newsapp.model.AbstractTO;
import com.epam.newsapp.persistence.dao.util.mapper.IRowMapper;

/**
 * Provides encapsulation for some database methods and operations
 * @author Aleh_Rusak
 * @param <TO> an <code>AbstractTO</code> extension
 */
public class DatabaseUtil<TO extends AbstractTO> {
	
	/**
	 * Provides getting list of TO instances
	 * @param resultSet the <code>ResultSet</code> instance
	 * @return list of created instances of <code>TO</code>
	 * @throws SQLException
	 */
	public List<TO> createInstancesList(ResultSet resultSet, IRowMapper<TO> rowMapper) 
			throws SQLException {
		List<TO> instancesList = new ArrayList<TO>();
		while(resultSet.next()) {
			instancesList.add(rowMapper.mapRow(resultSet));
		}
		return instancesList;
	}
	
	/**
	 * Provides safe batch execution using partial execution.
	 * The size of batch is no more than 2000.
	 * @param idList the list if items id's
	 * @param preparedStatement <code>PreparedStatement</code> instance
	 * @throws SQLException
	 */
	public void executeBatch(Long[] idList, PreparedStatement preparedStatement)
			throws SQLException {
		final int batchSize = 2000;
		int batchIndex = 0;
		for(Long id: idList) {
			preparedStatement.setLong(1, id);
			preparedStatement.addBatch();
			if(batchIndex >= batchSize) {
				preparedStatement.executeBatch();
				batchIndex = 0;
			}
			batchIndex++;
		}
		preparedStatement.executeBatch();
	}
	
	/**
	 * Provides preparing query to be used in WHERE IN () structure 
	 * @param paramArray the array of <code>TO</code> instances
	 * @return string to be used in WHERE IN () structure
	 */
	public String prepareWhereInQuery(Long[] paramArray) {
		StringBuilder resultString = new StringBuilder(); 
		resultString.append("(");
		for(Long currentParam: paramArray) {
			resultString.append(currentParam.toString());
			resultString.append(",");
		}
		resultString.deleteCharAt(resultString.length() - 1);
		resultString.append(") ");
		return resultString.toString();
	}
}
