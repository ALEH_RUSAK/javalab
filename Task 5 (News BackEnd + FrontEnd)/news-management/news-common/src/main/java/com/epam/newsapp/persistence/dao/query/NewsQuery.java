package com.epam.newsapp.persistence.dao.query;

public final class NewsQuery {

	private NewsQuery() {}
	
	/**
	 * SQL statements
	 */
	public static final String PL_CALL_GET_NEWS_BY_FILTER = "{call getNewsByFilter(?, ?, ?)}";
	public static final String SQL_SELECT_ALL_COUNT = "SELECT COUNT(*) FROM News";
	public static final String SQL_DELETE_NEWS = "DELETE FROM News WHERE news_id = ?";
	public static final String SQL_UPDATE_NEWS = "UPDATE News SET title = ?, brief = ?, content = ?, creation_date = ?, modification_date = ? WHERE news_id = ?";
	public static final String SQL_INSERT_NEWS = "INSERT INTO News (news_id, title, brief, content, creation_date, modification_date) VALUES (NEWS_SEQ.nextval,?,?,?,?,?)";
	public static final String SQL_SELECT_NEWS_BY_ID = "SELECT news_id, title, brief, content, creation_date, modification_date FROM NEWS WHERE news_id = ?";
	public static final String SQL_SELECT_COMMENTS_COUNT = "LEFT JOIN (SELECT news_id, COUNT(*) CNT FROM Comments GROUP BY news_id) c "
            + "ON n.news_id = c.news_id ";
	public static final String SQL_SELECTING_ORDER = "ORDER BY CNT DESC NULLS LAST, n.modification_date ";
	public static final String SQL_SELECT_ALL_NEWS = "SELECT n.news_id, n.title, n.brief, n.content, n.creation_date, "
			+ "n.modification_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT + SQL_SELECTING_ORDER;
	public static final String SQL_SELECT_NEWS_ON_PAGE = "SELECT news_id, title, brief, content, creation_date, modification_date FROM "
			+ "(SELECT ns.news_id, ns.title, ns.brief, ns.content, ns.creation_date, ns.modification_date, ROWNUM rnum FROM "
			+ "(SELECT n.news_id, n.title, n.brief, n.content, n.creation_date, n.modification_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT
			+ SQL_SELECTING_ORDER + " ) ns WHERE ROWNUM < ?) WHERE rnum >= ?";
	public static final String SQL_FIND_NEWS_BY_AUTHOR =
			"SELECT n.news_id, n.title, n.brief, n.content, "
			+ "n.modification_date, n.creation_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT 
			+ "INNER JOIN News_Authors ON News_Authors.news_id = n.news_id "
			+ "WHERE News_Authors.author_id = ?";
	public static final String SQL_FIND_NEWS_BY_TAGS = 
			"SELECT DISTINCT n.news_id, n.title, n.brief, n.content, "
			+ "n.modification_date, n.creation_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT 
			+ "INNER JOIN News_Tag ON News_Tag.news_id = n.news_id "
			+ "WHERE News_Tag.tag_id IN ";
	public static final String SQL_FIND_NEWS_BY_AUTHOR_AND_TAGS = 
			"SELECT DISTINCT n.news_id, n.title, n.brief, n.content, "
			+ "n.modification_date, n.creation_date, c.CNT FROM News n "
			+ SQL_SELECT_COMMENTS_COUNT 
			+ "INNER JOIN News_Tag ON News_Tag.news_id = n.news_id "
			+ "INNER JOIN News_Authors ON News_Authors.news_id = n.news_id "
			+ "WHERE News_Authors.author_id = ? AND News_Tag.tag_id IN ";
	public static final String SQL_SELECT_NEXT = "SELECT q.news_id FROM num_query q, "
			+ "(SELECT MIN(idx) AS idx FROM num_query q WHERE news_id = ?) nId "
			+ "WHERE q.idx IN (nId.idx + 1)";
	public static final String SQL_SELECT_PREVIOUS = "SELECT q.news_id FROM num_query q, "
			+ "(SELECT MIN(idx) AS idx FROM num_query q WHERE news_id = ?) nId "
			+ "WHERE q.idx IN (nId.idx - 1)";
}
