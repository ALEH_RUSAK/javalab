package com.epam.newsapp.model;

/**
 * Tag transfer object
 * @author Aleh_Rusak
 */
public final class TagTO extends AbstractTO {
	/**
	 * ID Version
	 */
	private static final long serialVersionUID = 3047185130962289441L;
	/**
	 * Tag name
	 */
	private String tagName;
	
	public TagTO() {}
	
	public TagTO(Long entityId, String tagName) {
		super(entityId);
		this.tagName = tagName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagTO other = (TagTO) obj;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}
	
	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
}
