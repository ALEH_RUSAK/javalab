package com.epam.newsapp.exceptions;

/**
 * Thrown when service layer's method execution is incorrect
 * @author Aleh_Rusak
 */
public class ServiceException extends BaseException {
	/**
	 * The ID version
	 */
	private static final long serialVersionUID = 2666849247698924153L;
	
	public ServiceException() {}
	
	public ServiceException(Throwable nestedException) {
		this.title = ServiceException.class.toString();
		this.nestedException = nestedException;
	}
}
