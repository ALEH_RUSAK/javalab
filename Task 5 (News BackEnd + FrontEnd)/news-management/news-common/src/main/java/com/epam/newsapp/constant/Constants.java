package com.epam.newsapp.constant;

/**
 * Constant experessions container
 * @author Aleh_Rusak
 */
public interface Constants {
	
	/* String expressions for logging */
	public static final String CANT_FIND_AUTHOR = "Author can not be found.";
	public static final String CANT_FIND_TAG = "Tag can not be found.";
	public static final String CANT_CREATE_INSTANCE = "Instance was not created. Can't create instance of a class:";
	public static final String CANT_READ_INSTANCE = "Can't read instance.";
	public static final String AUTHOR_ALREADY_EXISTS = "Author already exists. Author's name is: ";
	public static final String TAG_ALREADY_EXISTS = "Tag already exists. Tag's title is: ";
	public static final String WRONG_FILTER_STATE = "Wrong search filter state.";
	
	/* Web-controller constants */
	public static final String REDIRECT = "redirect:/";

	public static final String FORWARD_NEWS_LIST = "newsList";
	public static final String FORWARD_ADD_NEWS = "addNews";
	public static final String FORWARD_ERROR_PAGE = "errorPage";
	public static final String FORWARD_PAGE_NOT_FOUND_ERROR = "pageNotFound";
	public static final String FORWARD_VIEW_NEWS = "viewNews";
	public static final String FORWARD_ADD_AUTHOR = "addAuthor";
	public static final String FORWARD_ADD_TAG = "addTag";
	
	public static final String URL_NEWS_LIST = "news/list/";
	public static final String URL_VIEW_NEWS = "news/view/";
	public static final String URL_ADD_AUTHOR = "author/add/";
	public static final String URL_ADD_TAG = "tag/add/";
	
	public static final String REDIRECT_VIEW_NEWS = REDIRECT + URL_VIEW_NEWS;
	public static final String REDIRECT_NEWS_LIST = REDIRECT + URL_NEWS_LIST;
	public static final String REDIRECT_ADD_AUTHOR = REDIRECT + URL_ADD_AUTHOR;
	public static final String REDIRECT_ADD_TAG = REDIRECT + URL_ADD_TAG;

	/* Request/response parameter constants */
	public static final String NUMBER_OF_PAGES = "numberOfPages";
	public static final String AUTHORS_LIST = "authorsList";
	public static final String NEWS_LIST = "newsMessageList";
	public static final String CONCRETE_NEWS = "concreteNews";
	public static final String TAGS_LIST = "tagsList";
	public static final String CURRENT_PAGE = "currentPage";
	public static final String NEWS_MESSAGE = "newsMessage";
	public static final String NEW_AUTHOR = "newAuthor";
	public static final String FILTERS_AUTHOR = "filtersAuthor";
	public static final String FILTER = "filter";
	public static final String AUTHORS_NAME = "authorsName";
	public static final String ID_TO_UPDATE = "idToUpdate";
	public static final String TAG_DEF = "Tag";
	public static final String AUTHOR_DEF = "Author";
	public static final String PAGER = "pager";
	public static final String VIEWING_NEWS_ID = "viewingNewsId";
	public static final String ALREADY_EXISTS_ERROR = "alreadyExistsError";
}
