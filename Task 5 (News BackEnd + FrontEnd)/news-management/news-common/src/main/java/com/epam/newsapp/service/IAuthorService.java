package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;

/**
 * Service interface for author actions.
 * @author Aleh_Rusak
 */
public interface IAuthorService {
	
	/**
	 * Provides adding news author
	 * @param newAuthor <code>AuthorTO</code> instance
	 * @return the id of created tag
	 * @throws ServiceException
	 * @throws EntityAlreadyExistsException 
	 */
	public Long createAuthor(AuthorTO newAuthor) throws ServiceException, EntityAlreadyExistsException;
	
	/**
	 * Provides getting author
	 * @param authorId author's id
	 * @return <code>AuthorTO</code> instance
	 * @throws ServiceException
	 */
	public AuthorTO getAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Connects news with author
	 * @param authorId the id of the author
	 * @param newsId the id of the news
	 * @throws ServiceException
	 */
	public void bindAuthorWithNews(Long authorId, Long newsId) throws ServiceException;
	
	/**
	 * Unbinds author on news
	 * @param authorId the id of the author
	 * @param newsId the id of the news
	 * @throws ServiceException
	 */
	public void unbindAuthorOnNews(Long authorId, Long newsId) throws ServiceException;
	
	/**
	 * Provides getting news author
	 * @param newsId the id of the news
	 * @return <code>AuthorTO</code> instance
	 * @throws ServiceException
	 */
	public AuthorTO getNewsAuthor(Long newsId) throws ServiceException;
	
	/**
	 * Provides getting author by his name
	 * @param authorName
	 * @return <code>AuthorTO</code> instance
	 * @throws ServiceException
	 */
	public AuthorTO findAuthorByName(String authorName) throws ServiceException;
	
	/**
	 * Provides deleting authors
	 * @param authorIdArray the list of author's ids
	 * @throws ServiceException
	 */
	public void deleteAuthors(Long[] authorIdArray) throws ServiceException;

	/**
	 * Provides getting all available authors
	 * @return the list of authors
	 * @throws ServiceException
	 */
	public List<AuthorTO> getAllAuthors() throws ServiceException;

	public void updateAuthor(AuthorTO authorToUpdate) throws ServiceException;

	public void deleteAuthor(Long entityId) throws ServiceException;
}