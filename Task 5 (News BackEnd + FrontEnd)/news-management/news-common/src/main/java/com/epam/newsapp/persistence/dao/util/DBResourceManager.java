<<<<<<< HEAD:Task 5 (News BackEnd + FrontEnd)/news-management/news-common/src/main/java/com/epam/newsapp/persistence/dao/util/DatabaseResourceManager.java
package com.epam.newsapp.persistence.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsapp.exceptions.DAOException;

/**
 * Provides management for JDBC resources.
 * @author Aleh_Rusak
 */
public final class DatabaseResourceManager {

	/**
	 * Method closes all opened JDBC resources 
	 * and releases connection.
	 * @throws DAOException
	 */
	public static void closeJdbcResources(DataSource dataSource, Connection connection, 
			Statement statement, ResultSet resultSet) throws DAOException {
		try {
			try {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} finally {
					if (statement != null) {
						statement.close();
					}
				}			
			} finally {
				if (connection != null) {
					DataSourceUtils.releaseConnection(connection, dataSource);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
	
	/**
	 * Method closes all opened JDBC resources 
	 * and releases connection.
	 * @throws DAOException
	 */
	public static void closeJdbcResources(DataSource dataSource, Connection connection, 
			Statement preparedStatement) throws DAOException {
		try {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}			
			} finally {
				if (connection != null) {
					DataSourceUtils.releaseConnection(connection, dataSource);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
}
=======
package com.epam.newsapp.persistence.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsapp.exceptions.DAOException;

/**
 * Provides management for JDBC resources.
 * @author Aleh_Rusak
 */
public final class DBResourceManager {

	/**
	 * Method closes all opened JDBC resources 
	 * and releases connection.
	 * @throws DAOException
	 */
	public static void closeJdbcResources(DataSource dataSource, Connection connection, 
			Statement preparedStatement, ResultSet resultSet) throws DAOException {
		try {
			try {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} finally {
					if (preparedStatement != null) {
						preparedStatement.close();
					}
				}			
			} finally {
				if (connection != null) {
					DataSourceUtils.releaseConnection(connection, dataSource);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
	
	/**
	 * Method closes all opened JDBC resources 
	 * and releases connection.
	 * @throws DAOException
	 */
	public static void closeJdbcResources(DataSource dataSource, Connection connection, 
			Statement preparedStatement) throws DAOException {
		try {
			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}			
			} finally {
				if (connection != null) {
					DataSourceUtils.releaseConnection(connection, dataSource);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
}
>>>>>>> 6b913b5cc13c9dbe493c067f973f3a7891c40ec7:Task 5 (News BackEnd + FrontEnd)/news-management/news-common/src/main/java/com/epam/newsapp/persistence/dao/util/DBResourceManager.java
