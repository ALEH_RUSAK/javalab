package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.util.NavigationDirection;
import com.epam.newsapp.util.Pager;
import com.epam.newsapp.util.SearchFilter;
import com.epam.newsapp.valueobject.NewsMessage;

/**
 * The main business logic interface of an application.
 * Contains method to be called from the controller side.
 * Provides performing transactional business logic.
 * @author Aleh_Rusak
 */
public interface INewsManagerService {
	
	/* ====== NewsMessage business logic methods ====== */
	
	/**
	 * Provides adding new news message.
	 * @param newsMessage the news message to be added
	 * @throws ServiceException
	 */
	public void addNews(NewsMessage newsMessage) throws ServiceException; 
	
	/**
	 * Provides updating news message and it's content.
	 * @param newsMessage the news message to be updated
	 * @throws ServiceException
	 */
	public void updateNews(NewsMessage newsMessage) throws ServiceException;
	
	/**
	 * Provides viewing the list of news messages.
	 * @return the list of news messages
	 * @throws ServiceException
	 */
	public List<NewsMessage> getNewsList() throws ServiceException;
	
	/**
	 * Provides viewing concrete news 
	 * @param news the actual news
	 * @return the valuable new message
	 * @throws ServiceException
	 */
	public NewsMessage getSingleNews(Long newsId) throws ServiceException;
	
	/**
	 * Provides viewing news wrote by concrete author.
	 * @param authorName authors name
	 * @return the list of valuable news messages wrote by author with name param
	 * @throws ServiceException
	 */
	public List<NewsMessage> findNewsByAuthor(String authorName) throws ServiceException; 
	
	/**
	 * Provides viewing news connected with chosen tags.
	 * @param tagIdList the list of tag's ids
	 * @return the list of valuable news messages under chosen tags
	 * @throws ServiceException
	 */
	public List<NewsMessage> findNewsByTags(Long[] selectedTags) throws ServiceException;

	/**
	 * Provides deleting selected news.
	 * @param newsIdArray the list of news ids
	 * @throws ServiceException
	 */
	public void deleteNews(Long[] newsIdArray) throws ServiceException;
	
	/**
	 * Provides getting NEXT or PREVIOUS id of the news in current selection
	 * @param currentNewsId the id of current displayed news
	 * @param direction resolves the direction of selecting
	 * @param filter the <code>SearchFilter</code> instance
	 * @return the id of next or previous news
	 * @throws ServiceException
	 */
	public Long getNewsIdToNavigate(Long currentNewsId, SearchFilter filter,
			NavigationDirection direction) throws ServiceException ;
	
	/* ====== Tags business logic methods ====== */
	
	/**
	 * Provides updating tag.
	 * @param tagToUpdate the tag to be updated
	 */
	public void updateTag(TagTO tagToUpdate) throws ServiceException;
	
	/**
	 * Provides deleting selected tags.
	 * @param tagIdList the list of tags to be deleted
	 * @return the list of tags to view them
	 * @throws ServiceException
	 */
	public List<TagTO> deleteTags(Long[] tagIdArray) throws ServiceException;
	
	/**
	 * Provides adding new tag
	 * @param newTag tag to be added
	 * @return the id of created tag
	 * @throws ServiceException
	 * @throws EntityAlreadyExistsException 
	 */
	public Long addTag(TagTO newTag) throws ServiceException, EntityAlreadyExistsException;
	
	/**
	 * Provides getting the list of all available tags
	 * @return the list of tags
	 * @throws ServiceException
	 */
	public List<TagTO> getAllTags() throws ServiceException;
	
	/* ====== Comments business logic methods ====== */
	
	/**
	 * Provides adding comment to news
	 * @param newComment comment to be added
	 * @throws ServiceException
	 */
	public void addComment(CommentTO newComment) throws ServiceException;
	
	/**
	 * Provides deleting selected comment
	 * @param commentId the id of comment
	 * @throws ServiceException
	 */
	public void deleteComment(Long commentId) throws ServiceException;
	
	/**
	 * Provides adding new blogger.
	 * @param author the author to be added
	 * @return the id of created author
	 * @throws ServiceException
	 * @throws EntityAlreadyExistsException
	 */
	public Long addAuthor(AuthorTO author) throws ServiceException, EntityAlreadyExistsException;
	
	/**
	 * Provides getting the list of all available authors
	 * @return the list of authors
	 * @throws ServiceException
	 */
	public List<AuthorTO> getAllAuthors() throws ServiceException;

	/**
	 * Provides getting list of news found by it's author and the list of it's tags
	 * @param authorId the id of news author
	 * @param tagList the list of tag's ids
	 * @return the list of <code>NewsTO</code> instances
	 * @throws ServiceException
	 */
	public List<NewsMessage> findNewsByAuthorAndTags(Long authorId, Long[] selectedTags)
			throws ServiceException;
	
	/**
	 * Provides getting fixed count of news. Useful for paging. 
	 * @param from the index from which we start selecting news
	 * @param count the count of news should be selected
	 * @return the list of news
	 * @throws ServiceException
	 */
	public List<NewsMessage> getNewsOnPage(Pager pager) throws ServiceException;
	
	/**
	 * Provides for getting fixed count of news using searching filter.
	 * @param pager the instance of pager
	 * @param filter the instance of search filter
	 * @return the list of news messages
	 * @throws ServiceException
	 */
	public List<NewsMessage> getNewsOnPageUseFilter(Pager pager, SearchFilter filter)
			throws ServiceException;

	/**
	 * Provides getting number of news selected via help of <code>SearchFilter</code>.
	 * @param filter the instance of <code>SearchFilter</code>
	 * @return number of selected news
	 * @throws ServiceException
	 */
	public Long getNumberOfFilteredNews(SearchFilter filter) throws ServiceException;
	
	public TagTO getTag(Long tagId) throws ServiceException;
	
	public AuthorTO getAuthor(Long authorId) throws ServiceException;

	public void updateAuthor(AuthorTO authorToUpdate) throws ServiceException;

	public void deleteTag(Long entityId) throws ServiceException;
	
	public void deleteAuthor(Long entityId) throws ServiceException;
}