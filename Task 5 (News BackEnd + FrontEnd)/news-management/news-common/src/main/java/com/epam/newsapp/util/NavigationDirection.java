package com.epam.newsapp.util;

/**
 * Contains an enumeration of directions to move from one <code>NewsMessage</code>
 * to another.
 * @author Aleh_Rusak
 */
public enum NavigationDirection {
	NEXT, 
	PREVIOUS;
}
