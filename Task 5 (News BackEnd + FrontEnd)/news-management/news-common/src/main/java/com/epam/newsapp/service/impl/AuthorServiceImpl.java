package com.epam.newsapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsapp.constant.Constants;
import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.persistence.dao.IAuthorDAO;
import com.epam.newsapp.service.IAuthorService;

/**
 * <code>IAuthorService</code> implementation
 * @see <code>IAuthorService</code> for full information
 * @author Aleh_Rusak
 */
@Service
public final class AuthorServiceImpl implements IAuthorService {
	/**
	 * Author DAO
	 */
	@Autowired
	private IAuthorDAO authorDAO;
	
	public Long createAuthor(AuthorTO newAuthor) throws ServiceException, EntityAlreadyExistsException {
		try {
			if(authorDAO.findAuthorByName(newAuthor.getName()) == null) {
				return authorDAO.create(newAuthor);
			} else {
				throw new EntityAlreadyExistsException(Constants.AUTHOR_ALREADY_EXISTS
						+ newAuthor.getName());
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public AuthorTO getAuthor(Long authorId) throws ServiceException {
		try {
			return authorDAO.read(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void bindAuthorWithNews(Long authorId, Long newsId) throws ServiceException {
		try {
			authorDAO.bindAuthorWithNews(authorId, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public AuthorTO getNewsAuthor(Long newsId) throws ServiceException {
		try {
			return authorDAO.getNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public AuthorTO findAuthorByName(String authorName) throws ServiceException {
		try {
			return authorDAO.findAuthorByName(authorName);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void unbindAuthorOnNews(Long authorId, Long newsId) throws ServiceException {
		try {
			authorDAO.unbindAuthorOnNews(authorId, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void deleteAuthors(Long[] authorIdArray) throws ServiceException {
		try {
			authorDAO.multipleDeletion(authorIdArray);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<AuthorTO> getAllAuthors() throws ServiceException {
		try {
			return authorDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateAuthor(AuthorTO authorToUpdate) throws ServiceException {
		try {
			authorDAO.update(authorToUpdate);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteAuthor(Long entityId) throws ServiceException {
		try {
			authorDAO.delete(entityId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
