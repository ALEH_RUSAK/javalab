package com.epam.newsapp.util;

import java.io.Serializable;

/**
 * The implementation of pagination. This class is responsible for creating and 
 * storing information about paging.
 * 
 * @author Aleh_Rusak
 */
public class Pager implements Serializable {

	private static final long serialVersionUID = 1360286596412807773L;

	private final int itemsPerPage = 3;
	
	private Long numberOfItems = 0L;
	
	private int currentPage;
	
	private int numberOfPages;
	
	public int getNumberOfPages(Long numberOfItems) {
		if(!this.numberOfItems.equals(numberOfItems)) {
			numberOfPages = (int) Math.ceil(numberOfItems.intValue() * 1.0 / itemsPerPage);
	    	this.numberOfItems = numberOfItems;
		} 
		return numberOfPages;
	}
	
	/**
	 * @return the numberOfItems
	 */
	public Long getNumberOfItems() {
		return numberOfItems;
	}

	/**
	 * @return the numberOfPages
	 */
	public int getNumberOfPages() {
		return numberOfPages;
	}

	/**
	 * @param currentPage the currentPage to set
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * @param numberOfItems the numberOfItems to set
	 */
	public void setNumberOfItems(Long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}
	
	/**
	 * @return the itemsPerPage
	 */
	public int getItemsPerPage() {
		return itemsPerPage;
	}

	/**
	 * @return calculated "from" position
	 */
	public int getFrom() {
		return currentPage * itemsPerPage - 2;
	}

	/**
	 * @return the currentPage
	 */
	public int getCurrentPage() {
		return currentPage;
	}
}
