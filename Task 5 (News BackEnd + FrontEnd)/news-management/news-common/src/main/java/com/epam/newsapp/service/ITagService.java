package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.TagTO;

/**
 * Service interface for tag actions.
 * @author Aleh_Rusak
 */
public interface ITagService {
	
	/**
	 * Provides creating tag.
	 * @param newTag a tag to be created
	 * @return the id of created tag
	 * @throws ServiceException
	 * @throws EntityAlreadyExistsException 
	 */
	public Long createTag(TagTO newTag) throws ServiceException, EntityAlreadyExistsException;
	
	/**
	 * Provides service layer encapsulation for getting tag by it's id
	 * @param tagId the id of the tag
	 * @return <code>TagTO</code> instance
	 * @throws ServiceException
	 */
	public TagTO getTag(Long tagId) throws ServiceException;
	
	/**
	 * Provides getting all existing tags.
	 * @return the list of tags
	 * @throws ServiceException
	 */
	public List<TagTO> getAllTags() throws ServiceException;
	
	/**
	 * Provides deleting tags.
	 * @param the array of tag's id's
	 * @throws ServiceException
	 */
	public void deleteTags(Long[] tagIdArray) throws ServiceException;
	
	/**
	 * Provides getting list of news tags.
	 * @param newsId the id of the news
	 * @return list of tags connected with news
	 * @throws ServiceException
	 */
	public List<TagTO> getNewsTags(Long newsId) throws ServiceException;
	
	/**
	 * Provides binding tags on news message.
	 * @param newsId the id of the news
	 * @param tagIdList the list of tag id's
	 * @throws ServiceException
	 */
	public void bindTagsWithNews(List<Long> tagIdList, Long newsId) throws ServiceException;
	
	/**
	 * Provides unbinding tags on news.
	 * @param newsId the id of the news
	 * @param tagIdList the list of tag id's
	 * @throws ServiceException
	 */
	public void unbindTagsOnNews(List<Long> tagIdList, Long newsId) throws ServiceException;
	
	/**
	 * Provides updating tag.
	 * @param tagToUpdate tag to be updated
	 * @throws ServiceException
	 */
	public void updateTag(TagTO tagToUpdate) throws ServiceException;

	/**
	 * Provides deleting single tag.
	 * @param tagId the id of the tag to be deleted
	 * @throws ServiceException
	 */
	public void deleteTag(Long tagId) throws ServiceException;
}
