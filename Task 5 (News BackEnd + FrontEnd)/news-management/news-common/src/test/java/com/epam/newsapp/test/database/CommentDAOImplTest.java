/*package com.epam.newsapp.test.database;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.unitils.reflectionassert.ReflectionComparatorMode;

import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.persistence.dao.ICommentDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

*//**
 * Provides <code>CommentDAOImpl</code> test methods execution
 * @author Aleh_Rusak
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ApplicationContext.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("CommentDAOTestDataSet.xml")
public class CommentDAOImplTest {
	*//**
	 * Injected Comment DAO
	 *//*
	@Autowired
	private ICommentDAO commentDAO;

	@Test 
	public void testRead() throws Exception {
		Long commentId = 1L;
		CommentTO actualComment = commentDAO.read(commentId);
		CommentTO expectedComment = new CommentTO();
		expectedComment.setEntityId(commentId);
		expectedComment.setCommentText("Comment1");
		expectedComment.setCreationDate(Timestamp.valueOf("2015-03-02 07:53:34.101000000"));
		expectedComment.setNewsId(1L);
		assertReflectionEquals(expectedComment, actualComment);
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,
		value = "expected/CommentExpectedDataSet.xml")
	public void testDelete() throws Exception {
		commentDAO.multipleDeletion(new Long[] { 1L });
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testUpdate() throws Exception {
		Long commentId = 2L;
		CommentTO commentToUpdate = new CommentTO();
		commentToUpdate.setEntityId(commentId);
		commentToUpdate.setCommentText("NewComment");
		commentToUpdate.setCreationDate(Timestamp.valueOf("2015-03-03 00:00:00.0"));
		commentToUpdate.setNewsId(2L);
		commentId = commentDAO.update(commentToUpdate);
		CommentTO updatedComment = commentDAO.read(commentId);
		assertReflectionEquals(commentToUpdate, updatedComment);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testCreate() throws Exception {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setEntityId(0L);
		expectedComment.setCommentText("Comment4");
		expectedComment.setCreationDate(Timestamp.valueOf("2015-03-03 00:00:00.0"));
		expectedComment.setNewsId(2L);
		Long commendId = commentDAO.create(expectedComment);
		expectedComment.setEntityId(commendId);
		CommentTO actualComment = commentDAO.read(commendId);
		assertReflectionEquals(expectedComment, actualComment);
	}
	
	@Test
	public void testGetNewsComments() throws Exception {
		List<CommentTO> expectedCommentList = new ArrayList<CommentTO>();
		expectedCommentList.add(new CommentTO(
				1L,
				"Comment1",
				Timestamp.valueOf("2015-03-02 07:53:34.101000000"),
				1L
		));
		expectedCommentList.add(new CommentTO(
				3L,
				"Comment3",
				Timestamp.valueOf("2015-03-02 07:53:34.101000000"),
				1L
		));
		List<CommentTO> actualCommentList = commentDAO.getNewsComments(1L);
		assertReflectionEquals(expectedCommentList, actualCommentList, 
				ReflectionComparatorMode.LENIENT_ORDER);
	}
}*/