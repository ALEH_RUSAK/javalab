package com.epam.newsapp.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.persistence.dao.IAuthorDAO;
import com.epam.newsapp.service.IAuthorService;

/**
 * Mockito <code>AuthorServiceImpl</code> methods testing
 * @author Aleh_Rusak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ApplicationContext.xml"})
public class AuthorServiceTest {
	/**
	 * Author DAO mock
	 */
	@Mock
	private IAuthorDAO authorDAO;
	/**
	 * Author service with injected mocks
	 */
	@InjectMocks
	@Autowired
	private IAuthorService authorService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testCreateAuthor() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setName("name");
		Long expectedId = 1L;
		when(authorDAO.findAuthorByName(author.getName())).thenReturn(null);
		when(authorDAO.create(author)).thenReturn(expectedId);
		Long actualId = authorService.createAuthor(author);
		verify(authorDAO, times(1)).create(author);
		verify(authorDAO, times(1)).findAuthorByName(author.getName());
		assertEquals(expectedId, actualId);
	}
	
	@Test
	public void testGetAuthor() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO();
		Long authorId = 1L;
		when(authorDAO.read(authorId)).thenReturn(expectedAuthor);
		AuthorTO actualAuthor = authorService.getAuthor(authorId);
		verify(authorDAO, times(1)).read(authorId);
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testBindAuthorWithNews() throws Exception {
		Long authorId = 1L;
		Long newsId = 1L;
		authorService.bindAuthorWithNews(authorId, newsId);
		verify(authorDAO, times(1)).bindAuthorWithNews(authorId, newsId);
	}
	
	@Test
	public void testGetNewsAuthor() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(1L, "name");
		Long newsId = 1L;
		when(authorDAO.getNewsAuthor(newsId)).thenReturn(expectedAuthor);
		AuthorTO actualAuthor = authorService.getNewsAuthor(newsId);
		verify(authorDAO, times(1)).getNewsAuthor(newsId);
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testFindAuthorByName() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(1L, "name");
		String authorName = "name";
		when(authorDAO.findAuthorByName(authorName)).thenReturn(expectedAuthor);
		AuthorTO actualAuthor = authorService.findAuthorByName(authorName);
		verify(authorDAO, times(1)).findAuthorByName(authorName);
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testDeleteAuthors() throws Exception {
		Long[] idArray = new Long[] { 1L };
		authorService.deleteAuthors(idArray);
		verify(authorDAO, times(1)).multipleDeletion(idArray);
	}
}
