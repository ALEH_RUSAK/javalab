package com.epam.newsapp.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.persistence.dao.ITagDAO;
import com.epam.newsapp.service.ITagService;

/**
 * Mockito <code>TagServiceImpl</code> methods testing
 * @author Aleh_Rusak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ApplicationContext.xml"})
public class TagServiceTest {
	/**
	 * Tag DAO mock
	 */
	@Mock
	private ITagDAO tagDAO;
	/**
	 * Tag service with injected mocks
	 */
	@InjectMocks
	@Autowired
	private ITagService tagService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testCreateTag() throws Exception {
		TagTO newTag = new TagTO();
		newTag.setTagName("name");
		Long expectedId = 1L;
		when(tagDAO.findTagByName(newTag.getTagName())).thenReturn(null);
		when(tagDAO.create(newTag)).thenReturn(expectedId);
		Long actualId = tagService.createTag(newTag);
		verify(tagDAO, times(1)).create(newTag);
		verify(tagDAO, times(1)).findTagByName(newTag.getTagName());
		assertEquals(expectedId, actualId);
	}
	
	@Test
	public void testDeleteTags() throws Exception {
		Long[] idList = new Long[] { 1L, 2L };
		tagService.deleteTags(idList);
		verify(tagDAO, times(1)).multipleDeletion(idList);
	}
	
	@Test
	public void testGetAllTags() throws Exception {
		List<TagTO> expectedTagList = new ArrayList<TagTO>();
		when(tagDAO.getAll()).thenReturn(expectedTagList);
		List<TagTO> actualTagList = tagService.getAllTags();
		verify(tagDAO, times(1)).getAll();
		assertEquals(expectedTagList, actualTagList);
	}
}
