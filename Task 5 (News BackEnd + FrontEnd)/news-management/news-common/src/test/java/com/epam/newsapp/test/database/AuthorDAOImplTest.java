/*package com.epam.newsapp.test.database;

import static org.junit.Assert.assertNull;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.persistence.dao.IAuthorDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

*//**
 * Provides <code>AuthorDAOImpl</code> test methods execution
 * @author Aleh_Rusak
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ApplicationContext.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("AuthorDAOTestDataSet.xml")
public class AuthorDAOImplTest {
	*//**
	 * Author DAO
	 *//*
	@Autowired
	private IAuthorDAO authorDAO;
	
	@Test 
	public void testRead() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(1L, "One");
		AuthorTO actualAuthor = authorDAO.read(1L);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,
		value = "expected/AuthorExpectedDataSet.xml")
	public void testDelete() throws Exception {
		authorDAO.delete(1L);
	}
	
	@Test
	@Transactional
	public void testUpdate() throws Exception {
		Long authorId = 2L;
		AuthorTO authorToUpdate = new AuthorTO(authorId, "Four");
		authorId = authorDAO.update(authorToUpdate);
		AuthorTO updatedAuthor = authorDAO.read(authorId);
		assertReflectionEquals(authorToUpdate, updatedAuthor);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testCreate() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setName("Three");
		Long newAuthorId = authorDAO.create(expectedAuthor);
		expectedAuthor.setEntityId(newAuthorId);
		AuthorTO actualAuthor = authorDAO.read(newAuthorId);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testGetNewsAuthor() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(1L, "One");
		AuthorTO actualAuthor = authorDAO.getNewsAuthor(1L);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testFindAuthorByName() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(1L, "One");
		AuthorTO actualAuthor = authorDAO.findAuthorByName("One");
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testBindAuthorWithNews() throws Exception {
		Long id = 2L;
		authorDAO.bindAuthorWithNews(id, id);
		AuthorTO expectedAuthor = new AuthorTO(id, "Two");
		AuthorTO actualAuthor = authorDAO.getNewsAuthor(id);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testUnbindAuthorOnNews() throws Exception {
		Long id = 2L;
		authorDAO.unbindAuthorOnNews(id, id);
		assertNull(authorDAO.getNewsAuthor(id));
	}
}
*/