/*package com.epam.newsapp.test.database;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.unitils.reflectionassert.ReflectionComparatorMode;

import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.persistence.dao.ITagDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

*//**
 * Provides <code>TagDAOImpl</code> tests execution
 * @author Aleh_Rusak
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ApplicationContext.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("TagDAOTestDataSet.xml")
public class TagDAOImplTest {
	*//**
	 * Injected Tag DAO
	 *//*
	@Autowired
	private ITagDAO tagDAO;
	
	@Test 
	public void testRead() throws Exception {
		TagTO expectedTag = new TagTO(1L, "Tag1");
		TagTO actualTag = tagDAO.read(1L);
		assertReflectionEquals(expectedTag, actualTag);
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT, 
		value = "expected/TagExpectedDataSet.xml")
	public void testDelete() throws Exception {
		tagDAO.multipleDeletion(new Long[] { 1L });
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testUpdate() throws Exception {
		Long tagId = new Long(2);
		TagTO tagToUpdate = new TagTO(tagId, "NewTag");
		tagDAO.update(tagToUpdate);
		TagTO updatedTag = tagDAO.read(tagId);
		assertReflectionEquals(tagToUpdate, updatedTag);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testCreate() throws Exception {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagName("Three");
		Long newTagId = tagDAO.create(expectedTag);
		expectedTag.setEntityId(newTagId);
		TagTO actualTag = tagDAO.read(newTagId);
		assertReflectionEquals(expectedTag, actualTag);
	}
	
	@Test
	public void testGetNewsTagsList() throws Exception {
		List<TagTO> expectedTagList = new ArrayList<TagTO>();
		expectedTagList.add(new TagTO(1L, "Tag1"));
		List<TagTO> actualTagList = tagDAO.getNewsTags(1L);
		assertReflectionEquals(expectedTagList, actualTagList, ReflectionComparatorMode.LENIENT_ORDER);
	}
	
	@Test
	public void testFindTagByName() throws Exception {
		TagTO expectedTag = new TagTO(1L, "Tag1");
		TagTO actualTag = tagDAO.findTagByName("Tag1");
		assertReflectionEquals(expectedTag, actualTag);
	}
	
	@Test
	public void testBindTagsWithNews() throws Exception {
		List<TagTO> expectedTags = new ArrayList<TagTO>();
		expectedTags.add(new TagTO(1L, "Tag1"));
		expectedTags.add(new TagTO(2L, "Tag2"));
		Long newsId = 1L;
		tagDAO.bindTagsWithNews(new Long[] { 2L }, newsId);
		List<TagTO> actualTags = tagDAO.getNewsTags(newsId);
		assertReflectionEquals(expectedTags, actualTags, ReflectionComparatorMode.LENIENT_ORDER);
	}
}
*/