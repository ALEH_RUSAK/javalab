/*package com.epam.newsapp.test.database;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.unitils.reflectionassert.ReflectionComparatorMode;

import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.INewsDAO;
import com.epam.newsapp.persistence.dao.ITagDAO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

*//**
 * Provides <code>NewsDAOImpl</code> tests execution
 * @author Aleh_Rusak
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:ApplicationContext.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("NewsDAOTestDataSet.xml")
public class NewsDAOImplTest {
	*//**
	 * Injected News DAO
	 *//*
	@Autowired
	private INewsDAO newsDAO;
	*//**
	 * Injected Tag DAO
	 *//*
	@Autowired
	private ITagDAO tagDAO;
	
	@Test 
	public void testRead() throws Exception {
		List<NewsTO> newsList = newsDAO.getAll();
		Assert.assertEquals(3, newsList.size());
	}

	@Test
	@ExpectedDatabase(assertionMode = DatabaseAssertionMode.NON_STRICT,
		value = "expected/NewsExpectedDataSet.xml")
	public void testDelete() throws Exception {
		newsDAO.multipleDeletion(new Long[] { 1L });
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testUpdate() throws Exception {
		NewsTO newsToUpdate = new NewsTO(
				3L,
				"T4",
				"B4",
				"C4",
				Date.valueOf("2015-03-03"),
				Timestamp.valueOf("2015-03-03 00:00:00.0")
			);
		Long id = newsDAO.update(newsToUpdate);
		NewsTO updatedNews = newsDAO.read(id);
		assertReflectionEquals(newsToUpdate, updatedNews);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void testCreate() throws Exception {
		NewsTO expectedNews = new NewsTO(
				0L,
				"T4",
				"B4",
				"C4",
				Date.valueOf("2015-03-03"),
				Timestamp.valueOf("2015-03-03 00:00:00.0")
			);
		Long newAuthorId = newsDAO.create(expectedNews);
		expectedNews.setEntityId(newAuthorId);
		NewsTO actualNews = newsDAO.read(newAuthorId);
		assertReflectionEquals(expectedNews, actualNews);
	}
	
	@Test
	public void testFindNewsByAuthor() throws Exception {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(new NewsTO(
				1L,
				"T1",
				"B1",
				"C1",
				Date.valueOf("2015-03-02"),
				Timestamp.valueOf("2015-03-03 00:00:00.0")
			));
		List<NewsTO> actualNewsList = newsDAO.findNewsByAuthor(1L);
		assertReflectionEquals(expectedNewsList, actualNewsList, ReflectionComparatorMode.LENIENT_ORDER);
	}
	
	@Test
	public void testFindNewsByTags() throws Exception {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(new NewsTO(
				1L,
				"T1",
				"B1",
				"C1",
				Date.valueOf("2015-03-02"),
				Timestamp.valueOf("2015-03-02 00:00:00.0")
			));
		List<NewsTO> actualNewsList = newsDAO.findNewsByTags(new Long[] {2L, 1L});
		assertReflectionEquals(expectedNewsList, actualNewsList, ReflectionComparatorMode.LENIENT_ORDER);
	}
	
	@Test
	public void testFindNewsByAuthorAndTags() throws Exception {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(new NewsTO(
				1L,
				"T1",
				"B1",
				"C1",
				Date.valueOf("2015-03-02"),
				Timestamp.valueOf("2015-03-02 00:00:00.0")
			));
		List<NewsTO> actualNewsList = newsDAO.findNewsByAuthorAndTags(1L, new Long[] { 2L, 1L });
		assertReflectionEquals(expectedNewsList, actualNewsList, ReflectionComparatorMode.LENIENT_ORDER);
	}
}*/