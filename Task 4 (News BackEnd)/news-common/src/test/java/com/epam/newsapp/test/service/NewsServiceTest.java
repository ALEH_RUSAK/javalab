package com.epam.newsapp.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.INewsDAO;
import com.epam.newsapp.service.INewsService;

/**
 * Mockito <code>NewsServiceImpl</code> methods testing
 * @author Aleh_Rusak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestApplicationContext.xml"})
public class NewsServiceTest {
	/**
	 * News DAO mock
	 */
	@Mock
	private INewsDAO newsDAO;
	/**
	 * News service with injected mocks
	 */
	@InjectMocks
	@Autowired
	private INewsService newsService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreateNews() throws Exception {
		NewsTO newsMessage = new NewsTO();
		Long expectedId = 1L;
		when(newsDAO.create(newsMessage)).thenReturn(expectedId);
		Long actualId = newsService.createNews(newsMessage);
		verify(newsDAO, times(1)).create(newsMessage);
		assertEquals(expectedId, actualId);
    }
	
	@Test
	public void testGetAllNews() throws Exception {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		when(newsDAO.getAll()).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.getAllNews();
		verify(newsDAO, times(1)).getAll();
		assertEquals(expectedNewsList, actualNewsList);
	}

	@Test
	public void testGetNews() throws Exception {
		NewsTO expectedNews = new NewsTO();
		Long newsId = 1L;
		when(newsDAO.read(newsId)).thenReturn(expectedNews);
		NewsTO actualNews = newsService.getNews(newsId);
		verify(newsDAO, times(1)).read(newsId);
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void testDeleteNews() throws Exception {
		List<Long> idList = new ArrayList<Long>();
		idList.add(1L);
		idList.add(2L);
		newsService.deleteNews(idList);
		verify(newsDAO, times(1)).multipleDeletion(idList);
	}
	
	@Test
	public void testFindNewsByAuthor() throws Exception {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		Long authorId = 1L;
		when(newsDAO.findNewsByAuthor(authorId)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.getNewsByAuthor(authorId);
		verify(newsDAO, times(1)).findNewsByAuthor(authorId);
		assertEquals(expectedNewsList, actualNewsList);
	}
	
	@Test
	public void testFindNewsByTags() throws Exception {
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		List<Long> tagIdList = new ArrayList<Long>();
		when(newsDAO.findNewsByTags(tagIdList)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.getNewsByTags(tagIdList);
		verify(newsDAO, times(1)).findNewsByTags(tagIdList);
		assertEquals(expectedNewsList, actualNewsList);
	}
}
