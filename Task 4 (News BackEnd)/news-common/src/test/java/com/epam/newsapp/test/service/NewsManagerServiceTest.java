/*package com.epam.newsapp.test.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.service.IAuthorService;
import com.epam.newsapp.service.ICommentService;
import com.epam.newsapp.service.INewsManagerService;
import com.epam.newsapp.service.INewsService;
import com.epam.newsapp.service.ITagService;
import com.epam.newsapp.valueobject.NewsMessage;

*//**
 * Mockito <code>NewsManagerServiceImpl</code> methods testing
 * @author Aleh_Rusak
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {"classpath:TestApplicationContext.xml"})
public class NewsManagerServiceTest {
	*//**
	 * News service mock
	 *//*
	@Mock
	private INewsService newsService;
	*//**
	 * Author service mock
	 *//*
	@Mock
	private IAuthorService authorService;
	*//**
	 * Comment service mock
	 *//*
	@Mock
	private ICommentService commentService;
	*//**
	 * Tag service mock
	 *//*
	@Mock
	private ITagService tagService;
	*//**
	 * <code>NewsManagerService</code> variable with injected mocks
	 *//*
	@InjectMocks
	@Autowired
	private INewsManagerService newsManagerService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	private NewsMessage createPartialNewsMessage(Long newsId)
			throws ServiceException {
		NewsMessage newsMessage = new NewsMessage();
		newsMessage.setNewsMessage(newsService.getNews(newsId));
		newsMessage.setNewsAuthor(authorService.getNewsAuthor(newsId));
		newsMessage.setNewsTags(tagService.getNewsTags(newsId));
		return newsMessage;
	}
		
	private NewsMessage createFullNewsMessage(Long newsId)
			throws ServiceException {
		NewsMessage newsMessage = new NewsMessage();
		newsMessage.setNewsMessage(newsService.getNews(newsId));
		newsMessage.setNewsAuthor(authorService.getNewsAuthor(newsId));
		newsMessage.setNewsTags(tagService.getNewsTags(newsId));
		newsMessage.setNewsComments(commentService.getNewsComments(newsId));
		return newsMessage;
	}
	
	private List<NewsMessage> createNewsMessageList(List<NewsTO> newsList) throws ServiceException {
		List<NewsMessage> newsMessageList = new ArrayList<NewsMessage>();
		for(NewsTO news: newsList) {
			Long newsId = news.getEntityId();
			newsMessageList.add(this.createFullNewsMessage(newsId));
		}
		return newsMessageList;
	}
	
	@Test
	public void testAddNews() throws Exception {
		NewsTO news = new NewsTO(
				0L,
				"T4",
				"B4",
				"C4",
				Date.valueOf("2015-03-03"),
				new Timestamp(Date.valueOf("2015-03-03").getTime())
			);
		AuthorTO author = new AuthorTO(1L, "name");
		List<TagTO> newsTags = new ArrayList<TagTO>();
		newsTags.add(new TagTO(1L, "name"));
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(1L);
		//NewsMessage expectedNewsMessage = this.createPartialNewsMessage(news.getEntityId());
		Long authorId = author.getEntityId();
		Long newsId = 1L;
		when(newsService.createNews(news)).thenReturn(newsId);
		NewsMessage actualNewsMessage = newsManagerService.saveNews(news, tagIdList, authorId);
		verify(newsService).createNews(Matchers.any(NewsTO.class));
		verify(tagService, times(1)).bindTagsWithNews(tagIdList, newsId);
		verify(authorService, times(1)).bindAuthorWithNews(authorId, newsId);
		//assertEquals(expectedNewsMessage, actualNewsMessage);
	}
	
	@Test
	public void testUpdateNews() throws Exception {
		NewsTO news = new NewsTO(
				1L,
				"T4",
				"B4",
				"C4",
				Date.valueOf("2015-03-03"),
				new Timestamp(Date.valueOf("2015-03-03").getTime())
			);
		AuthorTO author = new AuthorTO(1L, "name");
		List<TagTO> newsTags = new ArrayList<TagTO>();
		newsTags.add(new TagTO(1L, "name"));
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(1L);
		Long newsId = news.getEntityId();
		Long authorId = author.getEntityId();
		//NewsMessage expectedNewsMessage = this.createPartialNewsMessage(newsId);
		List<TagTO> currentNewsTags = new ArrayList<TagTO>();
		currentNewsTags.add(new TagTO(2L, "name2"));
		when(newsService.updateNews(news)).thenReturn(newsId);
		when(tagService.getNewsTags(newsId)).thenReturn(currentNewsTags);
		NewsMessage actualNewsMessage = newsManagerService.saveNews(news, tagIdList, authorId);
		verify(newsService, times(1)).updateNews(news);
		verify(tagService, times(1)).bindTagsWithNews(tagIdList, newsId);
		verify(authorService, times(1)).bindAuthorWithNews(authorId, newsId);
		//assertEquals(expectedNewsMessage, actualNewsMessage);
	}
	
	@Test
	public void testViewNewsList() throws Exception {
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		NewsTO news = new NewsTO(
				1L,
				"T4",
				"B4",
				"C4",
				Date.valueOf("2015-03-03"),
				new Timestamp(Date.valueOf("2015-03-03").getTime())
			);
		newsList.add(news);
		Long newsId = news.getEntityId();
		AuthorTO author = new AuthorTO();
		List<TagTO> newsTags = new ArrayList<TagTO>();
		newsTags.add(new TagTO());
		List<CommentTO> newsComments = new ArrayList<CommentTO>();
		newsComments.add(new CommentTO());
		//List<NewsMessage> expectedNewsList = this.createNewsMessageList(newsList);
		when(newsService.getAllNews()).thenReturn(newsList);
		when(newsService.getNews(newsId)).thenReturn(news);
		when(authorService.getNewsAuthor(newsId)).thenReturn(author);
		when(commentService.getNewsComments(newsId)).thenReturn(newsComments);
		List<NewsMessage> actualNewsList = newsManagerService.viewNewsList();
		verify(newsService, times(1)).getAllNews();
		verify(newsService, times(1)).getNews(news.getEntityId());
	} 
}
*/