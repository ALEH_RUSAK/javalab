package com.epam.newsapp.test.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.ICommentDAO;
import com.epam.newsapp.service.ICommentService;

/**
 * Mockito <code>CommentServiceImpl</code> methods testing
 * @author Aleh_Rusak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestApplicationContext.xml"})
public class CommentServiceTest {
	/**
	 * Comment DAO mock
	 */
	@Mock
	private ICommentDAO commentDAO;
	/**
	 * Comment service with injected mocks
	 */
	@InjectMocks
	@Autowired
	private ICommentService commentService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testDeleteComments() throws Exception {
		NewsTO newsMessage = new NewsTO();
		newsMessage.setEntityId(0L);
		List<Long> idList = new ArrayList<Long>();
		idList.add(1L);
		idList.add(2L);
		commentService.deleteComments(idList);
		verify(commentDAO, times(1)).multipleDeletion(idList);
	}
	
	@Test
	public void testCreateComment() throws Exception {
		CommentTO newComment = new CommentTO();
		Long expectedId = 1L;
		when(commentDAO.create(newComment)).thenReturn(expectedId);
		Long actualId = commentService.createComment(newComment);
		verify(commentDAO, times(1)).create(newComment);
		assertEquals(expectedId, actualId);
	}
	
	@Test
	public void testGetComment() throws Exception {
		CommentTO expectedComment = new CommentTO();
		Long commentId = 1L;
		when(commentDAO.read(commentId)).thenReturn(expectedComment);
		CommentTO actualComment = commentService.getComment(commentId);
		verify(commentDAO, times(1)).read(commentId);
		assertEquals(expectedComment, actualComment);
	}
	
	@Test
	public void testGetNewsComments() throws Exception {
		List<CommentTO> expectedCommentList = new ArrayList<CommentTO>();
		Long newsId = 1L;
		when(commentDAO.getNewsComments(newsId)).thenReturn(expectedCommentList);
		List<CommentTO> actualCommentList = commentService.getNewsComments(newsId);
		verify(commentDAO, times(1)).getNewsComments(newsId);
		assertEquals(expectedCommentList, actualCommentList);
	}
}
