package com.epam.newsapp.resources;

/**
 * Constant experessions encapsulator
 * @author Aleh_Rusak
 */
public final class Constants {
	
	private Constants() {}
	
	/* String expressions for logging */
	public static final String CANT_FIND_AUTHOR = "Author can not be found.";
	public static final String CANT_FIND_TAG = "Tag can not be found.";
	public static final String CANT_CREATE_INSTANCE = "Instance was not created. Can't create instance of a class:";
	public static final String CANT_READ_INSTANCE = "Can't read instance.";
	public static final String AUTHOR_ALREADY_EXISTS = "Author already exists. Author's name is: ";
	public static final String TAG_ALREADY_EXISTS = "Tag already exists. Tag's title is: ";
}
