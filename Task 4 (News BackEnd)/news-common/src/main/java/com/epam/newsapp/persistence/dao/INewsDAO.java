package com.epam.newsapp.persistence.dao;

import java.util.List;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.NewsTO;

/**
 * <code>IGenericDAO<TO, PK></code> extension for operations with NEWS
 * @author Aleh_Rusak
 */
public interface INewsDAO extends IGenericDAO<NewsTO, Long> {
	
	/**
	 * Getting list of <code>NewsTO</code> instances by authorId
	 * @param authorId the id of <code>AuthorTO</code>
	 * @return list of <code>NewsTO</code> instances
	 * @throws DAOException
	 */
	public List<NewsTO> findNewsByAuthor(Long authorId) throws DAOException;
	
	/**
	 * Getting list of <code>NewsTO</code> instances by tags
	 * @param tagList the list of tags
	 * @return list of <code>NewsTO</code> instances
	 * @throws DAOException
	 */
	public List<NewsTO> findNewsByTags(List<Long> tagIdList) throws DAOException;
}
