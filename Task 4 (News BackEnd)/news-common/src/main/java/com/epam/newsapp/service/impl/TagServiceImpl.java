package com.epam.newsapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.persistence.dao.ITagDAO;
import com.epam.newsapp.resources.Constants;
import com.epam.newsapp.service.ITagService;

/**
 * <code>ITagService</code> implementation
 * @see <code>ITagService</code> for full information
 * @author Aleh_Rusak
 */
public final class TagServiceImpl implements ITagService {
	/**
	 * Injected Tag DAO
	 */
	@Autowired
	private ITagDAO tagDAO;
	
	@Override
	public Long createTag(TagTO newTag) throws ServiceException, EntityAlreadyExistsException {
		try {
			if(tagDAO.findTagByName(newTag.getTagName()) == null) {
				return tagDAO.create(newTag);
			} else {
				throw new EntityAlreadyExistsException(Constants.TAG_ALREADY_EXISTS
						+ newTag.getTagName());
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<TagTO> getAllTags() throws ServiceException {
		try {
			return tagDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteTags(List<Long> tagIdList) throws ServiceException {
		try {
			tagDAO.multipleDeletion(tagIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<TagTO> getNewsTags(Long newsId) throws ServiceException {
		try {
			return tagDAO.getNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void bindTagsWithNews(List<Long> tagIdList, Long newsId)
			throws ServiceException {
		try {
			tagDAO.bindTagsWithNews(tagIdList, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public TagTO getTag(Long tagId) throws ServiceException {
		try {
			return tagDAO.read(tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void unbindTagsOnNews(List<Long> tagIdList, Long newsId)
			throws ServiceException {
		try {
			tagDAO.unbindTagsOnNews(tagIdList, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
