package com.epam.newsapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.persistence.dao.IAuthorDAO;
import com.epam.newsapp.resources.Constants;
import com.epam.newsapp.service.IAuthorService;

/**
 * <code>IAuthorService</code> implementation
 * @see <code>IAuthorService</code> for full information
 * @author Aleh_Rusak
 */
public final class AuthorServiceImpl implements IAuthorService {
	/**
	 * Author DAO
	 */
	@Autowired
	private IAuthorDAO authorDAO;
	
	@Override
	public Long createAuthor(AuthorTO newAuthor) throws ServiceException, EntityAlreadyExistsException {
		try {
			if(authorDAO.findAuthorByName(newAuthor.getName()) == null) {
				return authorDAO.create(newAuthor);
			} else {
				throw new EntityAlreadyExistsException(Constants.AUTHOR_ALREADY_EXISTS
						+ newAuthor.getName());
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public AuthorTO getAuthor(Long authorId) throws ServiceException {
		try {
			return authorDAO.read(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void bindAuthorWithNews(Long authorId, Long newsId) throws ServiceException {
		try {
			authorDAO.bindAuthorWithNews(authorId, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public AuthorTO getNewsAuthor(Long newsId) throws ServiceException {
		try {
			return authorDAO.getNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public AuthorTO findAuthorByName(String authorName) throws ServiceException {
		try {
			return authorDAO.findAuthorByName(authorName);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void unbindAuthorOnNews(Long authorId, Long newsId) throws ServiceException {
		try {
			authorDAO.unbindAuthorOnNews(authorId, newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteAuthors(List<Long> authorIdList) throws ServiceException {
		try {
			authorDAO.multipleDeletion(authorIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
