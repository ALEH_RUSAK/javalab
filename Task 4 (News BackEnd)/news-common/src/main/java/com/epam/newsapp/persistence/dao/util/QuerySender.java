package com.epam.newsapp.persistence.dao.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.AbstractTO;
import com.epam.newsapp.resources.Constants;

/**
 * Provides implementation templates of performing
 * database operations
 * @author Aleh_Rusak
 */
public final class QuerySender<TO extends AbstractTO> {
	/**
	 * Injected data source
	 * @see <code>BasicDataSource</code> description
	 */
	@Autowired
	private DataSource dataSource;
	/**
	 * Injected database utility class
	 * @see <code>DatabaseUtil</code>
	 */
	@Autowired
	private DatabaseUtil<TO> dbUtil;
	
	/**
	 * Provides implementation of creation template
	 * @param newTO transfer object to be inserted into database
	 * @param sqlQuery SQL command
	 * @param idColumn the name of column which new value should be generated 
	 * @return the id of inserted object
	 * @throws DAOException
	 */
	public Long performCreate(TO transferObject, final String sqlQuery, 
			final String idColumn, IStatementMapper<TO> statementMapper) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long idToGenerate = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sqlQuery, new String[] {idColumn});
			statementMapper.mapStatement(transferObject, preparedStatement);
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()) {
				idToGenerate = resultSet.getLong(1);
			} else {
				throw new DAOException(Constants.CANT_CREATE_INSTANCE);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
		return idToGenerate;
	}
	
	/**
	 * Provides implementation of reading template
	 * @param instanceId the id of instance
	 * @param sqlQuery SQL command
	 * @return transfer object got by it's instanceId
	 * @throws DAOException
	 */
	public TO performRead(Long instanceId, final String sqlQuery, IRowMapper<TO> rowMapper)
			throws DAOException {
		Connection connection = null;
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    TO transferObject = null;
	    try {
	    	connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sqlQuery);
			preparedStatement.setLong(1, instanceId);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				transferObject = rowMapper.mapRow(resultSet);
			} else {
			    System.out.println("ffffff");
				throw new DAOException(Constants.CANT_READ_INSTANCE);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	    return transferObject;
	}
	
	/**
	 * Provides implementation of updating template
	 * @param transferObject object to be updated
	 * @param sqlQuery SQL command
	 * @return the id of updated object
	 * @throws DAOException
	 */
	public Long performUpdate(TO transferObject, final String sqlQuery, 
			IStatementMapper<TO> statementMapper) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sqlQuery);
			int mappedRows = statementMapper.mapStatement(transferObject, preparedStatement);
			preparedStatement.setLong(mappedRows + 1, transferObject.getEntityId());
			preparedStatement.executeUpdate();
			return transferObject.getEntityId();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}
	
	/**
	 * Provides implementation of delete template
	 * @param id the id of item that should be deleted
	 * @param sqlQuery SQL command
	 * @throws DAOException
	 */
	public void performDelete(Long id, final String sqlQuery) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sqlQuery);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}
	
	/**
	 * Provides implementation of getting all instances template
	 * @param sqlQuery
	 * @return
	 * @throws DAOException
	 */
	public List<TO> performGetAll(final String sqlQuery, IRowMapper<TO> rowMapper)
			throws DAOException {
		Connection connection = null;
		Statement statement = null;
	    ResultSet resultSet = null;
	    try {
	    	connection = dataSource.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlQuery);
			return dbUtil.createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, statement, resultSet);
		}
	}
	
	/**
	 * Provides implementation of multiple deletion template
	 * @param idList the list of id's of items that should be deleted
	 * @param sqlQuery the SQL command
	 * @throws DAOException
	 */
	public void performMultipleDeletion(List<Long> idList, final String sqlQuery) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sqlQuery);
			dbUtil.executeBatch(idList, preparedStatement);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}
	
	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @return the dbUtil
	 */
	public DatabaseUtil<TO> getDbUtil() {
		return dbUtil;
	}

	/**
	 * @param dbUtil the dbUtil to set
	 */
	public void setDbUtil(DatabaseUtil<TO> dbUtil) {
		this.dbUtil = dbUtil;
	}
}
