package com.epam.newsapp.persistence.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.persistence.dao.ITagDAO;
import com.epam.newsapp.persistence.dao.util.DatabaseResourceManager;
import com.epam.newsapp.persistence.dao.util.IRowMapper;
import com.epam.newsapp.persistence.dao.util.IStatementMapper;
import com.epam.newsapp.resources.Constants;

/**
 * DAO implementation to work with Tag table
 * @see <code>AbstractDAO</code> description and declaration
 * @author Aleh_Rusak
 */
@Repository
public class TagDAOImpl extends AbstractDAO<TagTO> implements ITagDAO {
	/**
	 * Tag ID
	 */
	private static final String TAG_ID = "tag_id";
	/**
	 * News_Tag ID
	 */
	private static final String NEWS_TAG_ID = "news_tag_id";
	/**
	 * SQL commands
	 */
	private static final String SQL_INSERT_TAG = "INSERT INTO Tag (tag_id, tag_name) VALUES (TAG_SEQ.nextval, ?)";
	private static final String SQL_BIND_TAGS_WITH_NEWS = "INSERT INTO News_Tag (news_tag_id, news_id, tag_id) "
			+ "VALUES (NEWS_TAG_SEQ.nextval,?,?)";
	private static final String SQL_SELECT_TAG_BY_ID = "SELECT tag_id, tag_name FROM Tag WHERE tag_id = ?";
	private static final String SQL_SELECT_TAG_BY_NAME = "SELECT tag_id, tag_name FROM Tag WHERE tag_name = ?";
	private static final String SQL_UPDATE_TAG = "UPDATE Tag SET tag_name = ? WHERE tag_id = ?";
	private static final String SQL_DELETE_TAG = "DELETE FROM Tag WHERE tag_id = ?";
	private static final String SQL_GET_NEWS_TAGS = "SELECT Tag.tag_id, Tag.tag_name FROM Tag INNER JOIN"
			+ " News_Tag ON News_Tag.tag_id = Tag.tag_id "
			+ "WHERE News_Tag.news_id = ?";
	private static final String SQL_GET_ALL_TAGS = "SELECT Tag.tag_id, Tag.tag_name FROM Tag";
	private static final String SQL_UNBIND_TAGS_ON_NEWS = "DELETE FROM News_Tag WHERE news_id = ? AND tag_id = ?";
	
	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>TagTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class TagRowMapper implements IRowMapper<TagTO> {
		@Override
		public TagTO mapRow(ResultSet resultSet) throws SQLException {
			TagTO tag = new TagTO();
			tag.setEntityId(resultSet.getLong(1));
			tag.setTagName(resultSet.getString(2));
			return tag;
		}
	}
	
	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>TagTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class TagStatementMapper implements IStatementMapper<TagTO> {
		@Override
		public int mapStatement(TagTO tag, PreparedStatement preparedStatement) throws SQLException {
			preparedStatement.setString(1, tag.getTagName());
			return 1;
		}	
	}
	
	/**
	 * Default constructor creates objects of mappers
	 */
	public TagDAOImpl() {
		rowMapper = new TagRowMapper();
		statementMapper = new TagStatementMapper();
	}

	@Override
	public Long create(TagTO newTag) throws DAOException {
		return querySender.performCreate(newTag, SQL_INSERT_TAG, TAG_ID, statementMapper);
	}

	@Override
	public TagTO read(Long tagId) throws DAOException {
		return querySender.performRead(tagId, SQL_SELECT_TAG_BY_ID, rowMapper);
	}

	@Override
	public Long update(TagTO tag) throws DAOException {
		return querySender.performUpdate(tag, SQL_UPDATE_TAG, statementMapper);
	}

	@Override
	public void delete(Long tagId) throws DAOException {
		querySender.performDelete(tagId, SQL_DELETE_TAG);
	}

	@Override
	public void multipleDeletion(List<Long> tagIdList) throws DAOException {
		querySender.performMultipleDeletion(tagIdList, SQL_DELETE_TAG);
	}

	@Override
	public List<TagTO> getAll() throws DAOException {
		return querySender.performGetAll(SQL_GET_ALL_TAGS, rowMapper);
	}
	
	@Override
	public void bindTagsWithNews(List<Long> tagIdList, Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			final int batchSize = 2000;
			int batchIndex = 0;
			preparedStatement = connection.prepareStatement(SQL_BIND_TAGS_WITH_NEWS, new String[] {NEWS_TAG_ID});
			for(Long tagId: tagIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
				if(batchIndex > batchSize) {
					preparedStatement.executeBatch();
					batchIndex = 0;
				}
				batchIndex++;
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}

	@Override
	public List<TagTO> getNewsTags(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    try {
	    	connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_GET_NEWS_TAGS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			return querySender.getDbUtil().createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}

	@Override
	public TagTO findTagByName(String tagName) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    TagTO tag = null;
	    try {
	    	connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_BY_NAME);
			preparedStatement.setString(1, tagName);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				tag = rowMapper.mapRow(resultSet);
			} else {
				throw new DAOException(Constants.CANT_FIND_TAG);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	    return tag;
	}

	@Override
	public void unbindTagsOnNews(List<Long> tagIdList, Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UNBIND_TAGS_ON_NEWS);
			final int batchSize = 2000;
			int batchIndex = 0;
			for(Long tagId: tagIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
				if(batchIndex > batchSize) {
					preparedStatement.executeBatch();
					batchIndex = 0;
				}
				batchIndex++;
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}
}
