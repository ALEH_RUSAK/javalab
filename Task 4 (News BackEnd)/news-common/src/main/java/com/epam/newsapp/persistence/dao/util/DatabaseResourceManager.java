package com.epam.newsapp.persistence.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.epam.newsapp.exceptions.DAOException;

/**
 * Provides management for JDBC resources.
 * @author Aleh_Rusak
 */
public final class DatabaseResourceManager {

	/**
	 * Method closes all opened JDBC resources 
	 * and releases connection into the pool
	 * @throws DAOException
	 */
	public static void closeJdbcResources(Connection connection, Statement preparedStatement,
			ResultSet resultSet) throws DAOException {
		try {
			try {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} finally {
					if (preparedStatement != null) {
						preparedStatement.close();
					}
				}			
			} finally {
				if (connection != null) {
					connection.close();
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
}
