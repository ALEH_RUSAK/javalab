package com.epam.newsapp.persistence.dao.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.epam.newsapp.model.AbstractTO;

/**
 * Provides encapsulation for some database methods and operations
 * @author Aleh_Rusak
 * @param <TO> an <code>AbstractTO</code> extension
 */
public final class DatabaseUtil<TO extends AbstractTO> {
	
	/**
	 * Provides getting list of TO instances
	 * @param resultSet the <code>ResultSet</code> instance
	 * @return list of created instances of <code>TO</code>
	 * @throws SQLException
	 */
	public List<TO> createInstancesList(ResultSet resultSet, IRowMapper<TO> rowMapper) 
			throws SQLException {
		List<TO> instanceList = new ArrayList<TO>();
		while(resultSet.next()) {
			TO toInstance = rowMapper.mapRow(resultSet);
			instanceList.add(toInstance);
		}
		return instanceList;
	}
	
	/**
	 * Provides safe batch execution using partial execution
	 * @param idList the list if items id's
	 * @param preparedStatement <code>PreparedStatement</code> instance
	 * @throws SQLException
	 */
	public void executeBatch(List<Long> idList, PreparedStatement preparedStatement)
			throws SQLException {
		final int batchSize = 2000;
		int batchIndex = 0;
		for(Long id: idList) {
			preparedStatement.setLong(1, id);
			preparedStatement.addBatch();
			if(batchIndex >= batchSize) {
				preparedStatement.executeBatch();
				batchIndex = 0;
			}
			batchIndex++;
		}
		preparedStatement.executeBatch();
	}
}
