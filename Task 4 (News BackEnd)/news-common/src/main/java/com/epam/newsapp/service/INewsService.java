package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.NewsTO;

/**
 * Service interface for news actions
 * @author Aleh_Rusak
 */
public interface INewsService {
	
	/**
	 * Provides creating news
	 * @param newsMessage <code>NewsTO</code> instance to be saved
	 * @return form to be proceeded
	 * @throws ServiceException
	 */
	public Long createNews(NewsTO newsMessage) throws ServiceException;

	/**
	 * Provides updating news
	 * @param newsMessage news to be updated
	 * @return the id of updated news
	 * @throws ServiceException
	 */
	public Long updateNews(NewsTO newsMessage) throws ServiceException;
	
	/**
	 * Provides deleting news
	 * @param newsIdList the list of news ids
	 * @throws ServiceException
	 */
	public void deleteNews(List<Long> newsIdList) throws ServiceException;
	 
	/**
	 * Getting news of concrete author
	 * @param authorId author's id
	 * @return the list of news wrote by author with authorId id
	 * @throws ServiceException
	 */
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Getting all news by selected tags
	 * @param tags ids list
	 * @return the list of news connected with tags
	 * @throws ServiceException
	 */
	public List<NewsTO> getNewsByTags(List<Long> tagIdList) throws ServiceException;
	
	/**
	 * Getting news message by it's id
	 * @param newsId the id of the news
	 * @return <code>NewsTO</code> instance
	 * @throws ServiceException
	 */
	public NewsTO getNews(Long newsId) throws ServiceException;

	/**
	 * Provides viewing the list of news messages 
	 * @return the list of news
	 * @throws ServiceException
	 */
	public List<NewsTO> getAllNews() throws ServiceException;
}
