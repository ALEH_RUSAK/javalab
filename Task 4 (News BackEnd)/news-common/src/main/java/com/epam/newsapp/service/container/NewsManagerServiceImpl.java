package com.epam.newsapp.service.container;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.*;
import com.epam.newsapp.service.*;
import com.epam.newsapp.valueobject.NewsMessage;

/**
 * This class encapsulates service's execution
 * Declares all services as private injections
 * and provides interface to execute methods of injected services.
 * @see <code>INewsManagerService</code> declaration
 * @author Aleh_Rusak
 */
public class NewsManagerServiceImpl implements INewsManagerService {
	/**
	 * News service
	 */
	@Autowired
	private INewsService newsService;
	/**
	 * Author service
	 */
	@Autowired
	private IAuthorService authorService;
	/**
	 * Tag service
	 */
	@Autowired
	private ITagService tagService;
	/**
	 * Comment service
	 */
	@Autowired
	private ICommentService commentService;
	/**
	 * News Value Object 
	 */
	private NewsMessage valuableNewsMessage;
	
	/**
	 * Provides creation of <code>NewsMessage</code> instance
	 * by collecting different news features in one Value Object
	 * @param newsId the id of the news
	 * @return <code>NewsMessage</code> instance
	 * @throws ServiceException 
	 */
	private NewsMessage createPartialNewsMessage(Long newsId) throws ServiceException {
		valuableNewsMessage.setNewsMessage(newsService.getNews(newsId));
		valuableNewsMessage.setNewsAuthor(authorService.getNewsAuthor(newsId));
		valuableNewsMessage.setNewsTags(tagService.getNewsTags(newsId));
		return valuableNewsMessage;
	}
		
	/**
	 * Provides creation of <code>NewsMessage</code> instance
	 * by collecting different news features in one Value Object
	 * @param newsId the id of the news
	 * @return <code>NewsMessage</code> instance
	 * @throws ServiceException 
	 */
	private NewsMessage createFullNewsMessage(Long newsId) throws ServiceException {
		System.out.println(newsId);
		valuableNewsMessage.setNewsMessage(newsService.getNews(newsId));
		valuableNewsMessage.setNewsAuthor(authorService.getNewsAuthor(newsId));
		valuableNewsMessage.setNewsTags(tagService.getNewsTags(newsId));
		valuableNewsMessage.setNewsComments(commentService.getNewsComments(newsId));
		return valuableNewsMessage;
	}
	
	/**
	 * Provides creation the list of <code>NewsMessage</code>s
	 * @param newsList the list of news
	 * @return the list of <code>NewsMessage</code>s
	 * @throws ServiceException
	 */
	private List<NewsMessage> createNewsMessageList(List<NewsTO> newsList)
			throws ServiceException {
		List<NewsMessage> newsMessageList = new ArrayList<NewsMessage>();
		for(NewsTO news: newsList) {
			Long newsId = news.getEntityId();
			newsMessageList.add(this.createFullNewsMessage(newsId));
		}
		return newsMessageList;
	}
	
	/**
	 * This constuctor implemented for creation <code>NewsMessage</code> instance
	 */
	public NewsManagerServiceImpl() {
		valuableNewsMessage = new NewsMessage();
	}
	
	/* ================ Manager's public API =================== */
	
	@Override
	@Transactional
	public NewsMessage saveNews(NewsTO news, List<Long> tagIdList,
			Long authorId) throws ServiceException {
		Long newsId = news.getEntityId();
		System.out.println(newsService);
		if(newsId == 0L) {
			newsId = newsService.createNews(news);
			tagService.bindTagsWithNews(tagIdList, newsId);
			authorService.bindAuthorWithNews(authorId, newsId);
		} else {
			newsId = newsService.updateNews(news);
			/* Updating news tags */
			List<TagTO> currentNewsTags = tagService.getNewsTags(newsId);
			List<TagTO> newTags = new ArrayList<TagTO>();
			for(Long tagId: tagIdList) {
				newTags.add(tagService.getTag(tagId));
			}
 			List<Long> currentNewsTagIds = new ArrayList<Long>();
			for(TagTO tag: currentNewsTags) {
				currentNewsTagIds.add(tag.getEntityId());
			}
			if(!currentNewsTags.equals(newTags)) {
				tagService.unbindTagsOnNews(currentNewsTagIds, newsId);
				tagService.bindTagsWithNews(tagIdList, newsId);
			}
			/* Updating news author */
			AuthorTO currentNewsAuthor = authorService.getNewsAuthor(newsId);
			if(!currentNewsAuthor.equals(authorService.getAuthor(authorId))) {
				authorService.unbindAuthorOnNews(currentNewsAuthor.getEntityId(), newsId);
				authorService.bindAuthorWithNews(authorId, newsId);
			}
		}
		return this.createPartialNewsMessage(newsId);
	}

	@Override
	@Transactional
	public List<NewsMessage> viewNewsList() throws ServiceException {
		return this.createNewsMessageList(newsService.getAllNews());
	}
	
	@Override
	@Transactional
	public NewsMessage viewSingleNews(NewsTO news) throws ServiceException {
		Long newsId = news.getEntityId();
		return this.createFullNewsMessage(newsId);
	}
	
	@Override
	@Transactional
	public List<NewsMessage> getNewsByAuthor(String authorName) 
			throws ServiceException {
		AuthorTO newsAuthor = authorService.findAuthorByName(authorName);
		Long authorId = newsAuthor.getEntityId();
		return this.createNewsMessageList(newsService.getNewsByAuthor(authorId));
	}
	
	@Override
	@Transactional
	public List<NewsMessage> getNewsByTags(List<Long> tagIdList) 
			throws ServiceException  {
		return this.createNewsMessageList(newsService.getNewsByTags(tagIdList));
	}

	@Override
	public void deleteNews(List<Long> newsIdList) throws ServiceException {
		newsService.deleteNews(newsIdList);
	}
	
	@Override
	@Transactional
	public CommentTO addComment(CommentTO newComment) throws ServiceException {
		Long commentId = commentService.createComment(newComment);
		return commentService.getComment(commentId);
	}
	
	@Override
	@Transactional
	public List<CommentTO> deleteComments(List<Long> commentIdList, Long newsId) 
			throws ServiceException {
		commentService.deleteComments(commentIdList);
		return commentService.getNewsComments(newsId);
	}
	
	@Override
	public List<TagTO> addTag(TagTO newTag) throws ServiceException {
		try {
			tagService.createTag(newTag);
		} catch (EntityAlreadyExistsException e) {
			//logger.error(e);
		}
		return tagService.getAllTags();
	}
	
	@Override
	public List<TagTO> deleteTags(List<Long> tagIdList) throws ServiceException {
		tagService.deleteTags(tagIdList);
		return tagService.getAllTags();
	}
	
	@Override
	public AuthorTO addNewsAuthor(AuthorTO author) throws ServiceException {
		Long authorId = null;
		try {
			authorId = authorService.createAuthor(author);
		} catch (EntityAlreadyExistsException e) {
			//logger.error(e);
		}
		return authorService.getAuthor(authorId);
	}
	
	/**
	 * @return the valuableNewsMessage
	 */
	public NewsMessage getValuableNewsMessage() {
		return valuableNewsMessage;
	}

	/**
	 * @param valuableNewsMessage the valuableNewsMessage to set
	 */
	public void setValuableNewsMessage(NewsMessage valuableNewsMessage) {
		this.valuableNewsMessage = valuableNewsMessage;
	}
}
