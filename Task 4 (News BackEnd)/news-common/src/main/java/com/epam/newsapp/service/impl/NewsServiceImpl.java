package com.epam.newsapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.INewsDAO;
import com.epam.newsapp.service.INewsService;

/**
 * <code>INewsService</code> implementation
 * @see <code>INewsService</code> for full information
 * @author Aleh_Rusak
 */
public final class NewsServiceImpl implements INewsService {
	/**
	 * Injected News DAO
	 */
	@Autowired
	private INewsDAO newsDAO;
	
	@Override
	public Long createNews(NewsTO news) throws ServiceException {
		try {
			return newsDAO.create(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getAllNews() throws ServiceException {
		try {
			return newsDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long updateNews(NewsTO news) throws ServiceException {
		try {
			return newsDAO.update(news);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNews(List<Long> newsIdList) throws ServiceException {
		try {
			newsDAO.multipleDeletion(newsIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException {
		try {
			return newsDAO.findNewsByAuthor(authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override 
	public List<NewsTO> getNewsByTags(List<Long> tagIdList) throws ServiceException {
		try {
			return newsDAO.findNewsByTags(tagIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public NewsTO getNews(Long newsId) throws ServiceException {
		try {
			return newsDAO.read(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
