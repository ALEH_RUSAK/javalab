package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.EntityAlreadyExistsException;
import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.TagTO;

/**
 * Service interface for tag actions
 * @author Aleh_Rusak
 */
public interface ITagService {
	
	/**
	 * Provides creating tags
	 * @param the list of tags was created
	 * @return the id of created tag
	 * @throws ServiceException
	 * @throws EntityAlreadyExistsException 
	 */
	public Long createTag(TagTO newTag) throws ServiceException, EntityAlreadyExistsException;
	
	/**
	 * Provides getting tag by it's id
	 * @param tagId the id of the tag
	 * @return <code>TagTO</code> instance
	 * @throws ServiceException
	 */
	public TagTO getTag(Long tagId) throws ServiceException;
	
	/**
	 * Provides getting all tags
	 * @return the list of tags
	 * @throws ServiceException
	 */
	public List<TagTO> getAllTags() throws ServiceException;
	
	/**
	 * Provides deleting tags
	 * @param the list of tag's ids of those tags, that we want to delete
	 * @throws ServiceException
	 */
	public void deleteTags(List<Long> tagIdList) throws ServiceException;
	
	/**
	 * Provides getting list of news tags
	 * @param newsId the id of the news
	 * @return list of tags connected with news
	 * @throws ServiceException
	 */
	public List<TagTO> getNewsTags(Long newsId) throws ServiceException;
	
	/**
	 * Setting tags to some news with newsId as it's id
	 * @param newsId the id of the news
	 * @param tagIdList the list of tag ids
	 * @throws ServiceException
	 */
	public void bindTagsWithNews(List<Long> tagIdList, Long newsId) throws ServiceException;
	
	/**
	 * Provides unbinding tags on news
	 * @param newsId the id of the news
	 * @param tagIdList the list of tag ids
	 * @throws ServiceException
	 */
	public void unbindTagsOnNews(List<Long> tagIdList, Long newsId) throws ServiceException;
}
