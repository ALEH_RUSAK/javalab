package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.model.TagTO;
import com.epam.newsapp.valueobject.NewsMessage;

/**
 * Encapsulates service's execution.
 * Provides methods for it.
 * @author Aleh_Rusak
 */
public interface INewsManagerService {
	
	/**
	 * Provides saving <code>NewsMessage</code> with authors and tags
	 * @param news the actual news
	 * @param tagIdList the list of tags
	 * @param authorId news author
	 * @return the valuable new message
	 * @throws ServiceException
	 */
	public NewsMessage saveNews(NewsTO news, List<Long> tagIdList, Long authorId) throws ServiceException;
	
	/**
	 * Provides viewing the list of news
	 * @return the list of valuable news messages
	 * @throws ServiceException
	 */
	public List<NewsMessage> viewNewsList() throws ServiceException;
	
	/**
	 * Provides viewing concrete news 
	 * @param news the actual news
	 * @return the valuable new message
	 * @throws ServiceException
	 */
	public NewsMessage viewSingleNews(NewsTO news) throws ServiceException;
	
	/**
	 * Provides viewing news wrote by concrete author
	 * @param authorName authors name
	 * @return the list of valuable news messages wrote by author with name param
	 * @throws ServiceException
	 */
	public List<NewsMessage> getNewsByAuthor(String authorName) throws ServiceException; 
	
	/**
	 * Provides viewing news connected with chosen tags
	 * @param tagIdList the list of tag's ids
	 * @return the list of valuable news messages under chosen tags
	 * @throws ServiceException
	 */
	public List<NewsMessage> getNewsByTags(List<Long> tagIdList) throws ServiceException;

	/**
	 * Provides deleting selected news
	 * @param newsIdList the list of news ids
	 * @throws ServiceException
	 */
	public void deleteNews(List<Long> newsIdList) throws ServiceException;

	/**
	 * Provides adding comment to news
	 * @param newComment comment to be added
	 * @return added comment
	 * @throws ServiceException
	 */
	public CommentTO addComment(CommentTO newComment) throws ServiceException;
	
	/**
	 * Provides deleting selected comments
	 * @param commentIdList the list of comment's ids
	 * @param newsId the id of the news
	 * @return updated comments list
	 * @throws ServiceException
	 */
	public List<CommentTO> deleteComments(List<Long> commentIdList, Long newsId) throws ServiceException;
	
	/**
	 * Provides adding new tag
	 * @param newTag tag to be added
	 * @return the list of tags to view them
	 * @throws ServiceException
	 */
	public List<TagTO> addTag(TagTO newTag) throws ServiceException;
	
	/**
	 * Provides deleting selected tags
	 * @param tagIdList the list of tags to be deleted
	 * @return the list of tags to view them
	 * @throws ServiceException
	 */
	public List<TagTO> deleteTags(List<Long> tagIdList) throws ServiceException;
	
	/**
	 * Provides adding new blogger
	 * @param author the author to be added
	 * @return added author to view
	 * @throws ServiceException
	 */
	public AuthorTO addNewsAuthor(AuthorTO author) throws ServiceException;
}
