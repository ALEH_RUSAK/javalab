package com.epam.newsapp.exceptions;

/**
 * Thrown when DAO layer's method execution is incorrect
 * @author Aleh_Rusak
 */
public class DAOException extends BaseException {
	/**
	 * The ID version
	 */
	private static final long serialVersionUID = 7878548672303746039L;

	public DAOException() {}
	
	public DAOException(Throwable nestedException) {
		this.title = DAOException.class.toString();
		this.nestedException = nestedException;
		this.exceptionMessage = nestedException.getMessage();
	}
	
	public DAOException(String exceptionMessage) {
		this.title = DAOException.class.toString();
		this.exceptionMessage = exceptionMessage;
	}
}
