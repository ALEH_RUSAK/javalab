package com.epam.newsapp.exceptions;


/**
 * Provides common context of exceptions in this application
 * @author Aleh_Rusak
 */
public abstract class BaseException extends Exception {
	/**
	 * The ID version
	 */
	private static final long serialVersionUID = -2056348909667290866L;
	/**
	 * Exception's unique title
	 */
	protected String title;
	/**
	 * Nested exception
	 */
	protected Throwable nestedException;
	/**
	 * Exception's unique message
	 */
	protected String exceptionMessage;
	
	/**
	 * 
	 */
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("Exception occured: ");
		strBuilder.append(title);
		if(nestedException != null) {
			strBuilder.append(", nested exception is: ");
			strBuilder.append(nestedException.toString());
		}
		if(exceptionMessage != null) {
			strBuilder.append(", message: ");
			strBuilder.append(exceptionMessage);
		}
		return strBuilder.toString();
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the nestedException
	 */
	public Throwable getNestedException() {
		return nestedException;
	}
}
