package com.epam.newsapp.valueobject;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.model.CommentTO;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.model.TagTO;

/**
 * News value object.
 * Contains various news attributes such as news, comments, author, etc.
 * @author Aleh_Rusak
 */
public final class NewsMessage {
	/**
	 * News message
	 */
	private NewsTO newsMessage;
	/**
	 * News author
	 */
	private AuthorTO newsAuthor;
	/**
	 * News tags
	 */
	private List<TagTO> newsTags;
	/**
	 * News comments
	 */
	private List<CommentTO> newsComments;
	
	public NewsMessage() {
		newsTags = new ArrayList<TagTO>();
		newsComments = new ArrayList<CommentTO>();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((newsAuthor == null) ? 0 : newsAuthor.hashCode());
		result = prime * result
				+ ((newsComments == null) ? 0 : newsComments.hashCode());
		result = prime * result
				+ ((newsMessage == null) ? 0 : newsMessage.hashCode());
		result = prime * result
				+ ((newsTags == null) ? 0 : newsTags.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsMessage other = (NewsMessage) obj;
		if (newsAuthor == null) {
			if (other.newsAuthor != null)
				return false;
		} else if (!newsAuthor.equals(other.newsAuthor))
			return false;
		if (newsComments == null) {
			if (other.newsComments != null)
				return false;
		} else if (!newsComments.equals(other.newsComments))
			return false;
		if (newsMessage == null) {
			if (other.newsMessage != null)
				return false;
		} else if (!newsMessage.equals(other.newsMessage))
			return false;
		if (newsTags == null) {
			if (other.newsTags != null)
				return false;
		} else if (!newsTags.equals(other.newsTags))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NewsMessage [newsMessage=");
		builder.append(newsMessage);
		builder.append(", newsAuthor=");
		builder.append(newsAuthor);
		builder.append(", newsTags=");
		builder.append(newsTags);
		builder.append(", newsComments=");
		builder.append(newsComments);
		builder.append("]");
		return builder.toString();
	}
	
	/**
	 * Cleares data of Value Object
	 */
	public void clear() {
		newsMessage = null;
		newsAuthor = null;
		newsTags.clear();
		newsComments.clear();
	}
	
	/**
	 * @return the newsMessage
	 */
	public NewsTO getNewsMessage() {
		return newsMessage;
	}
	
	/**
	 * @param newsMessage the newsMessage to set
	 */
	public void setNewsMessage(NewsTO newsMessage) {
		this.newsMessage = newsMessage;
	}
	
	/**
	 * @return the newsAuthor
	 */
	public AuthorTO getNewsAuthor() {
		return newsAuthor;
	}
	
	/**
	 * @return the newsTags
	 */
	public List<TagTO> getNewsTags() {
		return newsTags;
	}
	
	/**
	 * @param newsTags the newsTags to set
	 */
	public void setNewsTags(List<TagTO> newsTags) {
		this.newsTags = newsTags;
	}
	
	/**
	 * @return the newsComments
	 */
	public List<CommentTO> getNewsComments() {
		return newsComments;
	}
	
	/**
	 * @param newsComments the newsComments to set
	 */
	public void setNewsComments(List<CommentTO> newsComments) {
		this.newsComments = newsComments;
	}
	
	/**
	 * @param newsAuthor the newsAuthor to set
	 */
	public void setNewsAuthor(AuthorTO newsAuthor) {
		this.newsAuthor = newsAuthor;
	}
}
