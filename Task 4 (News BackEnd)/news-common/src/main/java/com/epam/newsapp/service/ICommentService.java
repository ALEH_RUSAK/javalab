package com.epam.newsapp.service;

import java.util.List;

import com.epam.newsapp.exceptions.ServiceException;
import com.epam.newsapp.model.CommentTO;

/**
 * Service interface for comment actions
 * @author Aleh_Rusak
 */
public interface ICommentService {

	/**
	 * Provides deleting comments
	 * @param newsForm incoming form
	 * @return form to be proceeded
	 * @throws ServiceException
	 */
	public void deleteComments(List<Long> commentIdList) throws ServiceException;

	/**
	 * Provides getting comments of some news
	 * @param newsId the id of the news which comments we want to get
	 * @return the list of comments
	 * @throws ServiceException
	 */
	public List<CommentTO> getNewsComments(Long newsId) throws ServiceException;
	
	/**
	 * Provides creating new comment
	 * @param comment <code>CommentTO</code> instance
	 * @return the id of new comment
	 * @throws ServiceException
	 */
	public Long createComment(CommentTO newComment) throws ServiceException;
	
	/**
	 * Provides getting single comment
	 * @param commentId the id of comment
	 * @return <code>CommentTO</code> instance
	 * @throws ServiceException
	 */
	public CommentTO getComment(Long commentId) throws ServiceException;
}
