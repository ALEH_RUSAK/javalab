package com.epam.newsapp.persistence.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.NewsTO;
import com.epam.newsapp.persistence.dao.INewsDAO;
import com.epam.newsapp.persistence.dao.util.DatabaseResourceManager;
import com.epam.newsapp.persistence.dao.util.IRowMapper;
import com.epam.newsapp.persistence.dao.util.IStatementMapper;

/**
 * The DAO implementation to work with news
 * @see <code>AbstractDAO</code> description and declaration
 * @author Aleh_Rusak
 */
public class NewsDAOImpl extends AbstractDAO<NewsTO> implements INewsDAO {
	/**
	 * PK identifier
	 */
	private static final String NEWS_ID = "news_id";
	/**
	 * SQL commands
	 */
	private static final String SQL_INSERT_NEWS = "INSERT INTO News (news_id, title, brief, content, creation_date, modification_date) VALUES (NEWS_SEQ.nextval,?,?,?,?,?)";
	private static final String SQL_SELECT_NEWS_BY_ID = "SELECT news_id, title, brief, content, creation_date, modification_date FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_SELECT_ALL_NEWS = "SELECT n.news_id, n.title, n.brief, n.content, n.creation_date, "
			+ "n.modification_date, c.CNT FROM News n "
			+ "LEFT JOIN (SELECT news_id, COUNT(*) CNT FROM Comments GROUP BY news_id) c "
            + "ON n.news_id = c.news_id "
            + "ORDER BY CNT DESC NULLS LAST";
	private static final String SQL_UPDATE_NEWS = "UPDATE News SET title = ?, brief = ?, content = ?, "
			+ "creation_date = ?, modification_date = ? WHERE news_id = ?";
	private static final String SQL_DELETE_NEWS = "DELETE FROM News WHERE news_id = ?";
	private static final String SQL_FIND_NEWS_BY_AUTHOR =
			"SELECT News.news_id, News.title, News.brief, News.content, "
			+ "News.modification_date, News.creation_date FROM News "
			+ "INNER JOIN News_Authors "
			+ "ON News_Authors.news_id = News.news_id "
			+ "WHERE News_Authors.author_id = ?";
	private static final String SQL_FIND_NEWS_BY_TAG = 
			"SELECT DISTINCT News.news_id, News.title, News.brief, News.content, "
			+ "News.modification_date, News.creation_date "
			+ "FROM News INNER JOIN News_Tag ON News_Tag.news_id = News.news_id "
			+ "INNER JOIN Tag ON Tag.tag_id = News_Tag.tag_id WHERE Tag.tag_id = ?";
	
	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>NewsTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class NewsRowMapper implements IRowMapper<NewsTO> {
		@Override
		public NewsTO mapRow(ResultSet resultSet) throws SQLException {
			NewsTO newsMessage = new NewsTO();
			newsMessage.setEntityId(resultSet.getLong(1));
			newsMessage.setTitle(resultSet.getString(2));
			newsMessage.setBrief(resultSet.getString(3));
			newsMessage.setContent(resultSet.getString(4));
			newsMessage.setModificationDate(resultSet.getDate(5));
			newsMessage.setCreationDate(resultSet.getTimestamp(6));
			return newsMessage;
		}
	}
	
	/**
	 * Inner class provides implementation for <code>IRowMapper</code>
	 * for <code>NewsTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class NewsStatementMapper implements IStatementMapper<NewsTO> {
		@Override
		public int mapStatement(NewsTO news, PreparedStatement preparedStatement) throws SQLException {
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getBrief());
			preparedStatement.setString(3, news.getContent());
			preparedStatement.setDate(4, new Date(news.getModificationDate().getTime()));
			preparedStatement.setTimestamp(5, new Timestamp(news.getCreationDate().getTime()));
			return 5;
		}	
	}
	
	/**
	 * Default constructor creates objects of mappers
	 */
	public NewsDAOImpl() {
		rowMapper = new NewsRowMapper();
		statementMapper = new NewsStatementMapper();
	}
	
	@Override
	public Long create(NewsTO newsMessage) throws DAOException {
		return querySender.performCreate(newsMessage, SQL_INSERT_NEWS, NEWS_ID, statementMapper);
	}

	@Override
	public NewsTO read(Long newsId) throws DAOException {
	    return querySender.performRead(newsId, SQL_SELECT_NEWS_BY_ID, rowMapper);
	}
	
	@Override
	public Long update(NewsTO newsMessage) throws DAOException {
		return querySender.performUpdate(newsMessage, SQL_UPDATE_NEWS, statementMapper);
	}

	@Override
	public void delete(Long newsId) throws DAOException {
		querySender.performDelete(newsId, SQL_DELETE_NEWS);
	}
	
	@Override
	public void multipleDeletion(List<Long> newsIdList) throws DAOException {
		querySender.performMultipleDeletion(newsIdList, SQL_DELETE_NEWS);
	}
	
	@Override
	public List<NewsTO> getAll() throws DAOException {
		return querySender.performGetAll(SQL_SELECT_ALL_NEWS, new NewsRowMapper());
	}

	@Override
	public List<NewsTO> findNewsByAuthor(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> newsList = null;
	    try {
	    	connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_FIND_NEWS_BY_AUTHOR);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			newsList = querySender.getDbUtil().createInstancesList(resultSet, rowMapper);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	    return newsList;
	}

	@Override
	public List<NewsTO> findNewsByTags(List<Long> tagIdList) 
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsTO> newsList = new ArrayList<NewsTO>();
	    try {
	    	connection = dataSource.getConnection();
	    	preparedStatement = connection.prepareStatement(SQL_FIND_NEWS_BY_TAG);
			for(Long tagId: tagIdList) {
				preparedStatement.setLong(1, tagId);
				resultSet = preparedStatement.executeQuery();
				if(resultSet.next()) {
					newsList.add(rowMapper.mapRow(resultSet));
				}
				resultSet.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
		return newsList;
	}
}
