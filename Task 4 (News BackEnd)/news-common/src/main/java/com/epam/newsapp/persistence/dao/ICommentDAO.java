package com.epam.newsapp.persistence.dao;

import java.util.List;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.CommentTO;

/**
 * <code>IGenericDAO<TO, PK></code> extension for operations with COMMENTS
 * @author Aleh_Rusak
 */
public interface ICommentDAO extends IGenericDAO<CommentTO, Long> {
	
	/**
	 * Getting list of news comments
	 * @param newsId the id of news
	 * @return the list of news comments
	 * @throws DAOException
	 */
	public List<CommentTO> getNewsComments(Long newsId) throws DAOException;
}
