package com.epam.newsapp.model;

/**
 * Author's transfer object
 * @author Aleh_Rusak
 */
public final class AuthorTO extends AbstractTO {
	/**
	 * ID Version
	 */
	private static final long serialVersionUID = 4494387171796587039L;
	/**
	 * Name of author
	 */
	private String name;

	public AuthorTO() {}
	
	public AuthorTO(Long entityId, String name) {
		super(entityId);
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorTO other = (AuthorTO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
