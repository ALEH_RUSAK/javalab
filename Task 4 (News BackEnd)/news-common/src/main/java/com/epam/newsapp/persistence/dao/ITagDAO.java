package com.epam.newsapp.persistence.dao;

import java.util.List;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.TagTO;

/**
 * <code>IGenericDAO<TO, PK></code> extension for operations with TAGS
 * @author Aleh_Rusak
 */
public interface ITagDAO extends IGenericDAO<TagTO, Long> {
	
	/**
	 * Binding news with it's tags
	 * @param tagsIdList the list of tag's ids
	 * @param newsId the id if the news
	 * @throws DAOException
	 */
	public void bindTagsWithNews(List<Long> tagsIdList, Long newsId) throws DAOException;
	
	/**
	 * Unbinding news on tags
	 * @param tagsIdList the list of tag's ids
	 * @param newsId the id of the news
	 * @throws DAOException
	 */
	public void unbindTagsOnNews(List<Long> tagsIdList, Long newsId) throws DAOException;
	
	/**
	 * Provides getting news tags
	 * @param newsId id of news
	 * @return list of news tags
	 * @throws DAOException 
	 */
	public List<TagTO> getNewsTags(Long newsId) throws DAOException;
	
	/**
	 * Provides getting tag by it's name
	 * @param name tag's name
	 * @return tag by it's name
	 * @throws DAOException
	 */
	public TagTO findTagByName(String name) throws DAOException;
}
