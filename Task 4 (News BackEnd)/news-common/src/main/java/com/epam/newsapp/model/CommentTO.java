package com.epam.newsapp.model;

import java.util.Date;

/**
 * Comment transfer object
 * @author Aleh_Rusak
 */
public final class CommentTO extends AbstractTO {
	/**
	 * ID Version
	 */
	private static final long serialVersionUID = 1922295983020072188L;
	/**
	 * Comment's text
	 */
	private String commentText;
	/**
	 * Comment's creation date
	 */
	private Date creationDate;
	/**
	 * News for which comment was wrote
	 */
	private Long newsId;

	public CommentTO() {
		super();
	}

	public CommentTO(Long entityId, String commentText, Date creationDate, Long newsId) {
		super(entityId);
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentTO other = (CommentTO) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}
	
	public String getCommentText() {
		return commentText;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
}
