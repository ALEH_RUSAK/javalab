package com.epam.newsapp.persistence.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.epam.newsapp.exceptions.DAOException;
import com.epam.newsapp.model.AuthorTO;
import com.epam.newsapp.persistence.dao.IAuthorDAO;
import com.epam.newsapp.persistence.dao.util.DatabaseResourceManager;
import com.epam.newsapp.persistence.dao.util.IRowMapper;
import com.epam.newsapp.persistence.dao.util.IStatementMapper;
import com.epam.newsapp.resources.Constants;

/**
 * The DAO implementation to work with authors
 * @see <code>AbstractDAO</code> description and declaration
 * @author Aleh_Rusak
 */
public class AuthorDAOImpl extends AbstractDAO<AuthorTO> implements IAuthorDAO {
	/**
	 * Author id identifier
	 */
	private static final String AUTHOR_ID = "author_id";
	/**
	 * News-author id identifier
	 */
	private static final String NEWS_AUTHORS_ID = "news_author_id";
	/**
	 * SQL commands
	 */
	private static final String SQL_SELECT_ALL_AUTHORS = "SELECT author_id, name FROM Authors";
	private static final String SQL_SELECT_AUTHOR_BY_NAME = "SELECT author_id, name FROM Authors WHERE name = ?";
	private static final String SQL_INSERT_AUTHOR = "INSERT INTO Authors (author_id, name) VALUES (AUTHOR_SEQ.nextval, ?)";
	private static final String SQL_SELECT_AUTHOR_BY_ID = "SELECT author_id, name FROM Authors WHERE author_id = ?";
	private static final String SQL_UPDATE_AUTHOR = "UPDATE Authors SET name = ? WHERE author_id = ?";
	private static final String SQL_DELETE_AUTHOR = "DELETE FROM Authors WHERE author_id = ?";
	private static final String SQL_GET_NEWS_AUTHOR = 
			"SELECT Authors.author_id, Authors.name FROM Authors "
			+ "INNER JOIN News_Authors "
			+ "ON News_Authors.author_id = Authors.author_id "
			+ "WHERE News_Authors.news_id = ?";
	private static final String SQL_CONNECT_AUTHOR_WITH_NEWS = "INSERT INTO News_Authors (news_author_id, news_id, author_id)"
			+ " VALUES (NEWS_AUTHORS_SEQ.nextval,?,?)";
	private static final String SQL_UNBIND_AUTHOR_ON_NEWS = "DELETE FROM News_Authors WHERE author_id = ? AND news_id = ?";
	
	/**
	 * Inner class provides implementation of <code>IRowMapper</code>
	 * for <code>AuthorTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class AuthorRowMapper implements IRowMapper<AuthorTO> {
		@Override
		public AuthorTO mapRow(ResultSet resultSet) throws SQLException {
			AuthorTO author = new AuthorTO();
			author.setEntityId(resultSet.getLong(1));
			author.setName(resultSet.getString(2));
			return author;
		}
	}
	
	/**
	 * Inner class provides implementation of <code>IRowMapper</code>
	 * for <code>AuthorTO</code> instances
	 * @author Aleh_Rusak
	 */
	private class AuthorStatementMapper implements IStatementMapper<AuthorTO> {
		@Override
		public int mapStatement(AuthorTO transferObject,
				PreparedStatement preparedStatement) throws SQLException {
			preparedStatement.setString(1, transferObject.getName());
			return 1;
		}	
	}
	
	/**
	 * Default constructor creates objects of mappers
	 */
	public AuthorDAOImpl() {
		rowMapper = new AuthorRowMapper();
		statementMapper = new AuthorStatementMapper();
	}
	
	@Override
	public Long create(AuthorTO newAuthor) throws DAOException {
		return querySender.performCreate(newAuthor, SQL_INSERT_AUTHOR, AUTHOR_ID, 
				statementMapper);
	}
	
	@Override
	public AuthorTO read(Long authorId) throws DAOException {
		return querySender.performRead(authorId, SQL_SELECT_AUTHOR_BY_ID, rowMapper);
	}

	@Override
	public Long update(AuthorTO newAuthor) throws DAOException {
		return querySender.performUpdate(newAuthor, SQL_UPDATE_AUTHOR, statementMapper);
	}
	
	@Override
	public List<AuthorTO> getAll() throws DAOException {
		return querySender.performGetAll(SQL_SELECT_ALL_AUTHORS, rowMapper);
	}

	@Override
	public AuthorTO getNewsAuthor(Long newsId) throws DAOException {
		return querySender.performRead(newsId, SQL_GET_NEWS_AUTHOR, rowMapper);
	}
	
	@Override
	public void delete(Long authorId) throws DAOException {
		querySender.performDelete(authorId, SQL_DELETE_AUTHOR);
	}

	@Override
	public void multipleDeletion(List<Long> authorIdList) throws DAOException {
		querySender.performMultipleDeletion(authorIdList, SQL_DELETE_AUTHOR);
	}
	
	@Override
	public void bindAuthorWithNews(Long authorId, Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CONNECT_AUTHOR_WITH_NEWS, new String[] {NEWS_AUTHORS_ID});
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}

	@Override
	public AuthorTO findAuthorByName(String authorName) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    AuthorTO author = null;
	    try {
	    	connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NAME);
			preparedStatement.setString(1, authorName);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				author = rowMapper.mapRow(resultSet);
			} else {
				throw new DAOException(Constants.CANT_FIND_AUTHOR);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	    return author;
	}

	@Override
	public void unbindAuthorOnNews(Long authorId, Long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UNBIND_AUTHOR_ON_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DatabaseResourceManager.closeJdbcResources(connection, preparedStatement, resultSet);
		}
	}
}
