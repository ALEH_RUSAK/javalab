package com.epam.newsapp.model;

import java.io.Serializable;

/**
 * Abstract transfer object is created to be sure that 
 * only <code>AbstractTO</code> objects can pass through DAO
 * @author Aleh_Rusak
 */
public abstract class AbstractTO implements Serializable {
	/**
	 * Version ID
	 */
	private static final long serialVersionUID = -3877356072202187112L;
	/**
	 * Id of entity
	 */
	protected Long entityId;

	public AbstractTO() {
		entityId = 0L;
	}
	
	public AbstractTO(Long entityId) {
		this.entityId = entityId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((entityId == null) ? 0 : entityId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractTO other = (AbstractTO) obj;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		return true;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	public Long getEntityId() {
		return entityId;
	}
}
