package com.epam.newsapp.persistence.dao.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsapp.model.AbstractTO;
import com.epam.newsapp.persistence.dao.util.IRowMapper;
import com.epam.newsapp.persistence.dao.util.IStatementMapper;
import com.epam.newsapp.persistence.dao.util.QuerySender;

/**
 * Abstraction for DAO layer
 * @author Aleh_Rusak
 * @param <TO> an <code>AbstractTO</code> extension
 */
public abstract class AbstractDAO<TO extends AbstractTO> {
	/**
	 * Injected <code>DataSource</code>
	 * @see <code>DataSource</code> description
	 */
	@Autowired
	protected DataSource dataSource;
	/**
	 * Injected <code>QuerySender</code>
	 * @see <code>QuerySender</code> description
	 */
	@Autowired
	protected QuerySender<TO> querySender;
	/**
	 * <code>AuthorRowMapper</code> instance
	 * @see <code>AuthorRowMapper</code> description
	 */
	protected IRowMapper<TO> rowMapper;
	/**
	 * <code>AuthorStatementMapper</code> instance
	 * @see <code>AuthorStatementMapper</code> description
	 */
	protected IStatementMapper<TO> statementMapper;
}
