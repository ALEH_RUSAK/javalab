package com.epam.newsapp.persistence.dao.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.epam.newsapp.model.AbstractTO;

/**
 * Mapping <code>AbstractTO</code> instance on <code>PreparedStatement</code>
 * @author Aleh_Rusak
 * @param <TO> an <code>AbstractTO</code> instance
 */
public interface IStatementMapper<TO extends AbstractTO> {
	
	/**
	 * Initializes statement with values from <code>TO</code> object
	 * @return the count of mapped statement rows (has to be specified by developer who overrides this method)
	 * @throws SQLException 
	 */
	public int mapStatement(TO transferObject, PreparedStatement preparedStatement)
			throws SQLException;
}
