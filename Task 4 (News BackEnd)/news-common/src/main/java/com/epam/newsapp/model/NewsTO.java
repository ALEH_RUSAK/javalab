package com.epam.newsapp.model;

import java.util.Date;

/**
 * News transfer object
 * @author Aleh_Rusak
 */
public final class NewsTO extends AbstractTO {
	/**
	 * ID Version
	 */
	private static final long serialVersionUID = -5903962588987544260L;
	/**
	 * News title
	 */
	private String title;	
	/**
	 * News date of modification
	 */
	private Date modificationDate;
	/**
	 * News date of publishing
	 */
	private Date creationDate;
	/**
	 * News brief
	 */
	private String brief;
	/**
	 * News content
	 */
	private String content;
	
	public NewsTO() {
		modificationDate = new Date(0L);
		creationDate = new Date(0L);
	}
	
	public NewsTO(Long entityId, String title, String brief, 
			String content, Date modificationDate, Date creationDate) {
		super(entityId);
		this.title = title;
		this.modificationDate = modificationDate;
		this.creationDate = creationDate;
		this.brief = brief;
		this.content = content;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((brief == null) ? 0 : brief.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsTO other = (NewsTO) obj;
		if (brief == null) {
			if (other.brief != null)
				return false;
		} else if (!brief.equals(other.brief))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("News { newsId = ");
		strBuilder.append(getEntityId());
		strBuilder.append(", title = ");
		strBuilder.append(title);
		strBuilder.append(", modificationDate = ");
		strBuilder.append(modificationDate.toString());
		strBuilder.append(", creationDate = ");
		strBuilder.append(creationDate.toString());
		strBuilder.append(", brief = ");
		strBuilder.append(brief);
		strBuilder.append(", content = ");
		strBuilder.append(content);
		strBuilder.append(" }");
		return strBuilder.toString();
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getBrief() {
		return brief;
	}
	
	public void setBrief(String brief) {
		this.brief = brief;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
