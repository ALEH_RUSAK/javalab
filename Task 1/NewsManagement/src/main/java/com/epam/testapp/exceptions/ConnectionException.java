package com.epam.testapp.exceptions;

/**
 * Thrown when <code>ConnectionPool</code> method execution is incorrect
 * @author Aleh_Rusak
 */
public class ConnectionException extends BaseException {
	
	private static final long serialVersionUID = 1L;
	
	public ConnectionException() {}

	public ConnectionException(String exceptionMessage) {
		this.code = 2;
		this.title = ConnectionException.class.toString();
		this.exceptionMessage = exceptionMessage;
	}
}
