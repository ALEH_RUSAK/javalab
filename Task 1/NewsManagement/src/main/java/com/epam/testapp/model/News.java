package com.epam.testapp.model;

import java.io.Serializable;
import java.sql.Date;

/**
 * The declaration of News model
 * of the application
 * @author Aleh_Rusak
 */
public final class News implements Serializable {
	
	private static final long serialVersionUID = 1L;
	/**
	 * News Id
	 */
	private int newsId;
	/**
	 * News title
	 */
	private String title;	
	/**
	 * News date of publishing
	 */
	private Date date;
	/**
	 * News brief
	 */
	private String brief;
	/**
	 * News content
	 */
	private String content;
	
	public News() {}
	
	public News(int newsId, String title, Date date, String brief, String content) {
		this.newsId = newsId;
		this.title = title;
		this.date = date;
		this.brief = brief;
		this.content = content;
	}
	
	public int hashCode() {
		int code = 11 * newsId;
		code += ((title != null) ? title.hashCode() : 0);
		code += ((date != null) ? date.hashCode() : 0);
		code += ((brief != null) ? brief.hashCode() : 0);
		code += ((content != null) ? content.hashCode() : 0);
		return (int) code;
	}
	
	public boolean equals(Object anObject) {
		if(this == anObject) {
			return true;
		}
		if(anObject == null) {
			return false;
		}
		if(getClass() == anObject.getClass()) {
			News temp = (News) anObject;
			return newsId == temp.newsId
				&& title.equals(temp.title)
				&& date.equals(temp.date)
				&& brief.equals(temp.brief)
				&& content.equals(temp.content);
		} else {
			return false;
		}
	}
	
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("News { newsId = ");
		strBuilder.append(newsId);
		strBuilder.append(", title = ");
		strBuilder.append(title);
		strBuilder.append(", date = ");
		strBuilder.append(date.toString());
		strBuilder.append(", brief = ");
		strBuilder.append(brief);
		strBuilder.append(", content = ");
		strBuilder.append(content);
		strBuilder.append(" }");
		return strBuilder.toString();
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getBrief() {
		return brief;
	}
	
	public void setBrief(String brief) {
		this.brief = brief;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public int getNewsId() {
		return newsId;
	}

	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}
}
