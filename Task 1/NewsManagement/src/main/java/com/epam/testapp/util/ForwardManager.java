package com.epam.testapp.util;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.epam.testapp.exceptions.ForwardException;

public final class ForwardManager {
	
	private ForwardManager() {}
	
	private static Logger logger = Logger.getLogger(ForwardManager.class);
	
	public static ActionForward getForward(ActionMapping mapping, final String paramForward) throws ForwardException {
		if(paramForward == null || mapping.findForward(paramForward) == null) {
			logger.error(ConstantsManager.PAGE_NOT_FOUND);
			throw new ForwardException(ConstantsManager.PAGE_NOT_FOUND);
		} 
		return mapping.findForward(paramForward);
	}
}
