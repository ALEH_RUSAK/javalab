package com.epam.testapp.util;

/**
 * Keeps and selects proper static final <code>String</code> value
 * @author Aleh_Rusak
 */
public final class ConstantsManager {
	/**
	 * Language constants
	 */
	public static final String ENGLISH_LOCALE = "en";
	public static final String RUSSIAN_LOCALE = "ru";
	/**
	 * Date patterns
	 */
	public static final String EN_DATE_FORMAT = "MM/dd/yyyy";
	public static final String RU_DATE_FORMAT = "dd/MM/yyyy";
	/**
	 * Logger informational constants
	 */
	public static final String ACTION_METHOD_EXECUTED = "method executed.";
	public static final String PAGE_NOT_FOUND = "Page not found.";
}
