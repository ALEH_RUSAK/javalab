package com.epam.testapp.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Filter-class makes request and response 
 * encoding to be UTF-8
 * @author Aleh_Rusak
 */
public class EncodingFilter implements Filter {

	public static final String ENCODING_UTF8 = "UTF-8";
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {
		
		String encoding = request.getCharacterEncoding();
        if (!ENCODING_UTF8.equals(encoding)) {
            request.setCharacterEncoding(ENCODING_UTF8);
        }
        chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {}
	
	@Override
	public void destroy() {}
}
