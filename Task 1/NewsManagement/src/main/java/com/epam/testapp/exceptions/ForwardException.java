package com.epam.testapp.exceptions;

/**
 * Thrown when <code>ActionForward</code> method execution is incorrect
 */
public class ForwardException extends BaseException {

	private static final long serialVersionUID = 1L;
	
	public ForwardException() {}

	public ForwardException(String exceptionMessage) {
		this.code = 4;
		this.title = ActionException.class.toString();
		this.exceptionMessage = exceptionMessage;
	}
}
