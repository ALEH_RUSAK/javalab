package com.epam.testapp.database.dao;

import java.util.ArrayList;
import java.util.List;

import com.epam.testapp.exceptions.DAOException;
import com.epam.testapp.model.News;

/**
 * Provides prototypes declaration of DAO methods
 * For full description of methods @see <code>NewsDAOImpl</code> docs
 * @author Aleh_Rusak
 */
public interface INewsDAO {
	
	public List<News> getNewsList() throws DAOException;
	
	public int saveNews(News newsMessage) throws DAOException;
	
	public void removeNews(ArrayList<Integer> newsIdList) throws DAOException;
	
	public News fetchNewsById(int newsId) throws DAOException;
}
