package com.epam.testapp.logger;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.PropertyConfigurator;

/**
 * Class provides logging support for application
 * @author Aleh_Rusak
 */
public final class LoggerInitializer implements ServletContextListener {
	
	private static final String PROPERTY_PATH = "WEB-INF/log4j.properties";

	@Override
	public void contextInitialized(ServletContextEvent event) {
		String rootPath = event.getServletContext().getRealPath("/");
		System.setProperty("rootPath", rootPath);
	    File propertiesFile = new File(rootPath, PROPERTY_PATH);
	    PropertyConfigurator.configure(propertiesFile.toString());
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {}
}
