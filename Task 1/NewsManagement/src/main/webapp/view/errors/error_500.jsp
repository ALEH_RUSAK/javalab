<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fatal Error</title>
    </head>
    	<h1 align=center>
			<bean:message key="error500.title" />
		</h1>
		<h3 align=center>
			<bean:message key="error500.content" />
		</h3>
    </body>
</html>
