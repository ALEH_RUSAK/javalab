<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<script src="js/script-messages.jsp" type="text/javascript"></script>
<script src="js/validation.js" type="text/javascript"></script>

<div class="right_side">

	<div class="navigation_links">
		<html:link href="newsList.do">
			<bean:message key="link.main" />
		</html:link>
		>>
		<bean:message key="link.list" />
	</div>
	
	<div class="valid_errors">
		<html:errors/>
	</div>
	
	<html:form action="/multipleDeletion" onsubmit="return deleteNews()">
		
		<logic:iterate id="news" name="newsForm" property="newsList">
			<div class="info">
				<div class="title">
					<strong> 
						<bean:write name="news" property="title" />
					<br/> 
						<bean:write	name="news" property="brief" />
					</strong>
				</div>
				
				<span>
					<bean:write name="news" property="date" formatKey="date.format"/>
				</span>
				
				<div class="clearfix">
				</div>
				
				<div class="links">
					<bean:define id="id" name="news" property="newsId" />
					
					<html:link paramId="newsMessage.newsId" paramName="id" href="viewNews.do">
						<bean:message key="news.link.view" />
					</html:link>
					
					<html:link paramId="newsMessage.newsId" paramName="id" href="editNews.do">
						<bean:message key="news.link.edit" />
					</html:link>
					
					<html:multibox property="selectedNews">
						<bean:write name="news" property="newsId" /> 
					</html:multibox>
				</div>
			</div>
		</logic:iterate>
		
		<logic:empty name="newsForm" property="newsList">
			<div class="info">
				<div class="no_news">
					<bean:message key="empty.news.list" />
				</div>
			</div>
		</logic:empty>
		
		<logic:notEmpty name="newsForm" property="newsList">
			<div class="button_area">
				<html:submit styleId="button_style">
					<bean:message key="button.delete" />
				</html:submit>
			</div>
		</logic:notEmpty>
	</html:form>
</div>
<div class="clearfix"></div>