<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<script src="js/script-messages.jsp" type="text/javascript"></script>
<script src="js/validation.js" type="text/javascript"></script>

<div class="right_side">
	<div class="navigation_links">
		<html:link href="newsList.do">
			<bean:message key="link.main" />
		</html:link>
		>>
		<bean:message key="link.view" />
	</div>

	<bean:define id="news" name="newsForm" property="newsMessage" />
	
	<div class="view_page">
		<div class="block">
			<div class="block_name">
				<bean:message key="label.news.title" />:
			</div>
			<div class="elem">
				<bean:write name="news" property="title" />
			</div>
		</div>
		<div class="block">
			<div class="block_name">
				<bean:message key="label.news.date" />:
			</div>
			<div class="elem">
				<bean:write name="news" property="date" formatKey="date.format"/>
			</div>
		</div>
		<div class="block">
			<div class="block_name">
				<bean:message key="label.news.brief" />:
			</div>
			<div class="brief">
				<bean:write name="news" property="brief" />
			</div>
		</div>
		<div class="block">
			<div class="block_name">
				<bean:message key="label.news.content" />:
			</div>
			<div class="content">
				<bean:write name="news" property="content" />
			</div>
		</div>
	</div>
	
	<html:form action="/singleDeletion" styleClass="button_form">
		<html:submit onclick="return confirmDeletion();" styleId="button_style">
			<bean:message key="button.delete" />
		</html:submit>
	</html:form>
	
	<html:form action="/editNews" styleClass="button_form">
		<html:submit styleId="button_style">
			<bean:message key="button.edit" />
		</html:submit>
	</html:form>
</div>
<div class="clearfix"></div>
