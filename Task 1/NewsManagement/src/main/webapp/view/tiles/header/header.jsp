<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<div class="header">
	<div class="top">
		<div class="title">
			<bean:message key="header.title" />
		</div>
		
		<div class="language_links">
			<bean:define id="en" value="en" />
			<html:link href="changeLanguage.do" paramId="locale" paramName="en">
				<bean:message key="header.lang.english" />
			</html:link>
			
			<bean:define id="ru" value="ru" />
			<html:link href="changeLanguage.do" paramId="locale" paramName="ru">
				<bean:message key="header.lang.russian" />
			</html:link>
		</div>
	</div>
</div>