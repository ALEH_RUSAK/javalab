<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="categoryName" />

	<xsl:template name="viewSubcategories">
		<xsl:param name="subcategoryName" select="@name" />
		<p>
			<a class="ref" href="servlet?commandBeanId=viewProducts&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}">
				<xsl:value-of select="$subcategoryName" />
			</a>
			(
			<xsl:value-of select="count(product)" />
			)
		</p>
	</xsl:template>

	<xsl:template match="/">
		<html>
			<head>
				<title>Subcategories</title>
				<link href="style/style.css" rel="stylesheet" type="text/css" />
			</head>
			<body>
				<div class="header">
					<h1>Welcome to our DeviceShop!</h1>
					<h3>Choose subcategory:</h3>
				</div>
				<div class="mainBlock">
					<xsl:for-each select="deviceShop/category[@name=$categoryName]/subcategory">
						<div class="linkBlock">
							<xsl:call-template name="viewSubcategories" />
						</div>
					</xsl:for-each>
					<div class="linkBlock">
						<a class="back" href="servlet?commandBeanId=viewCategories">Back</a>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>