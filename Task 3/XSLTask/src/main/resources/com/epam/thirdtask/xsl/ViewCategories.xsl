<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="viewCategories">
		<xsl:param name="categoryName" select="@name" />
		<p>
			<a class="ref" href="servlet?commandBeanId=viewSubcategories&amp;categoryName={$categoryName}">
				<xsl:value-of select="$categoryName" /> </a>
			(
			<xsl:value-of select="count(subcategory/product)" />
			)
		</p>
	</xsl:template>
	
	<xsl:template match="/">
		<html>
			<head>
				<title>Categories</title>
				<!-- <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen"/> -->
				<link href="style/style.css" rel="stylesheet" type="text/css" />
			</head>
			<body>
				<div class="header">
					<h1>Welcome to our DeviceShop!</h1>
					<h3>Choose category:</h3>
				</div>
				<div class="mainBlock">
					<xsl:for-each select="//category">
						<div class="linkBlock">
							<xsl:call-template name="viewCategories" />
						</div>
					</xsl:for-each>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>