<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:product="xalan://com.epam.thirdtask.model.Product"
	xmlns:validator="xalan://com.epam.thirdtask.validator.ProductValidator">

	<xsl:param name="product" />
	<xsl:param name="validator" />

	<xsl:param name="categoryName" />
	<xsl:param name="subcategoryName" />
	
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="validator:validateProduct($validator, $product)">
				<xsl:apply-templates />
			</xsl:when>
			<xsl:otherwise>
				<html>
					<head>
						<link href="style/style.css" rel="stylesheet" type="text/css"></link>
						<title>Add new Product</title>
					</head>
					<body>
						<div class="header">
							<h1>Welcome to our DeviceShop!</h1>
							<h3>
								<xsl:value-of select="$subcategoryName" />
							</h3>
						</div>
						<div class="mainBlock">
							<p align="center">Add New Product</p>
							<xsl:call-template name="addProduct" />
						</div>
					</body>
				</html>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="addProduct">
		<form action="servlet" method="GET">
			<input type="hidden" name="commandBeanId" value="saveProduct" />
			<input type="hidden" name="categoryName" value="{$categoryName}" />
			<input type="hidden" name="subcategoryName" value="{$subcategoryName}" />
			<div class="propName">
				<xsl:text>Producer:</xsl:text>
			</div>
			<input type="text" name="productParam" value="{product:getProducer($product)}" class="textField" />
			<div class="propName">
				<xsl:text>Model:</xsl:text>
			</div>
			<input type="text" name="productParam" value="{product:getModel($product)}" class="textField" />
			<div class="propName">
				<xsl:text>Date:</xsl:text>
			</div>
			<input type="text" name="productParam" value="{product:getDate($product)}" class="textField" />
			<div class="propName">
				<xsl:text>Color:</xsl:text>
			</div>
			<input type="text" name="productParam" value="{product:getColor($product)}" class="textField" />
			<div class="propName">
				<xsl:text>Price:</xsl:text>
			</div>
			<input type="text" name="productParam" value="{product:getPrice($product)}" class="textField" />
			<div class="propName">
				<xsl:text>Not in stock:</xsl:text>
			</div>
			<input type="checkbox" name="productParam" class="checkBoxBlock"/>
			<div class="emptyBlock">
			</div>
			<xsl:if test="validator:isProducerValid($validator)=false">
				<div class="errors">
					<xsl:text>Producer required.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isModelRequiredValid($validator)=false">
				<div class="errors">
					<xsl:text>Model required.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isModelValid($validator)=false">
				<div class="errors">
					<xsl:text>Invalid model format.
						Should consist of 2 letters and 3 digits.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isDateRequiredValid($validator)=false">
				<div class="errors">
					<xsl:text>Date required.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isDateValid($validator)=false">
				<div class="errors">
					<xsl:text>Preferred date format is dd-MM-YYYY.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isColorValid($validator)=false">
				<div class="errors">
					<xsl:text>Color required.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isPriceRequiredValid($validator)=false">
				<div class="errors">
					<xsl:text>Price required.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isPriceValid($validator)=false">
				<div class="errors">
					<xsl:text>Invalid price value. Should be an integer value.</xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="validator:isNotInStockValid($validator)=false">
				<div class="errors">
					<xsl:text>You can't set(unset) "Price" and "Not in stock" at the same time.</xsl:text>
				</div>
			</xsl:if>
			<div class="buttonBlock">
				<input class="buttonStyle" type="submit" value="Save" />
			</div>
		</form>
		<form class="cancel" action="servlet" method="GET">
			<div class="buttonBlock">
				<input class="buttonStyle" type="submit" value="Cancel" />
				<input type="hidden" name="commandBeanId" value="viewProducts" />
				<input type="hidden" name="categoryName" value="{$categoryName}" />
				<input type="hidden" name="subcategoryName" value="{$subcategoryName}" />
			</div>
		</form>
	</xsl:template>

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="deviceShop/category[@name=$categoryName]/subcategory[@name=$subcategoryName]">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
			<xsl:element name="product">
				<xsl:element name="producer">
					<xsl:value-of select="product:getProducer($product)" />
				</xsl:element>
				<xsl:element name="model">
					<xsl:value-of select="product:getModel($product)" />
				</xsl:element>
				<xsl:element name="date">
					<xsl:value-of select="product:getDate($product)" />
				</xsl:element>
				<xsl:element name="color">
					<xsl:value-of select="product:getColor($product)" />
				</xsl:element>
				<xsl:choose>
					<xsl:when test="product:getPrice($product)=''">
						<xsl:element name="notInStock">
							<xsl:value-of select="product:getNotInStock($product)" />
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="price">
							<xsl:value-of select="product:getPrice($product)" />
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>