<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="subcategoryName" />
	<xsl:param name="categoryName" />
	
	<xsl:template name="viewProducts">
		<tr>
			<td class="displayElement"><xsl:value-of select="producer"/></td>
			<td class="displayElement"><xsl:value-of select="model"/></td>
			<td class="displayElement"><xsl:value-of select="date"/></td>
			<td class="displayElement"><xsl:value-of select="color"/></td>
			<td class="displayElement">
				<xsl:choose>
					<xsl:when test="price&gt;0">
						<xsl:value-of select="price"/>
						<xsl:text> $</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="notInStock"/>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	
	<xsl:template match="/">
		<html>
			<head>
				<title>Products</title>
				<link href="style/style.css" rel="stylesheet" type="text/css" />
			</head>
			<body>
				<div class="header">
					<h1>Welcome to our DeviceShop!</h1>
					<h3><xsl:value-of select="$subcategoryName"/></h3>
				</div>
				<div class="mainBlock">
					<div class="products">
						<table border="2" width="100%">
							<tr style="font-size: 18px; font-family: Arial, Helvetica, sans-serif">
							    <td>Producer</td>
							    <td>Model</td> 
							    <td>Date</td>
							    <td>Color</td> 
							    <td>Price</td>
							</tr>
							<xsl:for-each select="deviceShop/category[@name=$categoryName]/subcategory[@name=$subcategoryName]/product">
								<xsl:call-template name="viewProducts" />
							</xsl:for-each>
						</table>
					</div>
					<form action="servlet" method="GET">
						<div class="addButtonBlock">
							<input class="buttonStyle" type="submit" name="add" value="Add Product"/>
							<input type="hidden" name="commandBeanId" value="addProduct" />
							<input type="hidden" name="categoryName" value="{$categoryName}" />
							<input type="hidden" name="subcategoryName" value="{$subcategoryName}" />
						</div>
					</form>
					<div class="linkBlock">
						<a class="back" href="servlet?commandBeanId=viewSubcategories&amp;categoryName={$categoryName}">Back</a>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>