package com.epam.thirdtask.constants;

import com.epam.thirdtask.util.ResourceLocator;

public final class Constants {

	private Constants() {}
	/**
	 * XML file path
	 */
	private static final String XML_PATH = "com/epam/thirdtask/xml/products.xml";
	public static final String XML_RESOURCE_PATH = ResourceLocator.getInstance().getResourcePath(XML_PATH);
	/**
	 * Command names
	 */
	public static final String VIEW_SUBCATEGORIES = "viewSubcategories";
	public static final String VIEW_PRODUCTS = "viewProducts";
	public static final String VIEW_CATEGORIES = "viewCategories";
	public static final String ADD_PRODUCT = "addProduct";
	/**
	 * XSL file paths
	 */
	public static final String XSL_VIEW_CATEGORIES = "com/epam/thirdtask/xsl/ViewCategories.xsl";
	public static final String XSL_VIEW_SUBCATEGORIES = "com/epam/thirdtask/xsl/ViewSubcategories.xsl";
	public static final String XSL_VIEW_PRODUCTS_LIST = "com/epam/thirdtask/xsl/ViewProducts.xsl";
	public static final String XSL_ADD_PRODUCT = "com/epam/thirdtask/xsl/AddProduct.xsl";
	/**
	 * Logger constants
	 */
	public static final String STARTING_COMMAND = "starting command...";
	public static final String EXECUTION_FINISHED = "execution finished.";
	public static final String RESPONSE_CONTENT_TYPE = "text/html";
	public static final String TRANSFORMER_PREPARED = "Transformer prepared: ";
	public static final String READING_LOCKED = "Reading LOCKED.";
	public static final String READING_UNLOCKED = "Reading UNLOCKED.";
	public static final String WRITING_LOCKED = "Writing LOCKED.";
	public static final String WRITING_UNLOCKED = "Writing UNLOCKED.";
	public static final String CACHE_CREATED = "Cache created.";
	/**
	 * Command identifiers
	 */
	public static final String COMMAND_PARAM = "commandBeanId";
	public static final String COMMAND_FACTORY = "commandFactory";
	/**
	 * Parameter names
	 */
	public static final String SUBCATEGORY_NAME_PARAM = "subcategoryName";
	public static final String CATEGORY_NAME_PARAM = "categoryName";
}
