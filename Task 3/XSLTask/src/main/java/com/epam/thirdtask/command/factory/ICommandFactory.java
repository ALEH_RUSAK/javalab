package com.epam.thirdtask.command.factory;

import com.epam.thirdtask.command.concrete.AbstractCommand;

/**
 * The "Factory method" design pattern interface
 * for getting <code>ICommand</code> implementations
 * @author Aleh_Rusak
 */
public interface ICommandFactory {
	
	/**
	 * Method helps to get <code>ICommand</code>
	 * implementation by it's name 
	 */
	public AbstractCommand getCommand(String commandBeanId);
}
