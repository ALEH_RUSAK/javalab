package com.epam.thirdtask.command.concrete;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.epam.thirdtask.constants.Constants;
import com.epam.thirdtask.exceptions.CommandException;

public class AddProduct extends AbstractCommand {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(AddProduct.class);
	
	@Override
	public void executeCommand(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		logger.info(Constants.STARTING_COMMAND);
		transformerParameters.clear();
		readWriteLock.readLock().lock();
    	logger.info(Constants.READING_LOCKED);
		transformerParameters.put(Constants.CATEGORY_NAME_PARAM, 
				request.getParameter(Constants.CATEGORY_NAME_PARAM));
		transformerParameters.put(Constants.SUBCATEGORY_NAME_PARAM, 
				request.getParameter(Constants.SUBCATEGORY_NAME_PARAM));
		Transformer preparedTransformer = getPreparedTransformer(response, Constants.ADD_PRODUCT);
		try {
			preparedTransformer.transform(new StreamSource(Constants.XML_RESOURCE_PATH),
					new StreamResult(response.getWriter()));
		} catch (TransformerException | IOException e) {
			logger.error(e);
			throw new CommandException(e.getMessage());
		}
		readWriteLock.readLock().unlock();
    	logger.info(Constants.READING_UNLOCKED);
		logger.info(Constants.EXECUTION_FINISHED); 
	}
}
