package com.epam.thirdtask.command.concrete;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.epam.thirdtask.constants.Constants;
import com.epam.thirdtask.exceptions.CommandException;
import com.epam.thirdtask.model.Product;
import com.epam.thirdtask.model.ProductCreator;
import com.epam.thirdtask.util.URLBuilder;
import com.epam.thirdtask.validator.ProductValidator;

public class SaveProduct extends AbstractCommand {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(SaveProduct.class);
	/**
	 * Transformer parameters
	 */
	private static final String VALIDATOR = "validator";
	private static final String PRODUCT = "product";
	/**
	 * Product parameter
	 */
	private static final String PRODUCT_PARAM = "productParam";
	
	@Override
	public void executeCommand(HttpServletRequest request, HttpServletResponse response)
			throws CommandException {
    	logger.info(Constants.STARTING_COMMAND);
		try {
			transformerParameters.clear();
			readWriteLock.readLock().lock();
			logger.info(Constants.READING_LOCKED);
			Product product = ProductCreator.createProduct(request.getParameterValues(PRODUCT_PARAM));
	    	ProductValidator validator = new ProductValidator();
	    	Transformer preparedTransformer = getPreparedTransformer(response, Constants.ADD_PRODUCT);
	    	preparedTransformer.setParameter(VALIDATOR, validator);
	    	preparedTransformer.setParameter(PRODUCT, product);
	    	Writer stringWriter = new StringWriter();
	    	preparedTransformer.transform(new StreamSource(Constants.XML_RESOURCE_PATH),
					new StreamResult(stringWriter));
			if(validator.isProductValid()) {
				readWriteLock.readLock().unlock();
				logger.info(Constants.READING_UNLOCKED);
				readWriteLock.writeLock().lock();
				logger.info(Constants.WRITING_LOCKED);
				try {
					if(validator.isProductValid()) {
						Writer xmlFileWriter = new PrintWriter(Constants.XML_RESOURCE_PATH);
						xmlFileWriter.write(stringWriter.toString());
						xmlFileWriter.flush();
						xmlFileWriter.close();
						Map<String, String> requestParametersMap = new HashMap<String, String>();
						requestParametersMap.put(Constants.CATEGORY_NAME_PARAM, 
								request.getParameter(Constants.CATEGORY_NAME_PARAM));
						requestParametersMap.put(Constants.SUBCATEGORY_NAME_PARAM, 
								request.getParameter(Constants.SUBCATEGORY_NAME_PARAM));
						setRedirectionPath(URLBuilder.buildURL(Constants.VIEW_PRODUCTS, requestParametersMap));
					}
				} finally {
					readWriteLock.readLock().lock();
					logger.info(Constants.READING_LOCKED);
				}
				readWriteLock.writeLock().unlock();
				logger.info(Constants.WRITING_UNLOCKED);
			} else {
				PrintWriter outWriter = response.getWriter();
				preparedTransformer.transform(new StreamSource(Constants.XML_RESOURCE_PATH), 
						new StreamResult(outWriter));
			}
			readWriteLock.readLock().unlock();
			logger.info(Constants.READING_UNLOCKED);
		} catch (TransformerException | IOException e) {
			logger.error(e);
			throw new CommandException(e.getMessage());
		}
		logger.info(Constants.EXECUTION_FINISHED); 
	}
}
