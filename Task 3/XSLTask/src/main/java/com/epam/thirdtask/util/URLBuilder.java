package com.epam.thirdtask.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Constructs URL for specified URL-pattern 
 * @author Aleh_Rusak
 */
public final class URLBuilder {
	/**
	 * URL-pattern
	 */
	private static final String URL_START = "servlet?commandBeanId=";
	
	public static String buildURL(String commandBeanId, Map<String, String> requestParametersMap) {
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(URL_START);
		urlBuilder.append(commandBeanId);
		Iterator<Entry<String, String>> mapIterator = requestParametersMap.entrySet().iterator();
		while (mapIterator.hasNext()) {
	        Entry<String, String> pair = mapIterator.next();
	        urlBuilder.append("&");
	        urlBuilder.append(pair.getKey());
	        urlBuilder.append("=");
	        urlBuilder.append(pair.getValue());
		}
		return urlBuilder.toString();
	}
}
