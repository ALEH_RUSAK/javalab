package com.epam.thirdtask.exceptions;

public final class CommandException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CommandException() {}

	public CommandException(String exceptionMessage) {
		this.code = 1;
		this.title = CommandException.class.toString();
		this.exceptionMessage = exceptionMessage;
	}
}
