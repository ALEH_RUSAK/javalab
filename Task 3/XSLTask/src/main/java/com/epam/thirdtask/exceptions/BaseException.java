package com.epam.thirdtask.exceptions;

/**
 * Provides common behavior 
 * of exceptions in this application
 * @author Aleh_Rusak
 */
public abstract class BaseException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Exception's unique title
	 */
	protected String title;
	/**
	 * Exception's unique code
	 */
	protected int code;
	/**
	 * Exception's message
	 */
	protected String exceptionMessage;
	
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("Exception occured: ");
		strBuilder.append(title);
		strBuilder.append(", code: ");
		strBuilder.append(code);
		strBuilder.append(", message: ");
		strBuilder.append(exceptionMessage);
		return strBuilder.toString();
	}

	public String getMessage() {
		return exceptionMessage;
	}

	public void setMessage(String message) {
		this.exceptionMessage = message;
	}
}