package com.epam.thirdtask.util;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.epam.thirdtask.constants.Constants;

/**
 * Encapsulates the process of creating and caching <code>Transformer</code>
 * objects through <code>Templates</code> objects
 * @author Aleh_Rusak
 */
public final class TransformerCache {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(TransformerCache.class);
	/**
	 * <code>TransformerFactory</code> instance
	 */
	private TransformerFactory trFactory;
	/**
	 * <code>ResourceLocator</code> instance
	 */
	private ResourceLocator resourceLocator;
	/**
	 * <code>Map</code> for caching <code>Transformer</code> objects
	 */
	private Map<String, Transformer> trCache;
	
	/**
	 * Fills in <code>trCache</code>
	 */
	public void initTransformerCache() {
	
		trFactory = TransformerFactory.newInstance();
		trCache = new HashMap<String, Transformer>();
		if (trCache != null) {
			trCache.put(Constants.VIEW_CATEGORIES,
					createTransformer(resourceLocator.getResourcePath(Constants.XSL_VIEW_CATEGORIES)));
			trCache.put(Constants.VIEW_SUBCATEGORIES,
					createTransformer(resourceLocator.getResourcePath(Constants.XSL_VIEW_SUBCATEGORIES)));
			trCache.put(Constants.VIEW_PRODUCTS,
					createTransformer(resourceLocator.getResourcePath(Constants.XSL_VIEW_PRODUCTS_LIST)));
			trCache.put(Constants.ADD_PRODUCT,
					createTransformer(resourceLocator.getResourcePath(Constants.XSL_ADD_PRODUCT)));
		}
		logger.info(Constants.CACHE_CREATED);
	}

	/**
	 * Provides creating <code>Transformer</code> instance through
	 * <code>Templates</code> instance created by <code>trFactory</code>
	 * @return <code>Transformer</code> instance
	 */
	private Transformer createTransformer(String xslPath) {
		Transformer transformer = null;
		Source xslSource = new StreamSource(xslPath);
		try {
			Templates template = trFactory.newTemplates(xslSource);
			transformer = template.newTransformer();
			logger.info("Source XSL file: " + xslPath.toString() + " found.");
		} catch (TransformerConfigurationException e) {
			logger.error(e);
		}
		return transformer;
	}

	/**
	 * Getting <code>Transformer</code> instance
	 * @param trName the name of command associated with transformer
	 * @return <code>Transformer</code> instance from the cache
	 */
	public Transformer getTransformer(String trName) {
		return trCache.get(trName);
	}

	/**
	 * Destroys transformers cache
	 */
	public void destroyTransformerCache() {
		trCache.clear();
		trCache = null;
	}

	public ResourceLocator getResourceLocator() {
		return resourceLocator;
	}

	public void setResourceLocator(ResourceLocator resourceLocator) {
		this.resourceLocator = resourceLocator;
	}
}
