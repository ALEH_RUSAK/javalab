package com.epam.thirdtask.command.concrete;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.thirdtask.constants.Constants;
import com.epam.thirdtask.exceptions.CommandException;
import com.epam.thirdtask.util.TransformerCache;

/**
 * Common specification for commands in application
 * @author Aleh_Rusak
 */
public abstract class AbstractCommand {
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(AbstractCommand.class);
	/**
	 * Redirection path
	 */
	private String redirectionPath;
	/**
	 * Transformer's cache
	 */
	@Autowired
	private TransformerCache trCache;
	/**
	 * Transformer parameters
	 */
	protected Map<String, String> transformerParameters = new HashMap<String, String>();
	/**
	 * Locker
	 */
	protected final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	
	/**
	 * Provides execution of command
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponse instance
	 */
	public abstract void executeCommand(HttpServletRequest request, HttpServletResponse response)
			throws CommandException;
	
	protected Transformer getPreparedTransformer(HttpServletResponse response, String transformerType) {
    	Transformer transformer = trCache.getTransformer(transformerType);
    	Iterator<Entry<String, String>> mapIterator = transformerParameters.entrySet().iterator();
		while (mapIterator.hasNext()) {
	        Entry<String, String> pair = mapIterator.next();
	        transformer.setParameter(pair.getKey(), pair.getValue());
		}
		if(logger.isInfoEnabled()) {
			logger.info(Constants.TRANSFORMER_PREPARED + transformerType);
		}
		return transformer;
	}
	
	public String getRedirectionPath() {
		return redirectionPath;
	}
	
	protected void setRedirectionPath(String redirectionPath) {
		this.redirectionPath = redirectionPath;
	}
}
