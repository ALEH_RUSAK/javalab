package com.epam.thirdtask.model;

/**
 * The model of product
 * @author Aleh_Rusak
 */
public final class Product {
	/**
	 * Product's producer
	 */
	private String producer;
	/**
	 * Product's model
	 */
	private String model;
	/**
	 * Product's date of issue
	 */
	private String date;
	/**
	 * Product's color
	 */
	private String color;
	/**
	 * Product's price
	 */
	private String price;
	/**
	 * Product's not in stock
	 */
	private String notInStock;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((producer == null) ? 0 : producer.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (producer == null) {
			if (other.producer != null)
				return false;
		} else if (!producer.equals(other.producer))
			return false;
		return true;
	}
	
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("Product: [ ");
		strBuilder.append(producer);
		strBuilder.append(", ");
		strBuilder.append(model);
		strBuilder.append(", ");
		strBuilder.append(date);
		strBuilder.append(", ");
		strBuilder.append(color);
		strBuilder.append(", ");
		strBuilder.append(price);
		strBuilder.append("]");
		return strBuilder.toString();
	}

	public String getProducer() {
		return producer;
	}
	
	public void setProducer(String producer) {
		this.producer = producer;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getPrice() {
		return price;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}

	public String getNotInStock() {
		return notInStock;
	}

	public void setNotInStock(String notInStock) {
		this.notInStock = notInStock;
	}
}
