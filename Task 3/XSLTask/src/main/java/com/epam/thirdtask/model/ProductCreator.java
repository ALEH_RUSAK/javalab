package com.epam.thirdtask.model;

/**
 * Encapsulates the process of creating <code>Product</code>
 * @author Aleh_Rusak
 */
public final class ProductCreator {
	
	/**
	 * Method initializes <code>Product</code>
	 * with <code>initialParameters</code>
	 */
	public static Product createProduct(String[] initialParameters) {
		Product product = new Product();
		product.setProducer(initialParameters[0]);
		product.setModel(initialParameters[1]);
		product.setDate(initialParameters[2]);
		product.setColor(initialParameters[3]);
		product.setPrice(initialParameters[4]);
		if(initialParameters.length == 6) {
			product.setNotInStock("Not in stock");
		}
		return product;
	}
}
