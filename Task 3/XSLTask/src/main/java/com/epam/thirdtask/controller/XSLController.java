package com.epam.thirdtask.controller;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.epam.thirdtask.command.concrete.AbstractCommand;
import com.epam.thirdtask.command.factory.ICommandFactory;
import com.epam.thirdtask.constants.Constants;
import com.epam.thirdtask.exceptions.CommandException;

/**
 * Servlet implementation class ServletController
 */
@WebServlet("/XSLController")
public final class XSLController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The Logger
	 */
	private static final Logger logger = Logger.getLogger(XSLController.class);
	
	@Autowired
	private ICommandFactory commandFactory;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public XSLController() {
		super();
	}
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
	}

	/**
	 * Provides processing request and executing command
	 * @param request HttpServletRequest instance
	 * @param response HttpServletResponse instance
	 * @throws ServletException 
	 * @throws IOException 
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String commandBeanId = request.getParameter(Constants.COMMAND_PARAM);
		response.setContentType(Constants.RESPONSE_CONTENT_TYPE);
		AbstractCommand command = commandFactory.getCommand(commandBeanId);
		try {
			command.executeCommand(request, response);
			if(command.getRedirectionPath() != null) {
				response.sendRedirect(command.getRedirectionPath());
			}
		} catch (CommandException e) {
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
