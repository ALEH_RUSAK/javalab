package com.epam.thirdtask.util;

/**
 * Provides getting applications resources
 * @author Aleh_Rusak
 */
public final class ResourceLocator {
	/**
	 * <code>ResourceLocator</code> reference
	 */
	private static ResourceLocator instance;
	
	/**
	 * Instance reference will be assigned here
	 * by instance of <code>ResourceLocator</code> 
	 * created by Spring container
	 */
	private ResourceLocator() {
		instance = this;
	}
	
	/**
	 * Provides getting applications resources by path
	 * @return resource's path
	 */
	public String getResourcePath(String resourceParam) {
		return getClass().getClassLoader().getResource(resourceParam).getPath();
	}
	
	public static ResourceLocator getInstance() {
		return instance;
	}
}
