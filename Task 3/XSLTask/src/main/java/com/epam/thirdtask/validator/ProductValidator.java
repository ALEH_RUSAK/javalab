package com.epam.thirdtask.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.thirdtask.model.Product;

/**
 * Provides implementation for validating 
 * <code>Products</code> before adding them
 * into xml
 * @author Aleh_Rusak
 */
public final class ProductValidator {
	/**
	 * Regular expressions
	 */
	private static final String DATE_REGEXP = "(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}";
	private static final String MODEL_REGEXP = "([A-Z]|[a-z]){2}([1-9]{3})";
	private static final String PRICE_REGEXP = "[0-9]+([.][0-9]{0,1})?";
	/**
	 * Validation parameters
	 */
	private static final String DATE_PARAM = "date";
	private static final String MODEL_PARAM = "model";
	private static final String PRICE_PARAM = "price";
	private static final String COLOR_PARAM = "color";
	private static final String PRODUCER_PARAM = "producer";
	/**
	 * Validation flags
	 */
	private boolean productValid = true;
	private boolean priceRequiredValid = true;
	private boolean priceValid = true;
	private boolean dateRequiredValid = true;
	private boolean dateValid = true;
	private boolean colorValid = true;
	private boolean producerValid = true;
	private boolean modelRequiredValid = true;
	private boolean modelValid = true;
	private boolean notInStockValid = true;

	/**
	 * Validates date entered by user
	 * @param date <code>String</code> representation of a date entered by user
	 * @return true in case of matching
	 */
	private boolean validateFieldByRegexp(String paramType, String regexp, String paramValue) {
		boolean requiredValid = true;
		boolean valid = true;
		if("".equals(paramValue)) {
			requiredValid = false;
		} else {
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(paramValue);
			if(!matcher.matches()) {
				valid = false;
			}
		}
		if(!requiredValid || !valid) {
			if(paramType.equals(DATE_PARAM)) {
				dateRequiredValid = requiredValid;
				dateValid = valid;
			} else if(paramType.equals(MODEL_PARAM)) {
				modelRequiredValid = requiredValid;
				modelValid = valid;
			} else if(paramType.equals(PRICE_PARAM)) {
				priceRequiredValid = requiredValid;
				priceValid = valid;
			}
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Checks, if field is empty
	 * @param paramValue parameter's value
	 * @param paramType parameter's type
	 * @return the boolean value
	 */
	private boolean validateByExistence(String paramType, String paramValue) {
		boolean valid = true;
		if("".equals(paramValue)) {
			valid = false;
		} 
		if(!valid) {
			if(paramType.equals(COLOR_PARAM)) {
				colorValid = valid;
			} else if(paramType.equals(PRODUCER_PARAM)) {
				producerValid = valid;
			} 
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Validates case when checkbox is checked
	 * @param product <code>Product</code> to be validated
	 * @return true, if all is right
	 */
	private boolean validateNotInStock(Product product) {
		if(product.getNotInStock() != null) {
			notInStockValid = true;
			product.setPrice("");
		} else {
			if(validateFieldByRegexp(PRICE_PARAM, PRICE_REGEXP, product.getPrice())) {
				notInStockValid = true;
			} else {
				notInStockValid = false;
			}
		}
		return notInStockValid;
	}
	
	/**
	 * Validates whole product
	 * @param product <code>Product</code> to be validated
	 * @return true, if all is right
	 */
	public boolean validateProduct(Product product) {
		if(validateFieldByRegexp(DATE_PARAM, DATE_REGEXP, product.getDate()) & 
				validateFieldByRegexp(MODEL_PARAM, MODEL_REGEXP, product.getModel()) &
				validateByExistence(PRODUCER_PARAM, product.getProducer()) &
				validateByExistence(COLOR_PARAM, product.getColor()) &
				validateNotInStock(product)) {
			productValid = true;
		} else {
			productValid = false;
		}
		return productValid;
	}

	/**
	 * @return the priceRequiredValid
	 */
	public boolean isPriceRequiredValid() {
		return priceRequiredValid;
	}
	
	/**
	 * @return the dateRequiredValid
	 */
	public boolean isDateRequiredValid() {
		return dateRequiredValid;
	}

	/**
	 * @return the priceValid
	 */
	public boolean isPriceValid() {
		return priceValid;
	}

	/**
	 * @return the dateValid
	 */
	public boolean isDateValid() {
		return dateValid;
	}

	/**
	 * @return the colorValid
	 */
	public boolean isColorValid() {
		return colorValid;
	}

	/**
	 * @return the producerValid
	 */
	public boolean isProducerValid() {
		return producerValid;
	}

	/**
	 * @return the modelValid
	 */
	public boolean isModelValid() {
		return modelValid;
	}

	/**
	 * @return the modelRequiredValid
	 */
	public boolean isModelRequiredValid() {
		return modelRequiredValid;
	}

	/**
	 * @return the productValid
	 */
	public boolean isProductValid() {
		return productValid;
	}

	/**
	 * @return the notInStockValid
	 */
	public boolean isNotInStockValid() {
		return notInStockValid;
	}
}
