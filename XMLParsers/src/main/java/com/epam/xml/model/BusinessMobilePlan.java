package com.epam.xml.model;

import java.io.Serializable;

/**
 * Provides description of mobile plan for businessmen with it's features.
 * @author Aleh_Rusak
 */
public class BusinessMobilePlan extends MobilePlan implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/* Business features */
	private String businessRoaming;
	private String eMailToSMS;
	
	public BusinessMobilePlan() {}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((businessRoaming == null) ? 0 : businessRoaming.hashCode());
		result = prime * result
				+ ((eMailToSMS == null) ? 0 : eMailToSMS.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessMobilePlan other = (BusinessMobilePlan) obj;
		if (businessRoaming == null) {
			if (other.businessRoaming != null)
				return false;
		} else if (!businessRoaming.equals(other.businessRoaming))
			return false;
		if (eMailToSMS == null) {
			if (other.eMailToSMS != null)
				return false;
		} else if (!eMailToSMS.equals(other.eMailToSMS))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BusinessMobilePlan [");
		builder.append(super.toString());
		builder.delete(18, 30);
		builder.append(", businessRoaming = ");
		builder.append(businessRoaming);
		builder.append(", eMailToSMS = ");
		builder.append(eMailToSMS);
		builder.append("]");
		return builder.toString();
	}

	public void setBusinessRoaming(String businessRoaming) {
		this.businessRoaming = businessRoaming;
	}

	public String getBusinessRoaming() {
		return businessRoaming;
	}

	public String geteMailToSMS() {
		return eMailToSMS;
	}

	public void seteMailToSMS(String eMailToSMS) {
		this.eMailToSMS = eMailToSMS;
	}
}
