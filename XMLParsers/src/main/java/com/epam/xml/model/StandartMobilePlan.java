package com.epam.xml.model;

import java.io.Serializable;

/**
 * class StandartMobilePlan provides tariffs for all people
 * @author Oleg
 */

public class StandartMobilePlan extends MobilePlan implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/* Standart features */
	private String favouriteNumbers;
	private int freeSMS;

	public StandartMobilePlan() {}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((favouriteNumbers == null) ? 0 : favouriteNumbers.hashCode());
		result = prime * result + freeSMS;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		StandartMobilePlan other = (StandartMobilePlan) obj;
		if (favouriteNumbers == null) {
			if (other.favouriteNumbers != null)
				return false;
		} else if (!favouriteNumbers.equals(other.favouriteNumbers))
			return false;
		if (freeSMS != other.freeSMS)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StandartMobilePlan [");
		builder.append(super.toString());
		builder.delete(18, 30);
		builder.append(", favouriteNumbers = ");
		builder.append(favouriteNumbers);
		builder.append(", freeSMS = ");
		builder.append(freeSMS);
		builder.append("]");
		return builder.toString();
	}
	
	public void setFreeSMS(int freeSMS) {
		this.freeSMS = freeSMS;
	}

	public int getFreeSMS() {
		return freeSMS;
	}
	
	public void setFavouriteNumbers(String favouriteNumbers) {
		this.favouriteNumbers = favouriteNumbers;
	}

	public String getFavouriteNumbers() {
		return favouriteNumbers;
	}
}
