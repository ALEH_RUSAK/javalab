package com.epam.xml.util.builder;

import java.util.List;

import com.epam.xml.model.MobilePlan;

public abstract class MobileBuilder {
	
	protected List<String> initialParameters;
	
	protected MobilePlan mobilePlan;
	
	/* This method has to be implemented in sub-builders.
	   It initializes fields of class which implements method */
	public abstract void buildMobilePlan();
	
	public void setInitialParameters(List<String> initialParameters) {
		this.initialParameters = initialParameters;
	}
	
	public MobilePlan getMobilePlan() {
		return mobilePlan;
	}
}
