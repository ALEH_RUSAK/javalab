package com.epam.xml.util;

import com.epam.xml.constant.Constant;

/**
 * The utility class provides validating name
 * coming from the XML during parsing.
 * @author Aleh_Rusak
 */
public final class NameValidator {
	
	private NameValidator() {}
	
	public static boolean validateElementName(String elementName) {
		elementName = elementName.toUpperCase();
		if((elementName.equals(Constant.BUSINESS) || (elementName.equals(Constant.STANDART)))) {
			return true;
		} else {
			return false;
		}
	}
}
