package com.epam.xml.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.epam.xml.model.MobilePlan;

/**
 * Provides a set of utility methods to work with mobile plans objects.
 * @author Aleh_Rusak
 */
public final class MobileCache {

	private static Map<String, MobilePlan> mobilePlanCache = new HashMap<String, MobilePlan>();
	
	public static void setMobileCache(List<MobilePlan> mobilePlanList) {
		for(MobilePlan mobilePlan: mobilePlanList) {
			mobilePlanCache.put(mobilePlan.getName(), mobilePlan);
		}
	}
	
	public static List<MobilePlan> getMobilePlans() {
		List<MobilePlan> mobilePlanList = new ArrayList<MobilePlan>();
		for(Entry<String, MobilePlan> key: mobilePlanCache.entrySet()) {
			mobilePlanList.add(mobilePlanCache.get(key));
		}
		return mobilePlanList;
	}
	
	public static MobilePlan getPlanByName(String name) {
		return mobilePlanCache.get(name);
	}
}
