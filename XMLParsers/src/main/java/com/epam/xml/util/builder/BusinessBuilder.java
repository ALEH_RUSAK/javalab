package com.epam.xml.util.builder;

import com.epam.xml.model.BusinessMobilePlan;

public final class BusinessBuilder extends MobileBuilder {

	@Override
	public void buildMobilePlan() {
		BusinessMobilePlan businessPlan = new BusinessMobilePlan();
		businessPlan.setName(initialParameters.get(0));
		businessPlan.setFee(Double.parseDouble(initialParameters.get(1)));
		businessPlan.setNumOfClients(Integer.parseInt(initialParameters.get(2)));
		businessPlan.setBusinessRoaming(initialParameters.get(3));
		businessPlan.seteMailToSMS(initialParameters.get(4));
		mobilePlan = businessPlan;
	}
}
