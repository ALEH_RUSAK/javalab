package com.epam.xml.util.builder;

import com.epam.xml.model.StandartMobilePlan;

public final class StandartBuilder extends MobileBuilder {

	@Override
	public void buildMobilePlan() {
		StandartMobilePlan standartPlan = new StandartMobilePlan();
		standartPlan.setName(initialParameters.get(0));
		standartPlan.setFee(Double.parseDouble(initialParameters.get(1)));
		standartPlan.setNumOfClients(Integer.parseInt(initialParameters.get(2)));
		standartPlan.setFavouriteNumbers(initialParameters.get(3));
		standartPlan.setFreeSMS(Integer.parseInt(initialParameters.get(4)));
		mobilePlan = standartPlan;
	}
}
