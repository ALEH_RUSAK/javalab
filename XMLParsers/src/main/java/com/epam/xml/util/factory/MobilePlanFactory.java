package com.epam.xml.util.factory;

import java.util.List;

import com.epam.xml.model.MobilePlan;
import com.epam.xml.model.name.MobilePlanName;
import com.epam.xml.util.builder.BusinessBuilder;
import com.epam.xml.util.builder.MobileBuilder;
import com.epam.xml.util.builder.StandartBuilder;

/**
 * The Factory Method implementation. 
 * Provides producing <code>MobilePlan</code>'s instances using data coming from XML.
 * @author Aleh_Rusak
 */
public final class MobilePlanFactory {

	private MobilePlanFactory() {}
	
	/**
	 * Provides building some <code>MobilePlan</code> instance.
	 * @param initialParameters the parameters which are used to initialize mobile plan
	 * @param planName the name of the mobile plan
	 * @return the built instance of mobile plan
	 */
	public static MobilePlan getMobilePlan(List<String> initialParameters, MobilePlanName planName) {
		MobileBuilder mobileBuilder = null;
		switch (planName) {
		case BUSINESS:
			mobileBuilder = new BusinessBuilder();
			break;
		case STANDART:
			mobileBuilder = new StandartBuilder();
			break;
		default:
			throw new EnumConstantNotPresentException(MobilePlanName.class, planName.name());
		}
		mobileBuilder.setInitialParameters(initialParameters);
		mobileBuilder.buildMobilePlan();
		return mobileBuilder.getMobilePlan();
	}
}
