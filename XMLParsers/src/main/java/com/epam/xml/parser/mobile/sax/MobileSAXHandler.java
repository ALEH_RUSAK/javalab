package com.epam.xml.parser.mobile.sax;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.epam.xml.model.MobilePlan;
import com.epam.xml.model.name.MobilePlanName;
import com.epam.xml.util.NameValidator;
import com.epam.xml.util.factory.MobilePlanFactory;

/**
 * Handler class, which object is going to be used in SAX parser.
 * Provides implementation for XML-elements processing.
 * @author Aleh_Rusak
 */
public class MobileSAXHandler extends DefaultHandler {
	
	//private static final Logger LOGGER = Logger.getLogger(MobileSAXHandler.class);
	
	/**
	 * The list of mobile plans
	 */
	private List<MobilePlan> mobilePlansList;
	/**
	 * The list of initial parameters for each mobile plan
	 */
	private List<String> initialParameters;
	
	@Override
	public void startDocument() throws SAXException {
		initialParameters = new ArrayList<String>();
		mobilePlansList = new ArrayList<MobilePlan>();
		//LOGGER.info("Parsing was started.");
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		if(NameValidator.validateElementName(qName)) {
			initialParameters.clear();
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String value = new String(ch, start, length).trim();
		if(!value.isEmpty()) {
			initialParameters.add(value); 
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(NameValidator.validateElementName(qName)) {
			mobilePlansList.add(MobilePlanFactory.getMobilePlan(initialParameters, 
					MobilePlanName.valueOf(qName.toUpperCase())));
		}
	}
	
	@Override
	public void endDocument() {
		//LOGGER.info("Parsing was ended.");
	}
	
	public List<MobilePlan> getMobilePlansList() {
		return mobilePlansList;
	}
	
	public void setMobilePlansList(List<MobilePlan> mobilePlanList) {
		this.mobilePlansList = mobilePlanList;
	}
}
