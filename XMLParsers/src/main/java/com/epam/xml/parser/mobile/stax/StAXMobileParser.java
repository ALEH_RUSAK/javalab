package com.epam.xml.parser.mobile.stax;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.epam.xml.constant.Constant;
import com.epam.xml.model.MobilePlan;
import com.epam.xml.model.name.MobilePlanName;
import com.epam.xml.parser.mobile.MobileXMLParser;
import com.epam.xml.util.NameValidator;
import com.epam.xml.util.factory.MobilePlanFactory;

public class StAXMobileParser implements MobileXMLParser {
	
	//private static final Logger LOGGER = Logger.getLogger(StAXMobileParser.class);
	
	private XMLInputFactory factory;
	private XMLStreamReader reader;
	
	public StAXMobileParser() {
		try {
			factory = XMLInputFactory.newInstance();
			reader = factory.createXMLStreamReader(new FileReader(Constant.MOBILE_PLAN_XML_PATH));
		} catch (XMLStreamException | FileNotFoundException e) {
			//LOGGER.error(e);
		}
	}
	
	@Override
	public List<MobilePlan> parseXML() {
		List<String> initialParameters = new ArrayList<String>();
		List<MobilePlan> mobilePlanList = new ArrayList<MobilePlan>();
		try {
			MobilePlanName plansName = null;
			while(reader.hasNext()) {
				int event = reader.next();
				if((event == XMLStreamConstants.START_ELEMENT) && 
						NameValidator.validateElementName(reader.getLocalName())) {
					plansName = MobilePlanName.valueOf(reader.getLocalName().toUpperCase());
					initialParameters.clear();
				} 
				if(event == XMLStreamConstants.CHARACTERS){
					String value = reader.getText().trim();
					if(!value.isEmpty()) {
						initialParameters.add(value);
					}
				}
				if((event == XMLStreamConstants.END_ELEMENT) && 
						NameValidator.validateElementName(reader.getLocalName())) {
					mobilePlanList.add(MobilePlanFactory.getMobilePlan(initialParameters, plansName));
				}
			}
		} catch(XMLStreamException e) {
			e.printStackTrace();
			//LOGGER.error(e);
		}
		return mobilePlanList;
	}
}
