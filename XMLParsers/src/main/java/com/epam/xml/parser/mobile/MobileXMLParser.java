package com.epam.xml.parser.mobile;

import java.util.List;

import com.epam.xml.model.MobilePlan;
import com.epam.xml.parser.XMLParser;

/**
 * Provides parsing an XML file which contains info about
 * company's mobile plans. Result of parsing will be the 
 * list of mobile plans and their info.
 * @author Aleh_Rusak
 */
public interface MobileXMLParser extends XMLParser<MobilePlan> {
	
	/**
	 * Method should be implemented by class-parser of XML
	 * that contains mobile plans information.
	 * @see <code>XMLParser</code> interface description.
	 * @return the list of mobile plans
	 */
	public List<MobilePlan> parseXML();
}
