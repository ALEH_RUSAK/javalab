package com.epam.xml.parser.mobile.sax;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.epam.xml.constant.Constant;
import com.epam.xml.model.MobilePlan;
import com.epam.xml.parser.mobile.MobileXMLParser;

/**
 * The SAX implementation for <code>MobileXMLParser</code>
 * @author Aleh_Rusak
 */
public final class SAXMobileParser implements MobileXMLParser {
	
	//private static final Logger LOGGER = Logger.getLogger(SAXMobileParser.class);
	
	private SAXParserFactory factory;
	private MobileSAXHandler planHandler;
	private SAXParser saxParser;
	
	public SAXMobileParser() {
		try {
			factory = SAXParserFactory.newInstance();
			saxParser = factory.newSAXParser();
		} catch (ParserConfigurationException | SAXException e) {
			//LOGGER.error(e);
		} 
		planHandler = new MobileSAXHandler();
	}

	/**
	 * Method executes the SAX-parser and fills the list of mobile plans
	 * @return 
	 */
	@Override
	public List<MobilePlan> parseXML() {
		try {
			saxParser.parse(Constant.MOBILE_PLAN_XML_PATH, planHandler);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			//LOGGER.error(e);
		}
		return planHandler.getMobilePlansList();
	}
}
