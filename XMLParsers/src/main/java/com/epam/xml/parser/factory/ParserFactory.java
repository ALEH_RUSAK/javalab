package com.epam.xml.parser.factory;

import com.epam.xml.parser.XMLParserName;
import com.epam.xml.parser.mobile.MobileXMLParser;
import com.epam.xml.parser.mobile.dom.DOMMobileParser;
import com.epam.xml.parser.mobile.sax.SAXMobileParser;
import com.epam.xml.parser.mobile.stax.StAXMobileParser;

/**
 * Factory method implementation for producing
 * XML parsers. 
 * @author Aleh_Rusak
 */
public final class ParserFactory {
	
	private ParserFactory() {}
	
	public static MobileXMLParser getXMLParser(XMLParserName parserName) {
		MobileXMLParser parser = null;
		switch(parserName) {
		case DOM:
			parser = new DOMMobileParser();
			break;
		case SAX:
			parser = new SAXMobileParser();
			break;
		case STAX:
			parser = new StAXMobileParser();
			break;
		}
		return parser;
	}
}
