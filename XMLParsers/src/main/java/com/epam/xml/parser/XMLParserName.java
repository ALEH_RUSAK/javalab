package com.epam.xml.parser;

/**
 * Parser names.
 * @author Aleh_Rusak
 */
public enum XMLParserName {
	DOM,
	SAX,
	STAX
}
