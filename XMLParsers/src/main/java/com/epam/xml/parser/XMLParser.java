package com.epam.xml.parser;

import java.util.List;

/**
 * Provides the only method to be implemented by any XML parser.
 * The highest XML processing abstraction.
 * @author Aleh_Rusak
 */
public interface XMLParser<T> {
	
	/**
	 * This method should be implemented by any XML parser.
	 * The implementation should actually contain parsing logic.
	 * @return the list of objects of specified type.
	 */
	public List<T> parseXML();
}
