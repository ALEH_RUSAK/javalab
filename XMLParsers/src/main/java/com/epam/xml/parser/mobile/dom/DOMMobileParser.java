package com.epam.xml.parser.mobile.dom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.xml.constant.Constant;
import com.epam.xml.model.MobilePlan;
import com.epam.xml.model.name.MobilePlanName;
import com.epam.xml.parser.mobile.MobileXMLParser;
import com.epam.xml.util.NameValidator;
import com.epam.xml.util.factory.MobilePlanFactory;

/**
 * The DOM implementation for <code>MobileXMLParser</code>.
 * @author Aleh_Rusak
 */
public final class DOMMobileParser implements MobileXMLParser {

	//private static final Logger LOGGER = Logger.getLogger(DOMMobileParser.class);

	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;

	public DOMMobileParser() {
		try {
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			//LOGGER.error(e);
		}
	}

	@Override
	public List<MobilePlan> parseXML() {
		/*
		 * Load and Parse the XML document Document contains the complete XML as
		 * a DOM Tree.
		 */
		Document document = null;
		try {
			document = builder.parse(Constant.MOBILE_PLAN_XML_PATH);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			//LOGGER.error(e);
		} 
		/* List of initializing elements */
		List<String> initialParameters = new ArrayList<String>();
		List<MobilePlan> mobilePlanList = new ArrayList<MobilePlan>();
		/* Iterating through the nodes and extracting the data. */
		Element element = document.getDocumentElement();
		NodeList mobilePlanNodes = element.getChildNodes();
		for (int pIndex = 0; pIndex < mobilePlanNodes.getLength(); pIndex++) {
			MobilePlanName plansName = null;
			Node mobileNode = mobilePlanNodes.item(pIndex);
			String mobileNodeName = mobileNode.getNodeName();
			if (mobileNode instanceof Element && 
					NameValidator.validateElementName(mobileNodeName)) {
				plansName = MobilePlanName.valueOf(mobileNodeName.toUpperCase());
				initialParameters.clear();
			}

			NodeList mobileContentNodes = mobileNode.getChildNodes();
			for (int cIndex = 0; cIndex < mobileContentNodes.getLength(); cIndex++) {
				Node contentNode = mobileContentNodes.item(cIndex);
				/* Identifying the child tag of mobile plan encountered. */
				if (contentNode instanceof Element) {
					/* Taking the field content from XML */
					initialParameters.add(contentNode.getLastChild().getTextContent().trim());
				}
			}

			if (plansName != null) {
				mobilePlanList.add(MobilePlanFactory.getMobilePlan(initialParameters, plansName));
			}
		}
		return mobilePlanList;
	}
}
