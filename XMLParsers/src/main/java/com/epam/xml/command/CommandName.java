package com.epam.xml.command;

public enum CommandName {
	VIEW_MOBILE_PLANS,
	VIEW_PLAN_INFO
}
