
package com.epam.xml.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xml.command.AbstractCommand;
import com.epam.xml.constant.Constant;
import com.epam.xml.model.MobilePlan;
import com.epam.xml.parser.XMLParserName;
import com.epam.xml.parser.factory.ParserFactory;
import com.epam.xml.parser.mobile.MobileXMLParser;

/**
 * Provides viewing the mobile plans list.
 * @author Aleh_Rusak
 */
public class ViewMobilePlans extends AbstractCommand {

	private static final String MOBILE_MAIN_PATH = "pages/mobile_main.jsp";
	
	@Override
	public void executeCommand(HttpServletRequest request, HttpServletResponse response) {
		String parserName = request.getParameter(Constant.PARSER_TYPE_PARAM);
		MobileXMLParser parser = ParserFactory.getXMLParser(XMLParserName.valueOf(parserName));
		List<MobilePlan> mobilePlanList = parser.parseXML();
		request.getSession().setAttribute(Constant.MOBILE_LIST_ATTR, mobilePlanList);
		this.setRedirectAndPath(MOBILE_MAIN_PATH, true);
	}
}
