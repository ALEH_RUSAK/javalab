package com.epam.xml.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command design pattern implementation for the application.
 * Every request is interpreted as a command and servlet might resolve
 * what to do if some command has to be executed.
 * @author Aleh_Rusak
 */
public abstract class AbstractCommand {
	
	/**
	 * Path to be redirected on or forwarded to.
	 */
	private String path;
	/**
	 * The redirect field
	 */
	private boolean redirect;

	/**
	 * @param redirect the redirect to set
	 */
	protected final void setRedirectAndPath(String path, boolean redirect) {
		this.path = path;
		this.redirect = redirect;
	}
	
	/**
	 * Command is executed by servlet. This method should be implemented 
	 * by every command to be managed by servlet.
	 * @param request the <code>HttpServletRequest</code> instance
	 * @param response the <code>HttpServletResponse</code> instance
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public abstract void executeCommand(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException; 

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return the redirect
	 */
	public boolean isRedirect() {
		return redirect;
	}
}
