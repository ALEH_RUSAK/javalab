package com.epam.xml.command;

import com.epam.xml.command.impl.ViewMobilePlans;

/**
 * The Factory Method implementation. 
 * Provides producing <code>ICommand</code>'s instances by it's name.
 * @author Aleh_Rusak
 */
public final class CommandFactory {
	
	private CommandFactory() {}
	
	/**
	 * Produces commands using their names which come from the request.
	 * @param commandName the name of the command
	 * @return the ICommand instance
	 */
	public static AbstractCommand getCommand(CommandName commandName) {
		AbstractCommand command = null;
		switch(commandName) {
		case VIEW_MOBILE_PLANS:
			command = new ViewMobilePlans();
			break;
		case VIEW_PLAN_INFO:
			//command = new ViewPlanInfo();
			break;
		default:
			throw new EnumConstantNotPresentException(CommandName.class, commandName.name());	
		}
		return command;
	}
}
