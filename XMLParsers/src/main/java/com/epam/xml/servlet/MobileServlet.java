package com.epam.xml.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xml.command.AbstractCommand;
import com.epam.xml.command.CommandFactory;
import com.epam.xml.command.CommandName;
import com.epam.xml.constant.Constant;

/**
 * Servlet implementation class MobileServlet.
 * Provides managing requests and responces from users
 * who want to get information about mobile plans.
 */
public class MobileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	//private static final Logger LOGGER = Logger.getLogger(MobileServlet.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MobileServlet() {
        super();
        //LOGGER.debug(Constant.SERVLET_CONTSTRUCTOR_CALL);
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		//LOGGER.debug(Constant.SERVLET_LOADED);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String commandName = request.getParameter(Constant.COMMAND_NAME_PARAM);
		if(commandName != null) {
			AbstractCommand command = CommandFactory.getCommand(CommandName.valueOf(commandName));
			try {
				command.executeCommand(request, response);
				if(command.isRedirect()) {
					response.sendRedirect(command.getPath());
				} 
			} catch (ServletException | IOException e) {
				//LOGGER.error(e);
				response.sendError(500);
			}
		}
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		this.processRequest(request, response);
	}
   
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		this.processRequest(request, response);
	}
	
	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		//LOGGER.debug(Constant.SERVLET_DELETED);
	}
}
