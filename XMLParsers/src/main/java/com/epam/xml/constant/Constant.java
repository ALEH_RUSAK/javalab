package com.epam.xml.constant;

public final class Constant {
	
	private Constant() {}
	
	public static final String MOBILE_PLAN_XML_PATH = "D:/workspace/mobileplans.xml";
	
	/* Mobile plans XML elements name */
	public static final String BUSINESS = "BUSINESS";
	public static final String STANDART = "STANDART";
	
	/* MobileServlet constants */
	public static final String SERVLET_LOADED = "Servlet was loaded into memory and is initializing now.";
	public static final String SERVLET_DELETED = "Servlet is going to be deleted.";
	public static final String SERVLET_CONTSTRUCTOR_CALL = "Servlet's constructor call.";
	
	/* Resources path's */
	public static final String MOBILE_MAIN = "../pages/mobile_main.jsp";

	public static final String PARSER_TYPE_PARAM = "parserType";
	public static final String MOBILE_LIST_ATTR = "mobilePlanList";

	public static final String COMMAND_NAME_PARAM = "commandName";
}
