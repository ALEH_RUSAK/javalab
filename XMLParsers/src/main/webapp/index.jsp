<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Choosing the XML-parser</title>
	</head>
	<body>
		<b>Choose any of the parsers.</b>   
		<form action="/mobile" method="POST">
			<table border=6> 
				<tr>
					<td>SAX</td>
					<td><input type="radio" name="parser" value="SAX" checked></td>
					<td>DOM</td>
					<td><input type="radio" name="parser" value="DOM"></td>
					<td>StAX</td>
					<td><input type="radio" name="parser" value="StAX"></td>
				</tr>
			</table>
			<input type="submit" value="Parse"><br> 
		</form>
	</body>
</html>