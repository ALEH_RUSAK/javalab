<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>The List Of Mobile Plans</title>
	</head>
	<body>
		<div class="page_body">
			<div class="title">
				The list of Company's mobile plans
			</div>
			<c:forEach var="plan" items="${mobilePlanList}">
				<div class="mobile_plan">
					<c:url var="plan_ref" value="/mobile?commandName=VIEW_PLAN_DETAILS&planName=${plan.name}" />
					<a href="${plan_ref}">${plan.name}</a>
				</div>
			</c:forEach>
		</div>
	</body>
</html>